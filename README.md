Summary
=======

This is the DuMuX module containing the code for producing the results
published in:

S. Ackermann<br>
Development and Evaluation of Iterative Solution Strategies for Coupled Stokes-Darcy Problems<br>
Master's thesis, Institut für Wasser- und Umweltsystemmodellierung, Universität Stuttgart, 4/2016.


Installation
============

You can build the module just like any other DUNE module. For building and running the executables, please go to the folders
containing the sources listed below. For the basic dependencies see dune-project.org. Note that you have to have the
BOOST library installed for this module.  

The easiest way to install this module is to create a new folder and to execute the file
[installAckermann2016a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Ackermann2016a/raw/master/installAckermann2016a.sh)
in this folder. 
  
```bash
mkdir -p Ackermann2016a && cd Ackermann2016a  
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/Ackermann2016a/raw/master/installAckermann2016a.sh  
sh ./installAckermann2016a.sh  
```

For a detailed information on installation have a look at the DuMuX installation guide or use the DuMuX handbook, chapter 2.


Applications
============

The results in the master's thesis are obtained by compiling and running the programs from the sources listed below,
which can be found in appl/staggeredgrid/multidomain/navierstokesdarcypdelab/:<br>

test_diffusionccfvDNcoupled.cc<br>
test_pdelabDNcoupled.cc<br>
test_pdelabDNcoupledtransient.cc<br>
test_pdelabASPINcoupled.cc<br>
testchidyagwairiviereanalytic.cc<br>
test_pdelabcoupledtransient.cc<br>


Output
======

Run the program with the respective source file modifications and ini-files, which can be found in the ini subfolder in the root directory:

* __test_diffusionccfvDNcoupled - Poisson problem__:  
  
    __Figure 4.1a - Convergence__:  
        - use only #define TEST_CASE_POLY (line 39) with "test-pp-conv-i.ini", i = 1,...,10  
        - for monolithic coupling, set [monolithic]solve = true in ini-files  
  
    __Figure 4.1b - Convergence__:  
        - use only #define TEST_CASE_EXP (line 40) with "test-pp-conv-i.ini", i = 1,...,7  
        - (for monolithic coupling, set [monolithic]solve = true in ini-file)  
  
    __Figure 4.2 - Computing times__: see Figure 4.1a, 4.1b  
  
    __Figure 4.3a/Table 4.2 - Damping parameter__:  
        - use only #define TEST_CASE_SIMPLE (line 37) for P0 elements with "test-pp-conv-3.ini"
        - vary parameter "[dn]damping"
        - use dune-multidomain/test/testpoisson-dirichlet-neumann for Q1 and Q2 elements
        - choose finite element maps in lines 1025-1032; use only lines 9, 122, 151, 169, 226, 250, 261, 277, 974 for simple test case!  

    __Figure 4.3b/Table 4.2 - Damping parameter__:  
        - use only #define TEST_CASE_COMPLEX (line 38) for P0 elements with "test-pp-conv-3.ini"  
        - vary parameter "[dn]damping"  
        - use dune-multidomain/test/testpoisson-dirichlet-neumann for Q1 and Q2 elements  
        - choose finite element maps in lines 1025-1032  


* __test_pdelabDNcoupled - Stationary Stokes/Darcy problem__:

    __Figure 4.4 - Setup, Solution__:  
        - use only #define TEST_CASE_E (line 143) in problemchidyagwairiviereanalytic.hh (test case D) with "test-sd-d.ini"  
  
    __Figure 4.5  Damping parameter__:  
        - use only #define TEST_CASE_E (line 143) in problemchidyagwairiviereanalytic.hh (test case D) with "test-sd-stat-d-damp-i.ini", i = 20, 25, ..., 85, 90, 91, ..., 99, 100  
  
    __Figure 4.6 - Convergence__:  
        - with "test-sd-stat-l2-i.ini", i = 1,..., 8  
        - _Figure 4.6a_: use only #define TEST_CASE_Ba (line 139) in problemchidyagwairiviereanalytic.hh (test case A)  
        - _Figure 4.6b_: use only #define TEST_CASE_C (line 141) in problemchidyagwairiviereanalytic.hh (test case B)  
        - _Figure 4.6c_: use only #define TEST_CASE_D (line 142) in problemchidyagwairiviereanalytic.hh (test case C)  
        - _Figure 4.6d_: use only #define TEST_CASE_E (line 143) in problemchidyagwairiviereanalytic.hh (test case D)  
  
    __Figure 4.7_ - Computing times__:  
        - choose test cases A, B, C, D as described for Figure 4.6  
        - monolithic coupling: use testchidyagwairiviereanalytic with refinement level i = 1, ..., 8  
        - iterative coupling: use test_pdelabDNcoupled with "test-sd-stat-l2-i.ini", i = 1, ..., 8  
  
  
* __test_pdelabDNcoupledtransient - Transient Stokes/Darcy problem__:

    __Figure 4.8	- Convergence__:  
        - use #include "problemtransient.hh" (line 31) and #define ANALYTIC_SOLUTION (line 32)
        - use only #define TEST_CASE_E (line 43) in problemtransient.hh (test case D)  
        - with "test-sd-trans-l2-i.ini", i = 1,..., 6  

    __Figure 4.9	- Setup, Solution__:  
        - use only #include "problemsina.hh" (line 33; uncomment lines 31 and 32!)
        - use only #define TEST_PRESSURE_GRADIENT (line 38) in problemsina.hh with "test-sd-e.ini"
  
    __Figure 4.10 - Computing times__:  
        - use only #include "problemsina.hh" (line 33; uncomment lines 31 and 32!)
        - monolithic coupling: use test_pdelabcoupledtransient with refinement level i = 1, ..., 7  
        - iterative coupling: use test_pdelabDNcoupledtransient with "test-sd-e-i.ini", i = 1, ..., 7  
  
  
* __test_pdelabASPINcoupled - ASPIN method__:  
  
    __Figure 4.11 - Convergence__:  
        - use only #define TEST_CASE_E (line 143) in problemchidyagwairiviereanalytic.hh (test case D)  
        - with "test-sd-aspin-i.ini", i = 1, ..., 7  
  
    __Figure 4.12 - ASPIN damping__:  
        - use only #define TEST_CASE_E (line 143) in problemchidyagwairiviereanalytic.hh (test case D)  
        - with "test-sd-aspin-damp-i.ini", i = 0, 1, ..., 10  


Used Versions and Software
==========================
For an overview of the used versions of the DUNE and DuMuX modules, please have a look at
[installAckermann2016a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Ackermann2016a/raw/master/installAckermann2016a.sh).