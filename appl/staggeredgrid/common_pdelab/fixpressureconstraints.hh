/** \file
 *  \ingroup StaggeredModel
 *
 *  \brief Constraints fixing the pressure value in cells, having boundaries
 *         with edges in the specified domain.
 *
 * \note This is only needed in cases were no velocity outflow boundary
 * condition is set. For outflow the pressure is evaluated at the
 * boundary.
*/

#ifndef DUMUX_NAVIER_STOKES_FIXPRESSURECONSTRAINTS_HH
#define DUMUX_NAVIER_STOKES_FIXPRESSURECONSTRAINTS_HH

#include<dune/grid/common/grid.hh>

#include<dune/pdelab/common/geometrywrapper.hh>
#include<dune/pdelab/constraints/common/constraintsparameters.hh>
#include<dune/pdelab/gridfunctionspace/gridfunctionspace.hh>

namespace Dune {
  namespace PDELab {

    /**
     * \brief Sets fix values for the specified domain.
     *
     * \tparam GridView GridView type
     * \tparam BC BoundaryCondition type
     */
    template<typename GridView, typename BC>
    class FixPressureConstraints
    {
    public:
      enum { doBoundary = false };
      enum { doProcessor = false };
      enum { doSkeleton = false };
      enum { doVolume = true };

      //! \brief Index of unknowns
      enum { velocityIdx = 0,
             pressureIdx = 1 };

      //! \brief Constructor
      //! \tparam GridView GridView type
      FixPressureConstraints(GridView gv_, BC bc_)
        : gv(gv_), bc(bc_)
      {}

      /**
       * \brief Volume constraints to fix specified elements to a given value.
       *
       * \tparam EG  element geometry
       * \tparam LFS local function space
       * \tparam T   transformation type
       */
      template<typename EG, typename LFS, typename T>
      void volume(const EG& eg, const LFS& lfs, T& trafo) const
      {
        const int dim = GridView::dimension;
        typedef typename BC::template Child<pressureIdx>::Type BCPressure;
          const BCPressure& bcPressure = bc.template child<pressureIdx>();

        typedef typename GridView::IntersectionIterator IntersectionIterator;
        IntersectionIterator endIt = gv.iend(eg.entity());
        for (IntersectionIterator it = gv.ibegin(eg.entity()); it != endIt; ++it)
        {
          if (it->boundary())
          {
            // whether the current intersection has a Dirichlet value at one of its corners
            bool dirichletCorner = false;
            // we have 2^(dim-1) corners, check each
            // each bit is the value of one dimension, e.g. 6 = 0b110 => (0.0, 1.0, 1.0)
            for (unsigned int curCorner = 0; curCorner < std::pow(2, dim-1); ++curCorner)
            {
              Dune::FieldVector<double, dim-1> faceCornerLocal(0.0);
              if (dim-1 >= 1)
              {
                faceCornerLocal[0] = curCorner % 2;
              }
              if (dim-1 >= 2)
              {
                faceCornerLocal[1] = (curCorner / 2 ) % 2;
              }
              dirichletCorner |= bcPressure.isDirichlet(*it, faceCornerLocal);
            }

            if (dirichletCorner)
            {
              // empty map means Dirichlet constraint
              typename T::RowType empty;

              // set first (and only because we have p0 elements) degrees of freedom
              trafo[lfs.dofIndex(0)] = empty;
            }
          }
        }
      }

    private:
      GridView gv;
      const BC& bc;
    };
  } // end namespace PDELab
} // end namespace Dune

#endif // DUMUX_NAVIER_STOKES_FIXPRESSURECONSTRAINTS_HH
