
install(FILES
        multidomainccfvelementgeometry.hh
        multidomaincclocalresidual.hh
        pdelablocaloperator.hh
        DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/appl/staggeredgrid/multidomain/common_pdelab)
