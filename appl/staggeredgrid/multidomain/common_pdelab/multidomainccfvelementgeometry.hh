// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.    *
 *****************************************************************************/
/*!
 * \file
 * \brief Represents the finite volume geometry of a single element in
 *        the cell-centered fv scheme with an extension for multidomaingrids.
 */
#ifndef DUMUX_MULTIDOMAIN_CCFVELEMENTGEOMETRY_PDELAB_HH
#define DUMUX_MULTIDOMAIN_CCFVELEMENTGEOMETRY_PDELAB_HH

#include <dune/common/version.hh>
#include <dune/geometry/referenceelements.hh>
#include <dune/grid/common/intersectioniterator.hh>

#include <dumux/implicit/cellcentered/ccfvelementgeometry.hh>

namespace Dumux
{
namespace Properties
{
NEW_PROP_TAG(GridView);
}

/*!
 * \brief Represents the finite volume geometry of a single element in
 *        the cell-centered fv scheme with an extension for multidomaingrids.
 */
template<class TypeTag>
class MultidomainCCFVElementGeometry
: public CCFVElementGeometry<TypeTag>
{
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    enum{dimWorld = GridView::dimensionworld};
    typedef typename GridView::ctype CoordScalar;
    typedef typename GridView::Traits::template Codim<0>::Entity Element;
    typedef typename GridView::Traits::template Codim<0>::EntityPointer ElementPointer;
    typedef typename Element::Geometry Geometry;
    typedef Dune::FieldVector<CoordScalar,dimWorld> GlobalPosition;
    typedef typename GridView::IntersectionIterator IntersectionIterator;

public:
    typedef typename CCFVElementGeometry<TypeTag>::SubControlVolumeFace SubControlVolumeFace;

    void update(const GridView& gridView, const Element& element)
    {
        this->updateInner(element);

        const Geometry& geometry = element.geometry();

        bool onBoundary = false;

        // fill neighbor information and control volume face data:
        IntersectionIterator isEndIt = gridView.iend(element);
        for (IntersectionIterator isIt = gridView.ibegin(element); isIt != isEndIt; ++isIt)
        {
            // neighbor information and inner cvf data
            // only neighbors in same SubDomain are included
            if (isIt->neighbor()
                && (gridView.indexSet().contains(1, *(isIt->inside()))
                    && gridView.indexSet().contains(1, *(isIt->outside())))
               )
            {
                this->numNeighbors++;
                ElementPointer elementPointer(isIt->outside());
                this->neighbors.push_back(elementPointer);

                int scvfIdx = this->numNeighbors - 2;
                SubControlVolumeFace& scvFace = this->subContVolFace[scvfIdx];

                scvFace.i = 0;
                scvFace.j = scvfIdx + 1;

                scvFace.ipGlobal = isIt->geometry().center();
                scvFace.ipLocal =  geometry.local(scvFace.ipGlobal);
                scvFace.normal = isIt->centerUnitOuterNormal();
                scvFace.normal *= isIt->geometry().volume();
                scvFace.area = isIt->geometry().volume();

                GlobalPosition distVec = this->elementGlobal
                                       - this->neighbors[scvfIdx+1]->geometry().center();
                distVec /= distVec.two_norm2();

                // gradients using a two-point flux approximation
                scvFace.numFap = 2;
                for (unsigned int fapIdx = 0; fapIdx < scvFace.numFap; fapIdx++)
                {
                    scvFace.grad[fapIdx] = distVec;
                    scvFace.shapeValue[fapIdx] = 0.5;
                }
                scvFace.grad[1] *= -1.0;

                scvFace.fapIndices[0] = scvFace.i;
                scvFace.fapIndices[1] = scvFace.j;

                scvFace.fIdx = isIt->indexInInside();
            }

            // boundary cvf data
            else
            {
                onBoundary = true;
                int bfIdx = isIt->indexInInside();
                SubControlVolumeFace& bFace = this->boundaryFace[bfIdx];

                bFace.ipGlobal = isIt->geometry().center();
                bFace.ipLocal =  geometry.local(bFace.ipGlobal);
                bFace.normal = isIt->centerUnitOuterNormal();
                bFace.normal *= isIt->geometry().volume();
                bFace.area = isIt->geometry().volume();
                bFace.i = 0;
                bFace.j = 0;

                GlobalPosition distVec = this->elementGlobal - bFace.ipGlobal;
                distVec /= distVec.two_norm2();

                // gradients using a two-point flux approximation
                bFace.numFap = 2;
                for (unsigned int fapIdx = 0; fapIdx < bFace.numFap; fapIdx++)
                {
                    bFace.grad[fapIdx] = distVec;
                    bFace.shapeValue[fapIdx] = 0.5;
                }
                bFace.grad[1] *= -1.0;

                bFace.fapIndices[0] = bFace.i;
                bFace.fapIndices[1] = bFace.j;
            }
        }

        // set the number of inner-domain subcontrolvolume faces
        this->numScvf = this->numNeighbors - 1;

        // treat elements on the boundary
        if (onBoundary)
        {
            ElementPointer elementPointer(element);
#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
            for (int bfIdx = 0; bfIdx < element.subEntities(1); bfIdx++)
#else
            for (int bfIdx = 0; bfIdx < element.template count<1>(); bfIdx++)
#endif
            {
                SubControlVolumeFace& bFace = this->boundaryFace[bfIdx];
                bFace.j = this->numNeighbors + bfIdx;
                bFace.fapIndices[1] = bFace.j;
                this->neighbors.push_back(elementPointer);
            }
        }
    }
};

} // end namespace Dumux

#endif // DUMUX_MULTIDOMAIN_CCFVELEMENTGEOMETRY_PDELAB_HH
