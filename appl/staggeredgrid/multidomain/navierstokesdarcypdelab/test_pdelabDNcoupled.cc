#include "config.h"

#define ITERATIVE_COUPLING

#include <dune/common/parametertreeparser.hh>
#include <dune/grid/sgrid.hh>
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>

#include <dune/pdelab/backend/istlmatrixbackend.hh>
#include <dune/pdelab/backend/istlsolverbackend.hh>
#include <dune/pdelab/backend/istlvectorbackend.hh>
#include <dune/pdelab/backend/seqistlsolverbackend.hh>
#include <dune/pdelab/common/vtkexport.hh>
#include <dune/pdelab/constraints/noconstraints.hh>
#include <dune/pdelab/finiteelementmap/p0fem.hh>
#include <dune/pdelab/gridfunctionspace/subspace.hh>
#include <dune/pdelab/newton/newton.hh>

#include <dune/pdelab/multidomain/constraints.hh>
#include <dune/pdelab/multidomain/coupling.hh>
#include <dune/pdelab/multidomain/gridoperator.hh>
#include <dune/pdelab/multidomain/interpolate.hh>
#include <dune/pdelab/multidomain/multidomaingridfunctionspace.hh>
#include <dune/pdelab/multidomain/subproblem.hh>
#include <dune/pdelab/multidomain/subproblemlocalfunctionspace.hh>
#include <dune/pdelab/multidomain/vtk.hh>

#include <dune/pdelab/stationary/linearproblem.hh>
#include <dune/pdelab/gridoperator/onestep.hh>
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>

#include "problemchidyagwairiviereanalytic.hh"    // test cases A, B, C, D
//#include "problemdiscacciati.hh"
#include "diffusionccfv.hh"
#include "l2.hh"
#include "../../common_pdelab/fixpressureconstraints.hh"
#include "../../common_pdelab/fixvelocityconstraints.hh"
#include "../../common_pdelab/l2interpolationerror.hh"
#include "../../localfunctions/staggeredq0fem.hh"
#include "../../freeflow/navierstokes/navierstokes_pdelab/navierstokesstaggeredgrid.hh"

#include "couplingstokes1pdarcy1p.hh" // monolithic coupling
#include "stokesdarcycoupling.hh" // iterative coupling

#include <dune/istl/io.hh>

template<template<class,class,class,int> class Preconditioner,
template<class> class Solver>
class ISTL_SEQ_Subblock_Backend
        : public Dune::PDELab::SequentialNorm
          , public Dune::PDELab::LinearResultStorage
          {
          public:
    /*! \brief make a linear solver object

    \param[in] maxiter_ maximum number of iterations to do
    \param[in] verbose_ print messages if true
     */
    explicit ISTL_SEQ_Subblock_Backend(unsigned block, unsigned maxiter_=5000, int verbose_=1)
    : _block(block)
    , maxiter(maxiter_)
    , verbose(verbose_)
    {}

    unsigned block() const
    {
        return _block;
    }

    void setBlock(unsigned block)
    {
        _block = block;
    }

    /*! \brief solve the given linear system

    \param[in] A the given matrix
    \param[out] z the solution vector to be computed
    \param[in] r right hand side
    \param[in] reduction to be achieved
     */
    template<class M, class V, class W>
    void apply(M& A, V& z, W& r, typename W::ElementType reduction)
    {
        static_assert(std::is_same<V,W>::value,"V and W must be identical");

        typedef typename Dune::PDELab::istl::raw_type<M>::type ISTLMatrix;
        typedef typename Dune::PDELab::istl::raw_type<V>::type ISTLVector;

        typedef typename ISTLMatrix::block_type MatrixBlock;
        typedef typename ISTLVector::block_type VectorBlock;

        Dune::MatrixAdapter<
        MatrixBlock,
        VectorBlock,
        VectorBlock
        > opa(Dune::PDELab::istl::raw(A)[_block][_block]);
        Preconditioner<
        MatrixBlock,
        VectorBlock,
        VectorBlock,
        1
        > prec(Dune::PDELab::istl::raw(A)[_block][_block], 3, 1.0);
        Solver<
        VectorBlock
        > solver(opa, prec, reduction, maxiter, verbose);
        Dune::InverseOperatorResult stat;
        solver.apply(Dune::PDELab::istl::raw(z)[_block], Dune::PDELab::istl::raw(r)[_block], stat);
        res.converged  = stat.converged;
        res.iterations = stat.iterations;
        res.elapsed    = stat.elapsed;
        res.reduction  = stat.reduction;
        res.conv_rate  = stat.conv_rate;
    }

          private:
    unsigned _block;
    unsigned maxiter;
    int verbose;
          };

int main(int argc, char** argv)
{
    try
    {
        Dune::MPIHelper::instance(argc,argv);

        if (argc != 2) {
            std::cerr << "Usage: " << argv[0] << " <ini file>" << std::endl;
            return 1;
        }
        Dune::ParameterTree parameters;
        Dune::ParameterTreeParser::readINITree(argv[1],parameters);

        // set up grid
        const int dim = 2;
        typedef Dune::SGrid<dim, dim, double> BaseGrid;
        Dune::FieldVector<double, dim> low(0.0);
        Dune::FieldVector<double, dim> high(0.0);
        high[0] = 1.0;
        high[1] = 2.0;
        Dune::FieldVector<int, dim> n(0.0);
        n[0] = 1;
        n[1] = 2;
        BaseGrid baseGrid(n, low, high);
        unsigned int refinementLevel = parameters.get<double>("mesh.refine");
        baseGrid.globalRefine(refinementLevel);
        const int gridCells = std::pow(2, refinementLevel + 1);
        typedef Dune::MultiDomainGrid<BaseGrid,Dune::mdgrid::FewSubDomainsTraits<BaseGrid::dimension,4> > Grid;
        Grid grid(baseGrid,false);
        typedef Grid::SubDomainGrid SubDomainGrid;
        SubDomainGrid& sdg0 = grid.subDomain(0);
        SubDomainGrid& sdg1 = grid.subDomain(1);
        typedef Grid::LeafGridView MDGV;
        typedef SubDomainGrid::LeafGridView SDGV;
        MDGV mdgv = grid.leafGridView();
        SDGV sdgv0 = sdg0.leafGridView(); // upper sub-domain
        SDGV sdgv1 = sdg1.leafGridView(); // lower sub-domain
        sdg0.hostEntityPointer(*sdgv0.begin<0>());
        grid.startSubDomainMarking();
        double interface = parameters.get<double>("mesh.interface");
        for (MDGV::Codim<0>::Iterator it = mdgv.begin<0>(); it != mdgv.end<0>(); ++it)
        {
            if (it->geometry().center()[1] > interface * (high[1] + low[1]))
            {
                grid.addToSubDomain(0, *it);
            }
            else
            {
                grid.addToSubDomain(1, *it);
            }
        }
        grid.preUpdateSubDomains();
        grid.updateSubDomains();
        grid.postUpdateSubDomains();

        // types and constants
        typedef double DF;
        typedef double RF;
        const int indexVelocity = 0;
        const int indexPressure = 1;
        const int indexUpper = 0;
        const int indexLower = 1;

        // instantiate finite element maps
        typedef Dune::PDELab::P0LocalFiniteElementMap<DF, RF, dim> P0FEM;
        P0FEM p0fem(Dune::GeometryType(Dune::GeometryType::cube, dim));
        typedef Dune::PDELab::StaggeredQ0LocalFiniteElementMap<DF, RF, dim> StaggeredQ0FEM;
        StaggeredQ0FEM staggeredq0fem;

        // set up functions defining the problem
        // functions for Navier-Stokes
        typedef Dune::PDELab::CompositeConstraintsParameters<BCVelocity, BCPressure> BCType;
        BCVelocity bcVelocity;
        BCPressure bcPressure;
        BCType bc(bcVelocity, bcPressure);
        DirichletVelocity<MDGV, RF> dirichletVelocity(mdgv);
        DirichletPressure<MDGV, RF> dirichletPressure(mdgv);
        NeumannVelocity<MDGV, RF> neumannVelocity(mdgv);
        NeumannPressure<MDGV, RF> neumannPressure(mdgv);
        SourceMomentumBalance<MDGV, RF> sourceMomentumBalance(mdgv);
        SourceMassBalance<MDGV, RF> sourceMassBalance(mdgv);
        typedef Dune::PDELab::CompositeGridFunction
                <DirichletVelocity<MDGV, RF>, DirichletPressure<MDGV, RF> > DirichletComposed;
        DirichletComposed dirichletComposed(dirichletVelocity, dirichletPressure);
        // functions for Darcy
        DarcyBC<MDGV> darcyBC(mdgv);
        DarcyG<MDGV, RF> darcyG(mdgv);
        DarcyK<MDGV, RF> darcyK(mdgv);
        DarcyA0<MDGV, RF> darcyA0(mdgv);
        DarcySource<MDGV, RF> darcySource(mdgv);
        DarcyJ<MDGV, RF> darcyJ(mdgv);

        // construct grid function spaces
        typedef Dune::PDELab::ISTLVectorBackend<> VectorBackend;
        typedef Dune::PDELab::ISTLVectorBackend<
                Dune::PDELab::ISTLParameters::dynamic_blocking
                > MDVBE;

        typedef Dune::PDELab::FixVelocityConstraints VelocityConstraints;
        VelocityConstraints velocityConstraints;
        typedef Dune::PDELab::GridFunctionSpace<SDGV, StaggeredQ0FEM, VelocityConstraints, VectorBackend> StaggeredQ0GFS;
        StaggeredQ0GFS staggeredQ0Gfs0(sdgv0, staggeredq0fem, velocityConstraints);
        staggeredQ0Gfs0.name("velocity");

        typedef Dune::PDELab::FixPressureConstraints<MDGV, BCType> PressureConstraints;
        PressureConstraints pressureConstraints(mdgv, bc);
        typedef Dune::PDELab::GridFunctionSpace<SDGV, P0FEM, PressureConstraints, VectorBackend> P0GFS;
        P0GFS p0gfs0(sdgv0, p0fem, pressureConstraints);
        p0gfs0.name("pressure");

        // GFS1
        typedef Dune::PDELab::GridFunctionSpace<SDGV, P0FEM, Dune::PDELab::NoConstraints, VectorBackend> P0DarcyGFS;
        P0DarcyGFS p0gfs1(sdgv1, p0fem);
        p0gfs1.name("pressure");

        // composed grid function space GFS0
        typedef Dune::PDELab::CompositeGridFunctionSpace<VectorBackend,
                Dune::PDELab::LexicographicOrderingTag, StaggeredQ0GFS, P0GFS> ComposedGFS;
        ComposedGFS composedGfs0(staggeredQ0Gfs0, p0gfs0);

        typedef Dune::PDELab::MultiDomain::MultiDomainGridFunctionSpace<
                Grid,
                MDVBE,
                Dune::PDELab::LexicographicOrderingTag,
                ComposedGFS,
                P0DarcyGFS
                > MultiGFS;
        MultiGFS multigfs(grid, composedGfs0, p0gfs1);

        // make grid function operator
        // for Stokes
        typedef Dune::PDELab::NavierStokesStaggeredGrid<BCType,
                SourceMomentumBalance<MDGV, RF>, SourceMassBalance<MDGV, RF>,
                DirichletVelocity<MDGV, RF>, DirichletPressure<MDGV, RF>,
                NeumannVelocity<MDGV, RF>, NeumannPressure<MDGV, RF>, MDGV> LOpStokes;
        LOpStokes lopStokes(bc, sourceMomentumBalance, sourceMassBalance,
                dirichletVelocity, dirichletPressure, neumannVelocity, neumannPressure, mdgv);

        // for Darcy
        typedef Dune::PDELab::DiffusionCCFV<DarcyK<MDGV, RF>, DarcyA0<MDGV, RF>,
                DarcySource<MDGV, RF>, DarcyBC<MDGV>, DarcyJ<MDGV, RF>, DarcyG<MDGV, RF> > LOpDarcy;
        LOpDarcy lopDarcy(darcyK, darcyA0, darcySource, darcyBC, darcyJ, darcyG);

        typedef Dune::PDELab::MultiDomain::SubDomainEqualityCondition<Grid> Condition;
        Condition c0(0);
        Condition c1(1);

        typedef Dune::PDELab::MultiDomain::SubProblem
                <MultiGFS, MultiGFS, LOpStokes, Condition, 0> UpperSubProblem;
        UpperSubProblem upper_sp(lopStokes, c0);

        typedef Dune::PDELab::MultiDomain::SubProblem
                <MultiGFS, MultiGFS, LOpDarcy, Condition, 1> LowerSubProblem;
        LowerSubProblem lower_sp(lopDarcy, c1);

        typedef typename Dune::PDELab::BackendVectorSelector<MultiGFS, DF>::Type V;
        V xOld(multigfs);
        xOld = 0.0;

        auto constraints = Dune::PDELab::MultiDomain::constraints<RF>(multigfs,
                Dune::PDELab::MultiDomain::constrainSubProblem(upper_sp, bc),
                Dune::PDELab::MultiDomain::constrainSubProblem(lower_sp, darcyBC));

        typedef MultiGFS::ConstraintsContainer<RF>::Type ConstraintsContainer;
        ConstraintsContainer constraintsContainer;
        constraints.assemble(constraintsContainer);

        typedef Dune::PDELab::ISTLMatrixBackend MatrixBackend;

        // for monolithic coupling
        CouplingStokes1pDarcy1p<RF> couplingStokes1pDarcy1p;
        typedef Dune::PDELab::MultiDomain::Coupling<UpperSubProblem,LowerSubProblem,CouplingStokes1pDarcy1p<RF> > Coupling_mono;
        Coupling_mono coupling_mono(upper_sp, lower_sp, couplingStokes1pDarcy1p);

        typedef Dune::PDELab::MultiDomain::GridOperator<
                MultiGFS, MultiGFS,
                MatrixBackend, RF, RF, RF, ConstraintsContainer, ConstraintsContainer,
                UpperSubProblem,
                LowerSubProblem,
                Coupling_mono
                > GridOperator;

        GridOperator gridoperator(multigfs, multigfs,
                constraintsContainer, constraintsContainer,
                upper_sp,
                lower_sp,
                coupling_mono
        );

        // for iterative coupling
        auto parse_coupling_type = [](std::string name) -> Dune::PDELab::CouplingMode
        {
            if (name == "Stokes")
                return Dune::PDELab::CouplingMode::Stokes;
            if (name == "Darcy")
                return Dune::PDELab::CouplingMode::Darcy;
            DUNE_THROW(Dune::Exception,"Unknown coupling type " << name);
        };

        typedef Dune::PDELab::StokesDarcyCoupling<RF> SDCoupling;

        SDCoupling sd_coupling_stokes(parse_coupling_type(parameters["dn.coupling.upper"]));
        SDCoupling sd_coupling_darcy(parse_coupling_type(parameters["dn.coupling.lower"]));

        typedef Dune::PDELab::MultiDomain::Coupling<
                UpperSubProblem,
                LowerSubProblem,
                SDCoupling
                > StokesCoupling;
        StokesCoupling stokes_coupling(
                upper_sp,
                lower_sp,
                sd_coupling_stokes
        );

        typedef Dune::PDELab::MultiDomain::Coupling<
                UpperSubProblem,
                LowerSubProblem,
                SDCoupling
                > DarcyCoupling;
        DarcyCoupling darcy_coupling(
                upper_sp,
                lower_sp,
                sd_coupling_darcy
        );

        typedef Dune::PDELab::MultiDomain::GridOperator<
                MultiGFS,MultiGFS,
                MatrixBackend, RF, RF, RF, ConstraintsContainer, ConstraintsContainer,
                UpperSubProblem,
                StokesCoupling,
                LowerSubProblem,
                DarcyCoupling
                > FullOperator;
        FullOperator full_operator(
                multigfs,multigfs,
                constraintsContainer, constraintsContainer,
                upper_sp,
                stokes_coupling,
                lower_sp,
                darcy_coupling
        );

        typedef Dune::PDELab::MultiDomain::GridOperator<
                MultiGFS,MultiGFS,
                MatrixBackend, RF, RF, RF, ConstraintsContainer, ConstraintsContainer,
                UpperSubProblem,
                StokesCoupling
                > UpperOperator;
        UpperOperator upper_operator(
                multigfs,multigfs,
                constraintsContainer, constraintsContainer,
                upper_sp,
                stokes_coupling
        );

        typedef Dune::PDELab::MultiDomain::GridOperator<
                MultiGFS,MultiGFS,
                MatrixBackend, RF, RF, RF, ConstraintsContainer, ConstraintsContainer,
                LowerSubProblem,
                DarcyCoupling
                > LowerOperator;
        LowerOperator lower_operator(
                multigfs,multigfs,
                constraintsContainer, constraintsContainer,
                lower_sp,
                darcy_coupling
        );

        // make coefficient Vector and initialize it from a function
        Dune::PDELab::MultiDomain::interpolateOnTrialSpace(multigfs, xOld,
                dirichletComposed, upper_sp, darcyG, lower_sp);

        // clear interior
        Dune::PDELab::set_nonconstrained_dofs(constraintsContainer, 0.0, xOld);

        int dofs_stokes = xOld.block(0).N();
        int dofs_darcy = xOld.block(1).N();
        int dofs = dofs_stokes + dofs_darcy;
        std::cout << "interpolation: " << dofs << " dof total, " << constraintsContainer.size() << " dof constrained" << std::endl;

        using SubDomainIndex = typename Grid::SubDomainIndex;

        if (parameters.get<bool>("monolithic.solve"))
        {
            // <<<5>>> Select a linear solver backend
#if HAVE_SUPERLU
            typedef Dune::PDELab::ISTLBackend_SEQ_SuperLU LinearSolver;
#else
#error No direct linear solver, use SuperLU.
#endif
            LinearSolver ls(false);

            V xOld2(xOld);

            // <<<6>>> Solve nonlinear problem
            typename Dune::PDELab::Newton<GridOperator, LinearSolver, V> newton(gridoperator, xOld2, ls);
            newton.setVerbosityLevel(2);
            newton.setMaxIterations(25);
            newton.setReduction(1e-9);
            newton.setLineSearchStrategy(newton.noLineSearch);

            auto start = std::chrono::high_resolution_clock::now();

            newton.apply();

            auto end = std::chrono::high_resolution_clock::now();
            auto elapsed = end - start;

            // <<<8>>> graphical output
            typedef Dune::PDELab::GridFunctionSubSpace
                    <MultiGFS, Dune::TypeTree::TreePath<indexUpper, indexVelocity> > VSUB;
            VSUB vsub(multigfs);                   // velocity subspace

            typedef Dune::PDELab::GridFunctionSubSpace
                    <MultiGFS, Dune::TypeTree::TreePath<indexUpper, indexPressure> > PSUB;
            PSUB psub(multigfs);                   // pressure subspace

            // make discrete function object
            typedef Dune::PDELab::DiscreteGridFunction<VSUB, V> StaggeredQ0DGF;
            StaggeredQ0DGF staggeredQ0DGF(vsub, xOld2);
            typedef Dune::PDELab::DiscreteGridFunction<PSUB, V> P0DGF;
            P0DGF p0dgf(psub, xOld2);

            // plot result as VTK
            Dune::SubsamplingVTKWriter<SDGV> vtkwriterStokes(sdgv0, 1);
            vtkwriterStokes.addCellData(new Dune::PDELab::VTKGridFunctionAdapter<StaggeredQ0DGF>(staggeredQ0DGF, "velocity"));
            vtkwriterStokes.addCellData(new Dune::PDELab::VTKGridFunctionAdapter<P0DGF>(p0dgf, "pressure"));
            char fname[255];
            sprintf(fname, "test_pdelabcoupled_stokes-%04d", gridCells);
            vtkwriterStokes.write(fname, Dune::VTK::ascii);

            typedef Dune::PDELab::GridFunctionSubSpace
                    <MultiGFS, Dune::TypeTree::TreePath<indexLower> > LowerGFS;
            LowerGFS lowerGFS(multigfs);                   // velocity subspace
            // make discrete function object
            typedef Dune::PDELab::DiscreteGridFunction<LowerGFS, V> P0DarcyDGF;
            P0DarcyDGF p0dgfDarcy(lowerGFS, xOld2);

            // plot result as VTK
            Dune::VTKWriter<SDGV> vtkwriterDarcy(sdgv1, Dune::VTK::nonconforming);
            vtkwriterDarcy.addCellData(new Dune::PDELab::VTKGridFunctionAdapter<P0DarcyDGF>(p0dgfDarcy, "pressure"));
            sprintf(fname, "test_pdelabcoupled_darcy-%04d", gridCells);
            vtkwriterDarcy.write(fname, Dune::VTK::ascii);

            // compute L2 error if analytical solution is available
            DirichletVelocity<SDGV, RF> dirichletVelocitySubdomain(sdgv0);
            DirichletPressure<SDGV, RF> dirichletPressureSubdomain(sdgv0);
            DarcyG<SDGV, RF> darcyPressureSubdomain(sdgv1);
            std::cout.precision(8);
//            std::cout << "L2 error (Stokes) for "
//                    << std::setw(6) << sdgv0.size(0)
//                    << " elements. pressure: "
//                    << std::scientific
//                    << l2interpolationerror(dirichletPressureSubdomain, psub, xOld2, 8)
//                    << " velocity: "
//                    << std::scientific
//                    << l2interpolationerror(dirichletVelocitySubdomain, vsub, xOld2, 8)
//                    << std::endl;

            double elements = std::pow(4,refinementLevel);
            std::ofstream file("results_pdelabcoupled.txt", std::ios::out|std::ios::app);
            file << refinementLevel << "\t"
                    << elements << "\t"
                    << std::chrono::duration_cast<std::chrono::duration<double> >(elapsed).count() << "\t"
                    << std::scientific << l2interpolationerror(dirichletPressureSubdomain, psub, xOld2, 8) << "\t"
                    << std::scientific << l2interpolationerror(dirichletVelocitySubdomain, vsub, xOld2, 8) << "\t"
                    << std::scientific << l2interpolationerror(darcyPressureSubdomain, lowerGFS, xOld2, 8) << std::endl;

        } // if(parameters.get<bool>("monolithic.solve"))

        if (parameters.get<bool>("dn.solve"))
        {
            // <<<5>>> Select a linear solver backend

            // direct solver for Stokes
#if HAVE_SUPERLU
            typedef Dune::PDELab::ISTLBackend_SEQ_SuperLU StokesSolver;
#else
#error No direct linear solver, use SuperLU.
#endif

            StokesSolver stokes_solver(false);

            // iterative solver for Darcy
            typedef ISTL_SEQ_Subblock_Backend<
                    Dune::SeqSSOR,
                    Dune::BiCGSTABSolver
                    > DarcySolver;

            DarcySolver darcy_solver(
                    1,
                    parameters.get<int>("dn.lower.linearsolver.iterations"),
                    parameters.get<int>("dn.lower.linearsolver.verbosity")
            );

            // <<<6>>> Solver for nonlinear problem per stage (Stokes)
            double newton_reduction = parameters.get<double>("dn.upper.newtonreduction");
            typename Dune::PDELab::Newton<UpperOperator, StokesSolver, V> upper_newton(upper_operator, xOld, stokes_solver);
            upper_newton.setVerbosityLevel(2);
            upper_newton.setMaxIterations(25);
            upper_newton.setReduction(newton_reduction);
            upper_newton.setLineSearchStrategy(upper_newton.noLineSearch);


            // Solver for linear problem per stage (Darcy)
            typedef Dune::PDELab::StationaryLinearProblemSolver<
                    LowerOperator,
                    DarcySolver,
                    V
                    > LowerPDESolver;
            LowerPDESolver lower_pde_solver(
                    lower_operator,
                    darcy_solver,
                    parameters.sub("dn.lower.solver")
            );

            V residual(multigfs);
            full_operator.residual(xOld,residual);

            RF r, start_r;
            r = start_r = residual.two_norm();
            std::cout << "Start residual: " << r << std::endl;

            std::size_t i = 0;
            std::size_t max_iterations = parameters.get<int>("dn.coupling.iterations");

            auto x(xOld);

            double theta = parameters.get<double>("dn.coupling.damping");
            double rel_reduction = parameters.get<double>("dn.coupling.reduction");
            double max_error = parameters.get<double>("dn.coupling.maxerror");

            bool reassemble_jacobian_lower = parameters.get<bool>("dn.lower.reassemble");
            double lower_max_reduction = parameters.get<double>("dn.lower.solver.reduction");
            double lower_min_reduction = parameters.get<double>("dn.lower.solver.minreduction");
            double lower_decay = parameters.get<double>("dn.lower.solver.reductiondecay");
            double lower_offset = 1.0 / (1.0 - lower_max_reduction / lower_min_reduction);

            auto start = std::chrono::high_resolution_clock::now();

            const std::size_t vtk_frequency = parameters.get<int>("dn.coupling.vtk_frequency");
            const bool vtk_enabled = vtk_frequency > 0;

//            std::ofstream file_res("residual.txt", std::ios::out|std::ios::app);
//            file_res <<  i << "\t" << r << "\t" << start_r << "\t" << std::endl;

            while (r / start_r > rel_reduction && r > max_error && i < max_iterations)
            {
                if (vtk_enabled && i % vtk_frequency == 0)
                    {
                        {
                            Dune::VTKWriter<SDGV> vtkwriter(sdgv0,Dune::VTK::nonconforming);
                            Dune::PDELab::MultiDomain::addSolutionToVTKWriter(
                                    vtkwriter,
                                    multigfs,
                                    xOld,
                                    Dune::PDELab::MultiDomain::subdomain_predicate<SubDomainIndex>(sdgv0.grid().domain())
                                    );
                            Dune::PDELab::MultiDomain::addSolutionToVTKWriter(
                                    vtkwriter,
                                    multigfs,
                                    residual,
                                    Dune::PDELab::MultiDomain::subdomain_predicate<SubDomainIndex>(sdgv0.grid().domain()),
                                    Dune::PDELab::vtk::DefaultFunctionNameGenerator("r")
                                    );
                            std::stringstream fn;
                            fn << "upper-" << i;
                            vtkwriter.write(fn.str(),Dune::VTK::ascii);
                        }
                        {
                            Dune::SubsamplingVTKWriter<SDGV> vtkwriter(sdgv1,2);
                            Dune::PDELab::MultiDomain::addSolutionToVTKWriter(
                                    vtkwriter,
                                    multigfs,
                                    xOld,
                                    Dune::PDELab::MultiDomain::subdomain_predicate<SubDomainIndex>(sdgv1.grid().domain())
                                    );
                            Dune::PDELab::MultiDomain::addSolutionToVTKWriter(
                                    vtkwriter,
                                    multigfs,
                                    residual,
                                    Dune::PDELab::MultiDomain::subdomain_predicate<SubDomainIndex>(sdgv1.grid().domain()),
                                    Dune::PDELab::vtk::DefaultFunctionNameGenerator("r")
                            );
                            std::stringstream fn;
                            fn << "lower-" << i;
                            vtkwriter.write(fn.str(),Dune::VTK::ascii);
                        }
                    } // if (vtk_enabled && i % vtk_frequency == 0)

                std::cout << "\n*** Stokes:" << std::endl;
                x = xOld;
                upper_newton.apply();

                std::cout << "\n*** Darcy:" << std::endl;
                lower_pde_solver.setReduction(lower_min_reduction * (1.0 - 1.0 / (lower_offset + lower_decay * i)));
                lower_pde_solver.apply(xOld,!reassemble_jacobian_lower && (i>0));

                xOld *= theta;
                xOld.axpy(1.0-theta,x);

                residual = 0.0;
                full_operator.residual(xOld,residual);
                r = residual.two_norm();
                ++i;
//                file_res <<  i << "\t" << r << "\t" << start_r << "\t" << std::endl;
                std::cout << std::setw(4) << "i = " << i << ", res = " << r << std::endl;
            } // while (r / start_r > rel_reduction && r > max_error && i < max_iterations)

            auto end = std::chrono::high_resolution_clock::now();

            auto elapsed = end - start;

            std::cout << std::endl
                    << std::endl
                    << "iterative Stokes-Darcy solver: " << std::chrono::duration_cast<std::chrono::duration<double> >(elapsed).count() << std::endl
                    << std::endl;

            // <<<8>>> graphics
            typedef Dune::PDELab::GridFunctionSubSpace
                    <MultiGFS, Dune::TypeTree::TreePath<indexUpper, indexVelocity> > VSUB;
            VSUB vsub(multigfs);                   // velocity subspace

            typedef Dune::PDELab::GridFunctionSubSpace
                    <MultiGFS, Dune::TypeTree::TreePath<indexUpper, indexPressure> > PSUB;
            PSUB psub(multigfs);                   // pressure subspace

            // make discrete function object
            typedef Dune::PDELab::DiscreteGridFunction<VSUB, V> StaggeredQ0DGF;
            StaggeredQ0DGF staggeredQ0DGF(vsub, xOld);
            typedef Dune::PDELab::DiscreteGridFunction<PSUB, V> P0DGF;
            P0DGF p0dgf(psub, xOld);

            // plot result as VTK
            Dune::SubsamplingVTKWriter<SDGV> vtkwriterStokes(sdgv0, 1);
            vtkwriterStokes.addCellData(new Dune::PDELab::VTKGridFunctionAdapter<StaggeredQ0DGF>(staggeredQ0DGF, "velocity"));
            vtkwriterStokes.addCellData(new Dune::PDELab::VTKGridFunctionAdapter<P0DGF>(p0dgf, "pressure"));
            char fname[255];
            sprintf(fname, "SDcoupled-Stokes-%04d", gridCells);
            vtkwriterStokes.write(fname, Dune::VTK::ascii);

            typedef Dune::PDELab::GridFunctionSubSpace
                    <MultiGFS, Dune::TypeTree::TreePath<indexLower> > LowerGFS;
            LowerGFS lowerGFS(multigfs);                   // velocity subspace
            // make discrete function object
            typedef Dune::PDELab::DiscreteGridFunction<LowerGFS, V> P0DarcyDGF;
            P0DarcyDGF p0dgfDarcy(lowerGFS, xOld);

            // plot result as VTK
            Dune::VTKWriter<SDGV> vtkwriterDarcy(sdgv1, Dune::VTK::nonconforming);
            vtkwriterDarcy.addCellData(new Dune::PDELab::VTKGridFunctionAdapter<P0DarcyDGF>(p0dgfDarcy, "pressure"));
            sprintf(fname, "SDcoupled-Darcy-%04d", gridCells);
            vtkwriterDarcy.write(fname, Dune::VTK::ascii);

            // compute L2 error if analytical solution is available
            DirichletVelocity<SDGV, RF> dirichletVelocitySubdomain(sdgv0);
            DirichletPressure<SDGV, RF> dirichletPressureSubdomain(sdgv0);
            DarcyG<SDGV, RF> darcyPressureSubdomain(sdgv1);
            std::cout.precision(8);
//            std::cout << "L2 error (Stokes) for "
//                    << std::setw(6) << sdgv0.size(0)
//                    << " elements. pressure: "
//                    << std::scientific
//                    << l2interpolationerror(dirichletPressureSubdomain, psub, xOld, 8)
//                    << ", velocity: "
//                    << std::scientific
//                    << l2interpolationerror(dirichletVelocitySubdomain, vsub, xOld, 8)
//                    << std::endl
//                    << "L2 error (Darcy)  for "
//                    << std::setw(6) << sdgv1.size(0)
//                    << " elements. pressure: "
//                    << std::scientific
//                    << l2interpolationerror(darcyPressureSubdomain, lowerGFS, xOld, 8)
//                    << std::endl;

            std::ofstream file("results_pdelabDNcoupled.txt", std::ios::out|std::ios::app);
            file << "dn" << "\t"
                    << refinementLevel << "\t"
                    << std::chrono::duration_cast<std::chrono::duration<double> >(elapsed).count() << "\t"
                    << std::scientific << l2interpolationerror(dirichletPressureSubdomain, psub, xOld, 8) << "\t"
                    << std::scientific << l2interpolationerror(dirichletVelocitySubdomain, vsub, xOld, 8) << "\t"
                    << std::scientific << l2interpolationerror(darcyPressureSubdomain, lowerGFS, xOld, 8) << "\t"
                    << i << std::endl;

        } // if (parameters.get<bool>("dn.solve"))

        return EXIT_SUCCESS;
    } // try
    catch (Dune::Exception &e){
        std::cerr << "Dune reported error: " << e << std::endl;
        return 1;
    }
    catch (std::exception e) {
        std::cerr << "Dune reported std error: " << e.what() << std::endl;
    }
    catch (...){
        std::cerr << "Unknown exception thrown!" << std::endl;
        return 1;
    }
} // main
