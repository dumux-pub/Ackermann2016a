#include "config.h"

#include <dune/grid/sgrid.hh>
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>

#include <dune/pdelab/backend/istlmatrixbackend.hh>
#include <dune/pdelab/backend/istlvectorbackend.hh>
#include <dune/pdelab/backend/seqistlsolverbackend.hh>
#include <dune/pdelab/common/vtkexport.hh>
#include <dune/pdelab/constraints/noconstraints.hh>
#include <dune/pdelab/finiteelementmap/p0fem.hh>
#include <dune/pdelab/gridfunctionspace/subspace.hh>
#include <dune/pdelab/newton/newton.hh>

#include <dune/pdelab/multidomain/constraints.hh>
#include <dune/pdelab/multidomain/coupling.hh>
#include <dune/pdelab/multidomain/gridoperator.hh>
#include <dune/pdelab/multidomain/interpolate.hh>
#include <dune/pdelab/multidomain/multidomaingridfunctionspace.hh>
#include <dune/pdelab/multidomain/subproblem.hh>
#include <dune/pdelab/multidomain/subproblemlocalfunctionspace.hh>
#include <dune/pdelab/multidomain/vtk.hh>

#include "problemtransient.hh"                        // test cases with transient boundary condition
#define ANALYTIC_SOLUTION // only for problemtransient! not for problemsina!
//#include "problemsina.hh"

#include "couplingstokes1pdarcy1p.hh"
#include "diffusionccfv.hh"
#include "l2.hh"
#include "../../common_pdelab/fixpressureconstraints.hh"
#include "../../common_pdelab/fixvelocityconstraints.hh"
#include "../../common_pdelab/l2interpolationerror.hh"
#include "../../localfunctions/staggeredq0fem.hh"
#include "../../freeflow/navierstokes/navierstokes_pdelab/navierstokesstaggeredgrid.hh"
#include "../../freeflow/navierstokes/navierstokes_pdelab/navierstokestransientstaggeredgrid.hh"
#include "../../../../../dune-pdelab/dune/pdelab/gridoperator/onestep.hh"
#include <dune/pdelab/gridoperator/common/timesteppingparameterinterface.hh>



int main(int argc, char** argv)
{
  unsigned int refinementLevel = 3;
  if (argc < 3)
  {
    std::cerr << "Usage: " << argv[0] << " <refinement level> <factor>" << std::endl;
    return 1;
  }
  else
  {
    refinementLevel = atoi(argv[1]);
  }

  try
  {
    // set up grid
    const int dim = 2;
    typedef Dune::SGrid<dim, dim, double> BaseGrid;
    Dune::FieldVector<double, dim> low(0.0);
    Dune::FieldVector<double, dim> high(0.0);
    high[0] = 1.0;
    high[1] = 2.0;
    Dune::FieldVector<int, dim> n(0.0);
    n[0] = 1;
    n[1] = 2;
    BaseGrid baseGrid(n, low, high);
    baseGrid.globalRefine(refinementLevel);
    typedef Dune::MultiDomainGrid<BaseGrid,Dune::mdgrid::FewSubDomainsTraits<BaseGrid::dimension,4> > Grid;
    Grid grid(baseGrid,false);
    typedef Grid::SubDomainGrid SubDomainGrid;
    SubDomainGrid& sdg0 = grid.subDomain(0);
    SubDomainGrid& sdg1 = grid.subDomain(1);
    typedef Grid::LeafGridView MDGV;
    typedef SubDomainGrid::LeafGridView SDGV;
    MDGV mdgv = grid.leafGridView();
    // upper subdomain
    SDGV sdgv0 = sdg0.leafGridView();
    // lower subdomain
    SDGV sdgv1 = sdg1.leafGridView();
    sdg0.hostEntityPointer(*sdgv0.begin<0>());
    grid.startSubDomainMarking();
    for (MDGV::Codim<0>::Iterator it = mdgv.begin<0>(); it != mdgv.end<0>(); ++it)
    {
      if (it->geometry().center()[1] > 0.5 * (high[1] + low[1]))
      {
        grid.addToSubDomain(0, *it);
      }
      else
      {
        grid.addToSubDomain(1, *it);
      }
    }
    grid.preUpdateSubDomains();
    grid.updateSubDomains();
    grid.postUpdateSubDomains();

    // types and constants
    typedef double DF;
    typedef double RF;
    const int indexVelocity = 0;
    const int indexPressure = 1;
    const int indexUpper = 0;
    const int indexLower = 1;

    // instantiate finite element maps
    typedef Dune::PDELab::P0LocalFiniteElementMap<DF, RF, dim> P0FEM;
    P0FEM p0fem(Dune::GeometryType(Dune::GeometryType::cube, dim));
    typedef Dune::PDELab::StaggeredQ0LocalFiniteElementMap<DF, RF, dim> StaggeredQ0FEM;
    StaggeredQ0FEM staggeredq0fem;

    // set up functions defining the problem
    // functions for Navier-Stokes
    typedef Dune::PDELab::CompositeConstraintsParameters<BCVelocity, BCPressure> BCType;
    BCVelocity bcVelocity;
    BCPressure bcPressure;
    BCType bc(bcVelocity, bcPressure);
    DirichletVelocity<MDGV, RF> dirichletVelocity(mdgv); // stationary boundary conditions
    //DirichletVelocity<MDGV, RF, RF> dirichletVelocity(mdgv); // instationary boundary conditions
    DirichletPressure<MDGV, RF> dirichletPressure(mdgv);
    NeumannVelocity<MDGV, RF> neumannVelocity(mdgv);
    NeumannPressure<MDGV, RF> neumannPressure(mdgv);
    SourceMomentumBalance<MDGV, RF> sourceMomentumBalance(mdgv);
    SourceMassBalance<MDGV, RF> sourceMassBalance(mdgv);
    typedef Dune::PDELab::CompositeGridFunction
      <DirichletVelocity<MDGV, RF>, DirichletPressure<MDGV, RF> > DirichletComposed;
    DirichletComposed dirichletComposed(dirichletVelocity, dirichletPressure);
    // functions for Darcy
    DarcyBC<MDGV> darcyBC(mdgv);
    DarcyG<MDGV, RF> darcyG(mdgv);
    DarcyK<MDGV, RF> darcyK(mdgv);
    DarcyA0<MDGV, RF> darcyA0(mdgv);
    DarcySource<MDGV,RF> darcySource(mdgv);
    DarcyJ<MDGV, RF> darcyJ(mdgv);

    // construct grid function spaces
    typedef Dune::PDELab::ISTLVectorBackend<> VectorBackend;
    typedef Dune::PDELab::FixVelocityConstraints VelocityConstraints;
    VelocityConstraints velocityConstraints;
    typedef Dune::PDELab::GridFunctionSpace<SDGV, StaggeredQ0FEM, VelocityConstraints, VectorBackend> StaggeredQ0GFS;
    StaggeredQ0GFS staggeredQ0Gfs0(sdgv0, staggeredq0fem, velocityConstraints);
    staggeredQ0Gfs0.name("velocity");
    typedef Dune::PDELab::FixPressureConstraints<MDGV, BCType> PressureConstraints;
    PressureConstraints pressureConstraints(mdgv, bc);
    typedef Dune::PDELab::GridFunctionSpace<SDGV, P0FEM, PressureConstraints, VectorBackend> P0GFS;
    P0GFS p0gfs0(sdgv0, p0fem, pressureConstraints);
    p0gfs0.name("pressure");
    typedef Dune::PDELab::GridFunctionSpace<SDGV, P0FEM, Dune::PDELab::NoConstraints, VectorBackend> P0DarcyGFS;
    P0DarcyGFS p0gfs1(sdgv1, p0fem);
    p0gfs1.name("pressure");
    // composed function space
    typedef Dune::PDELab::CompositeGridFunctionSpace<VectorBackend,
      Dune::PDELab::LexicographicOrderingTag, StaggeredQ0GFS, P0GFS> ComposedGFS;
    ComposedGFS composedGfs0(staggeredQ0Gfs0, p0gfs0);

    typedef Dune::PDELab::MultiDomain::MultiDomainGridFunctionSpace<
      Grid,
      VectorBackend,
      Dune::PDELab::LexicographicOrderingTag,
      ComposedGFS,
      P0DarcyGFS
      > MultiGFS;
    MultiGFS multigfs(grid, composedGfs0, p0gfs1);

    // make grid function operator
    // for Stokes
    typedef Dune::PDELab::NavierStokesStaggeredGrid<BCType,
      SourceMomentumBalance<MDGV, RF>, SourceMassBalance<MDGV, RF>,
      DirichletVelocity<MDGV, RF>, DirichletPressure<MDGV, RF>,
      NeumannVelocity<MDGV, RF>, NeumannPressure<MDGV, RF>, MDGV> LOpStokes;
    LOpStokes lopStokes(bc, sourceMomentumBalance, sourceMassBalance,
            dirichletVelocity, dirichletPressure, neumannVelocity, neumannPressure, mdgv);

    typedef Dune::PDELab::NavierStokesTransientStaggeredGrid<MDGV> TLOpStokes;
    TLOpStokes tlopStokes(mdgv);

    // for Darcy
    typedef Dune::PDELab::DiffusionCCFV<DarcyK<MDGV, RF>, DarcyA0<MDGV, RF>,
      DarcySource<MDGV, RF>, DarcyBC<MDGV>, DarcyJ<MDGV, RF>, DarcyG<MDGV, RF> > LOpDarcy;
    LOpDarcy lopDarcy(darcyK, darcyA0, darcySource, darcyBC, darcyJ, darcyG);

    typedef Dune::PDELab::L2<MDGV> TLOpDarcy;
    TLOpDarcy tlopDarcy(mdgv);

    typedef Dune::PDELab::MultiDomain::SubDomainEqualityCondition<Grid> Condition;
    Condition c0(0);
    Condition c1(1);

    typedef Dune::PDELab::MultiDomain::SubProblem
      <MultiGFS, MultiGFS, LOpStokes, Condition, 0> UpperSubProblem_dt0;
    UpperSubProblem_dt0 upper_sp_dt0(lopStokes, c0);

    typedef Dune::PDELab::MultiDomain::SubProblem
      <MultiGFS, MultiGFS, TLOpStokes, Condition, 0> UpperSubProblem_dt1;
    UpperSubProblem_dt1 upper_sp_dt1(tlopStokes, c0);

    typedef Dune::PDELab::MultiDomain::SubProblem
      <MultiGFS, MultiGFS, LOpDarcy, Condition, 1> LowerSubProblem_dt0;
    LowerSubProblem_dt0 lower_sp_dt0(lopDarcy, c1);

    typedef Dune::PDELab::MultiDomain::SubProblem
      <MultiGFS, MultiGFS, TLOpDarcy, Condition, 1> LowerSubProblem_dt1;
    LowerSubProblem_dt1 lower_sp_dt1(tlopDarcy, c1);

    CouplingStokes1pDarcy1p<RF> couplingStokes1pDarcy1p;

    typedef Dune::PDELab::MultiDomain::Coupling
      <UpperSubProblem_dt0, LowerSubProblem_dt0, CouplingStokes1pDarcy1p<RF> > Coupling;
    Coupling coupling(upper_sp_dt0, lower_sp_dt0, couplingStokes1pDarcy1p);

    auto constraints = Dune::PDELab::MultiDomain::constraints<RF>(multigfs,
      Dune::PDELab::MultiDomain::constrainSubProblem(upper_sp_dt0, bc),
      Dune::PDELab::MultiDomain::constrainSubProblem(lower_sp_dt0, darcyBC));

    typedef MultiGFS::ConstraintsContainer<RF>::Type ConstraintsContainer;
    ConstraintsContainer constraintsContainer;
    constraints.assemble(constraintsContainer);

    typedef Dune::PDELab::ISTLMatrixBackend MatrixBackend;

    typedef Dune::PDELab::MultiDomain::GridOperator<
      MultiGFS, MultiGFS,
      MatrixBackend, RF, RF, RF, ConstraintsContainer, ConstraintsContainer,
      UpperSubProblem_dt0,
      LowerSubProblem_dt0,
      Coupling
      > GridOperator_dt0;

    typedef Dune::PDELab::MultiDomain::GridOperator<
      MultiGFS, MultiGFS,
      MatrixBackend, RF, RF, RF, ConstraintsContainer, ConstraintsContainer,
      UpperSubProblem_dt1,
      LowerSubProblem_dt1
      > GridOperator_dt1;

    typedef Dune::PDELab::OneStepGridOperator<GridOperator_dt0,GridOperator_dt1> GridOperator;

    // make coefficent Vector and initialize it from a function
    typedef typename Dune::PDELab::BackendVectorSelector<MultiGFS, DF>::Type V;
    V xOld(multigfs);
    xOld = 0.0;
    Dune::PDELab::MultiDomain::interpolateOnTrialSpace(multigfs, xOld,
      dirichletComposed, upper_sp_dt0, darcyG, lower_sp_dt0);

#ifdef ANALYTIC_SOLUTION
        // clear interior
        Dune::PDELab::set_nonconstrained_dofs(constraintsContainer, 0.0, xOld);
#endif
    std::cout << "interpolation: " << xOld.N() << " dof total, " << constraintsContainer.size() << " dof constrained" << std::endl;

    GridOperator_dt0 gridoperator_dt0(multigfs, multigfs,
      constraintsContainer, constraintsContainer,
      upper_sp_dt0,
      lower_sp_dt0,
      coupling);

    GridOperator_dt1 gridoperator_dt1(multigfs, multigfs,
      constraintsContainer, constraintsContainer,
      upper_sp_dt1,
      lower_sp_dt1);

    GridOperator gridoperator(gridoperator_dt0,gridoperator_dt1);

    // <<<5>>> Select a linear solver backend
#if HAVE_SUPERLU
    typedef Dune::PDELab::ISTLBackend_SEQ_SuperLU LinearSolver;
#else
#error No direct linear solver, use SuperLU.
#endif
    LinearSolver ls(false);

    // <<<6>>> Solve nonlinear problem
    typedef Dune::PDELab::Newton<GridOperator, LinearSolver, V> NEWTONSOLVER;
    // !
    std::ofstream file_newton("Ftt_newton_iterations.txt", std::ios::out|std::ios::app);
    file_newton << "-- global Newton solver for r = " << refinementLevel << std::endl;
    NEWTONSOLVER newton(gridoperator, xOld, ls);
    newton.setVerbosityLevel(2);
    newton.setMaxIterations(25);
    newton.setReduction(1e-9);
    newton.setLineSearchStrategy(newton.noLineSearch);

    // <<7>> time stepper
    Dune::PDELab::ImplicitEulerParameter<RF> method; // defines coefficients
    Dune::PDELab::OneStepMethod<RF, GridOperator, NEWTONSOLVER,V,V> osm(method,gridoperator,newton);
    osm.setVerbosityLevel(2);  // time stepping scheme

    // <<<8>>> graphics for initial guess
    typedef Dune::PDELab::GridFunctionSubSpace
      <MultiGFS, Dune::TypeTree::TreePath<indexUpper, indexVelocity> > VSUB;
    VSUB vsub(multigfs);                   // velocity subspace

    typedef Dune::PDELab::GridFunctionSubSpace
      <MultiGFS, Dune::TypeTree::TreePath<indexUpper, indexPressure> > PSUB;
    PSUB psub(multigfs);                   // pressure subspace

    // make discrete function object
    typedef Dune::PDELab::DiscreteGridFunction<VSUB, V> StaggeredQ0DGF;
    StaggeredQ0DGF staggeredQ0DGF(vsub, xOld);
    typedef Dune::PDELab::DiscreteGridFunction<PSUB, V> P0DGF;
    P0DGF p0dgf(psub, xOld);

    Dune::PDELab::FilenameHelper fn_left("test_pdelabcoupledtransient_upper");
    Dune::PDELab::FilenameHelper fn_right("test_pdelabcoupledtransient_lower");

    // plot result as VTK
    Dune::SubsamplingVTKWriter<SDGV> vtkwriterStokes(sdgv0, 1);
    vtkwriterStokes.addCellData(new Dune::PDELab::VTKGridFunctionAdapter<StaggeredQ0DGF>(staggeredQ0DGF, "velocity"));
    vtkwriterStokes.addCellData(new Dune::PDELab::VTKGridFunctionAdapter<P0DGF>(p0dgf, "pressure"));

    vtkwriterStokes.write(fn_left.getName(), Dune::VTK::ascii);
    fn_left.increment();

    typedef Dune::PDELab::GridFunctionSubSpace
      <MultiGFS, Dune::TypeTree::TreePath<indexLower> > LowerGFS;
    LowerGFS lowerGFS(multigfs);                   // velocity subspace
    // make discrete function object
    typedef Dune::PDELab::DiscreteGridFunction<LowerGFS, V> P0DarcyDGF;
    P0DarcyDGF p0dgfDarcy(lowerGFS, xOld);

    // plot result as VTK
    Dune::VTKWriter<SDGV> vtkwriterDarcy(sdgv1, Dune::VTK::nonconforming);
    vtkwriterDarcy.addCellData(new Dune::PDELab::VTKGridFunctionAdapter<P0DarcyDGF>(p0dgfDarcy, "pressure"));

    vtkwriterDarcy.write(fn_right.getName(), Dune::VTK::ascii);
    fn_right.increment();

    // <<9>> time loop
    RF time = 0;
#ifdef ANALYTIC_SOLUTION
            const RF dt_ini = M_PI*0.05;//parameters.get<double>("discretization.dt");
            const RF tEnd = M_PI*0.5; //parameters.get<double>("discretization.tEnd");
#else
            const RF dt_ini = 16*0.05*M_PI;//parameters.get<double>("discretization.dt");
            const RF tEnd = 16*0.5*M_PI; //parameters.get<double>("discretization.tEnd");
#endif
    const RF factor = atof(argv[2]);
    const RF dt = dt_ini * factor;
    V xNew(xOld);
    int k = 0;

    auto start = std::chrono::high_resolution_clock::now();

    while (time < tEnd - 1e-8) {
        // do time step
        k += 1;

        auto f_ = Dune::PDELab::MultiDomain::interpolateOnSubProblems(dirichletComposed,upper_sp_dt0,darcyG,lower_sp_dt0);

        osm.apply(time,dt,xOld,f_,xNew);    // do one time step

        // graphics

        {   // Stokes
            Dune::SubsamplingVTKWriter<SDGV> vtkwriterStokes(sdgv0, 1);
            vtkwriterStokes.addCellData(new Dune::PDELab::VTKGridFunctionAdapter<StaggeredQ0DGF>(staggeredQ0DGF, "velocity"));
            vtkwriterStokes.addCellData(new Dune::PDELab::VTKGridFunctionAdapter<P0DGF>(p0dgf, "pressure"));

            vtkwriterStokes.write(fn_left.getName(), Dune::VTK::ascii);
            fn_left.increment();
        }

        {   // Darcy
            Dune::VTKWriter<SDGV> vtkwriterDarcy(sdgv1, Dune::VTK::nonconforming);
            vtkwriterDarcy.addCellData(new Dune::PDELab::VTKGridFunctionAdapter<P0DarcyDGF>(p0dgfDarcy, "pressure"));

            vtkwriterDarcy.write(fn_right.getName(), Dune::VTK::ascii);
            fn_right.increment();
        }

        xOld = xNew;                    // advance in time step
        time += dt;
    } //while (time < tEnd - 1e-8)

    auto end = std::chrono::high_resolution_clock::now();

    auto elapsed = end - start;

    std::cout << std::endl
            << std::endl
            << "monolithic solver needed " << std::chrono::duration_cast<std::chrono::duration<double> >(elapsed).count() <<"s." <<std::endl
            << std::endl;

    // compute L2 error if analytical solution is available
    DirichletVelocity<SDGV, RF> dirichletVelocitySubdomain(sdgv0);
    dirichletVelocitySubdomain.setTime(time);
    DirichletPressure<SDGV, RF> dirichletPressureSubdomain(sdgv0);
    DarcyG<SDGV, RF> darcyPressureSubdomain(sdgv1);
    std::cout.precision(8);
//    std::cout << "L2 error (Stokes) for "
//              << std::setw(6) << sdgv0.size(0)
//              << " elements. pressure: "
//              << std::scientific
//              << l2interpolationerror(dirichletPressureSubdomain, psub, xOld, 8)
//              << " velocity: "
//              << std::scientific
//              << l2interpolationerror(dirichletVelocitySubdomain, vsub, xOld, 8)
//              << std::endl
//              << "L2 error (Darcy)  for "
//              << std::setw(6) << sdgv1.size(0)
//              << " elements. pressure: "
//              << std::scientific
//              << l2interpolationerror(darcyPressureSubdomain, lowerGFS, xOld, 8)
//              << std::endl;

    std::ofstream file("results_pdelabcoupledtransient.txt", std::ios::out|std::ios::app);
    file << refinementLevel << "\t"
            << std::chrono::duration_cast<std::chrono::duration<double> >(elapsed).count() << "\t"
            << std::scientific << l2interpolationerror(dirichletPressureSubdomain, psub, xOld, 8) << "\t"
            << std::scientific << l2interpolationerror(dirichletVelocitySubdomain, vsub, xOld, 8) << "\t"
            << std::scientific << l2interpolationerror(darcyPressureSubdomain, lowerGFS, xOld, 8) << std::endl;

    return EXIT_SUCCESS;
  }
  catch (Dune::Exception &e){
    std::cerr << "Dune reported error: " << e << std::endl;
    return 1;
  }
  catch (std::exception e) {
    std::cerr << "Dune reported std error: " << e.what() << std::endl;
  }
  catch (...){
    std::cerr << "Unknown exception thrown!" << std::endl;
    return 1;
  }
}
