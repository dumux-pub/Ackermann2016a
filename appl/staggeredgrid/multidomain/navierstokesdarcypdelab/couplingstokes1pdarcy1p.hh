#ifndef DUMUX_MULTIDOMAIN_PDELAB_COUPLING_STOKES_1P_DARCY_1P_HH
#define DUMUX_MULTIDOMAIN_PDELAB_COUPLING_STOKES_1P_DARCY_1P_HH

#include <dune/geometry/quadraturerules.hh>

#include <dune/pdelab/localoperator/idefault.hh>
#include <dune/pdelab/localoperator/pattern.hh>

#include <dune/pdelab/multidomain/couplingutilities.hh>

#include "diffusionccfv.hh"

/**
 * \tparam TReal Scalar value type.
 * \tparam SolutionVector Solution vector type, needed to set Dirichlet constrained values.
 * \tparam GFS Global grid function space, needed to set Dirichlet constrained values.
 */
template<typename TReal>
class CouplingStokes1pDarcy1p
  : public Dune::PDELab::MultiDomain::CouplingOperatorDefaultFlags
  , public Dune::PDELab::MultiDomain::NumericalJacobianCoupling<CouplingStokes1pDarcy1p<TReal> >
  , public Dune::PDELab::MultiDomain::NumericalJacobianApplyCoupling<CouplingStokes1pDarcy1p<TReal> >
  , public Dune::PDELab::MultiDomain::FullCouplingPattern
  , public Dune::PDELab::InstationaryLocalOperatorDefaultMethods<TReal>
{
public:
  static const bool doAlphaCoupling = true;
  static const bool doPatternCoupling = true;

  //! \brief Index of velocity
  static const unsigned int velocityIdx = 0;
  static const unsigned int pressureIdx = 1;

  template<typename IG, typename LFSUStokes, typename LFSUDarcy,
           typename X, typename LFSVStokes, typename LFSVDarcy,
           typename R>
  void alpha_coupling(const IG& ig,
                      const LFSUStokes& lfsu_s, const X& x_s, const LFSVStokes& lfsv_s,
                      const LFSUDarcy& lfsu_n, const X& x_n, const LFSVDarcy& lfsv_n,
                      R& r_s, R& r_n) const
  {
    // select the velocity component from the subspaces for Navier-Stokes
    // other components are not needed
    typedef typename LFSUStokes::template Child<velocityIdx>::Type LFSU_V;
    const LFSU_V& lfsu_v_s = lfsu_s.template child<velocityIdx>();

    // domain and range field type
    typedef typename LFSU_V::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::DomainFieldType DF;
    typedef typename LFSU_V::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::RangeFieldType RF;
    typedef typename LFSU_V::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::RangeType RangeVelocity;

    // /////////////////////
    // geometry information

    // dimension
    static const unsigned int dim = IG::Geometry::dimension;

    // local position of face center and face normal
    const Dune::FieldVector<DF, dim-1>& faceCenterLocal =
      Dune::ReferenceElements<DF, dim-1>::general(ig.geometry().type()).position(0, 0);
    const Dune::FieldVector<DF, dim>& faceUnitOuterNormal = ig.centerUnitOuterNormal();

    // evaluate orientation of intersection
    unsigned int normDim = 0;
    unsigned int tangDim = 1;
    for (unsigned int curDim = 0; curDim < dim; ++curDim)
    {
      if (std::abs(faceUnitOuterNormal[curDim]) > 1e-10 && dim > 1)
      {
        normDim = curDim;
        tangDim = 1 - curDim;
      }
    }

    // face midpoints of all faces
    const unsigned int numFaces =
      Dune::ReferenceElements<DF, dim>::general(ig.inside()->type()).size(1);
    std::vector<Dune::FieldVector<DF, dim> > faceCentersLocal_s(numFaces);
    std::vector<Dune::FieldVector<DF, dim> > faceCentersLocal_n(numFaces);
    std::vector<Dune::FieldVector<DF, dim> > faceCentersGlobal_s(numFaces);
    std::vector<Dune::FieldVector<DF, dim> > faceCentersGlobal_n(numFaces);
    for (unsigned int curFace = 0; curFace < numFaces; ++curFace)
    {
      faceCentersLocal_s[curFace] =
        Dune::ReferenceElements<DF, dim>::general(ig.inside()->geometry().type()).position(curFace, 1);
      faceCentersLocal_n[curFace] =
        Dune::ReferenceElements<DF, dim>::general(ig.outside()->geometry().type()).position(curFace, 1);
      faceCentersGlobal_s[curFace] = ig.inside()->geometry().global(faceCentersLocal_s[curFace]);
      faceCentersGlobal_n[curFace] = ig.outside()->geometry().global(faceCentersLocal_n[curFace]);
    }

    // face volume for integration
    RF faceVolume = ig.geometry().integrationElement(faceCenterLocal)
                    * Dune::ReferenceElements<DF, dim-1>::general(ig.geometry().type()).volume();

    // /////////////////////
    // velocities

    // evaluate shape functions at all face midpoints
    std::vector<std::vector<RangeVelocity> > velocityBasis_s(numFaces);
    for (unsigned int curFace = 0; curFace < numFaces; ++curFace)
    {
      velocityBasis_s[curFace].resize(lfsu_v_s.size());
      lfsu_v_s.finiteElement().localBasis().evaluateFunction(
        faceCentersLocal_s[curFace], velocityBasis_s[curFace]);
    }

    // evaluate velocity on midpoints of all faces
    std::vector<RangeVelocity> velocities_s(numFaces);
    for (unsigned int curFace = 0; curFace < numFaces; ++curFace)
    {
      velocities_s[curFace] = RangeVelocity(0.0);
      for (unsigned int i = 0; i < lfsu_v_s.size(); i++)
      {
        velocities_s[curFace].axpy(x_s(lfsu_v_s, i), velocityBasis_s[curFace][i]);
      }
    }

    // /////////////////////
    // evaluation of unknown, upwinding and averaging

    // evaluate cell values
    DF pressure_n = x_n(lfsu_n, 0);

    // upwinding on staggered intersection
    std::vector<RF> velocity_s_up(dim);
    for (unsigned int curDim = 0; curDim < dim; ++curDim)
    {
      velocity_s_up[curDim] = velocities_s[curDim*2][curDim];
      if (velocity_s_up[curDim] < 0)
      {
        velocity_s_up[curDim] = velocities_s[curDim*2+1][curDim];
      }
    }

    //! \todo TODO: remove hard coded gravitional acceleration
//         Dune::FieldVector<DF, dim> gravity(0.0);
//         gravity[dim-1] = -9.81;

    // normal velocity
    RF normalAtInterfaceVelocity_s = velocities_s[ig.indexInInside()] * faceUnitOuterNormal;

    RF fromPermeability = std::pow(1.0, -0.5); // TODO: take value from DarcyK function
    RF alphaBeaversJoseph = 1.0;

    unsigned int indexTangentialVelocity0 = (1 - normDim) * 2; // TODO: extend to 3d
    unsigned int indexTangentialVelocity1 = indexTangentialVelocity0 + 1;

    // Neumann boundary condition for normal momentum equation of Stokes
    // (continuity of stress in normal direction)
    r_s.accumulate(lfsu_v_s, ig.indexInInside(),
                    faceUnitOuterNormal[normDim]
                    * pressure_n
                    * faceVolume);

    // Robin boundary condition for tangential momentum equation of Stokes
    // (Beavers-Joseph-Saffman condition)
    r_s.accumulate(lfsu_v_s, indexTangentialVelocity0,
                   -0.5 * alphaBeaversJoseph
                   * fromPermeability
                   * velocities_s[2*tangDim][tangDim]
                   * faceVolume);
    r_s.accumulate(lfsu_v_s, indexTangentialVelocity1,
                   -0.5 * alphaBeaversJoseph
                   * fromPermeability
                   * velocities_s[2*tangDim+1][tangDim]
                   * faceVolume);

    // pressure for Darcy (continuity of normal mass fluxes)
    r_n.accumulate(lfsu_n, 0,
                   -1.0 * normalAtInterfaceVelocity_s
                   * faceVolume);
  }
};

#endif // DUMUX_MULTIDOMAIN_PDELAB_COUPLING_STOKES_1P_DARCY_1P_HH
