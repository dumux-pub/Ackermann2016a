#include "config.h"

#include <dune/grid/sgrid.hh>
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>

#include <dune/pdelab/backend/istlmatrixbackend.hh>
#include <dune/pdelab/backend/istlsolverbackend.hh>
#include <dune/pdelab/backend/istlvectorbackend.hh>
#include <dune/pdelab/common/vtkexport.hh>
#include <dune/pdelab/finiteelementmap/p0fem.hh>
#include <dune/pdelab/gridfunctionspace/subspace.hh>
#include <dune/pdelab/gridoperator/onestep.hh>
#include <dune/pdelab/instationary/onestep.hh>
#include <dune/pdelab/stationary/linearproblem.hh>

#include <dune/pdelab/multidomain/constraints.hh>
#include <dune/pdelab/multidomain/coupling.hh>
#include <dune/pdelab/multidomain/gridoperator.hh>
#include <dune/pdelab/multidomain/interpolate.hh>
#include <dune/pdelab/multidomain/multidomaingridfunctionspace.hh>
#include <dune/pdelab/multidomain/subproblem.hh>
#include <dune/pdelab/multidomain/subproblemlocalfunctionspace.hh>
#include <dune/pdelab/multidomain/vtk.hh>

#define ENABLE_NAVIER_STOKES 1
#define KINEMATIC_VISCOSITY 1e-2

#include "../../common_pdelab/fixpressureconstraints.hh"
#include "../../common_pdelab/fixvelocityconstraints.hh"
#include "../../common_pdelab/l2interpolationerror.hh"
#include "../../localfunctions/staggeredq0fem.hh"
#include "../../freeflow/navierstokes/navierstokes_pdelab/problemliddrivencavity.hh"
// #include "../../navierstokes_pdelab/problemstokesdoneahuerta.hh"
#include "../../freeflow/navierstokes/navierstokes_pdelab/navierstokesstaggeredgrid.hh"
#include "../../freeflow/navierstokes/navierstokes_pdelab/navierstokestransientstaggeredgrid.hh"

#include "stokesstokescoupling.hh"


int main(int argc, char** argv)
{
  unsigned int refinementLevel = 2;
  if (argc < 2)
  {
    std::cout << "Usage: " << argv[0] << " <refinement level>" << std::endl;
    std::cout << "Defaulting refinement level to " << refinementLevel << std::endl;
  }
  else
  {
    refinementLevel = atoi(argv[1]);
  }

  try
  {
    // set up grid
    const int dim = 2;
    typedef Dune::SGrid<dim, dim, double> BaseGrid;
    Dune::FieldVector<double, dim> low(0.0);
    Dune::FieldVector<double, dim> high(1.0);
    Dune::FieldVector<int, dim> n(2);
    BaseGrid baseGrid(n, low, high);
    baseGrid.globalRefine(refinementLevel);
    typedef Dune::MultiDomainGrid<BaseGrid,Dune::mdgrid::FewSubDomainsTraits<BaseGrid::dimension,4> > Grid;
    Grid grid(baseGrid,false);
    typedef Grid::SubDomainGrid SubDomainGrid;
    SubDomainGrid& sdg0 = grid.subDomain(0);
    SubDomainGrid& sdg1 = grid.subDomain(1);
    typedef Grid::LeafGridView MDGV;
    typedef SubDomainGrid::LeafGridView SDGV;
    MDGV mdgv = grid.leafGridView();
    // left subdomain
    SDGV sdgv0 = sdg0.leafGridView();
    // right subdomain
    SDGV sdgv1 = sdg1.leafGridView();
    sdg0.hostEntityPointer(*sdgv0.begin<0>());
    grid.startSubDomainMarking();
    for (MDGV::Codim<0>::Iterator it = mdgv.begin<0>(); it != mdgv.end<0>(); ++it)
    {
      if (it->geometry().center()[0] > 0.5)
      {
        grid.addToSubDomain(1,*it);
      }
      else
      {
        grid.addToSubDomain(0,*it);
      }
    }
    grid.preUpdateSubDomains();
    grid.updateSubDomains();
    grid.postUpdateSubDomains();

    std::cout << "grid setup" << std::endl;

    // types and constants
    typedef double DF;
    typedef double RF;
    const int indexVelocity = 0;
    const int indexPressure = 1;
    const int indexLeft = 0;
    const int indexRight = 1;

    // instantiate finite element maps
    typedef Dune::PDELab::P0LocalFiniteElementMap<DF, RF, dim> P0FEM;
    P0FEM p0fem(Dune::GeometryType(Dune::GeometryType::cube, dim));
    typedef Dune::PDELab::StaggeredQ0LocalFiniteElementMap<DF, RF, dim> StaggeredQ0FEM;
    StaggeredQ0FEM staggeredq0fem;

    // set up functions defining the problem
    typedef Dune::PDELab::CompositeConstraintsParameters<BCVelocity, BCPressure> BCType;
    BCVelocity bcVelocity;
    BCPressure bcPressure;
    BCType bc(bcVelocity, bcPressure);

    // construct grid function spaces
    typedef Dune::PDELab::ISTLVectorBackend<> VectorBackend;
    typedef Dune::PDELab::FixVelocityConstraints VelocityConstraints;
    VelocityConstraints velocityConstraints;
    typedef Dune::PDELab::GridFunctionSpace<SDGV, StaggeredQ0FEM, VelocityConstraints, VectorBackend> StaggeredQ0GFS;
    StaggeredQ0GFS staggeredQ0Gfs0(sdgv0, staggeredq0fem, velocityConstraints);
    StaggeredQ0GFS staggeredQ0Gfs1(sdgv1, staggeredq0fem, velocityConstraints);
    staggeredQ0Gfs0.name("velocity");
    staggeredQ0Gfs1.name("velocity");
    typedef Dune::PDELab::FixPressureConstraints<MDGV, BCType> PressureConstraints;
    PressureConstraints pressureConstraints(mdgv, bc);
    typedef Dune::PDELab::GridFunctionSpace<SDGV, P0FEM, PressureConstraints, VectorBackend> P0GFS;
    P0GFS p0gfs0(sdgv0, p0fem, pressureConstraints);
    P0GFS p0gfs1(sdgv1, p0fem, pressureConstraints);
    p0gfs0.name("pressure");
    p0gfs1.name("pressure");
    // composed function space
    typedef Dune::PDELab::CompositeGridFunctionSpace<VectorBackend,
      Dune::PDELab::LexicographicOrderingTag, StaggeredQ0GFS, P0GFS> ComposedGFS;
    ComposedGFS composedGfs0(staggeredQ0Gfs0, p0gfs0);
    ComposedGFS composedGfs1(staggeredQ0Gfs1, p0gfs1);

    typedef Dune::PDELab::MultiDomain::MultiDomainGridFunctionSpace<
      Grid,
      VectorBackend,
      Dune::PDELab::LexicographicOrderingTag,
      ComposedGFS,
      ComposedGFS
      > MultiGFS;
    MultiGFS multigfs(grid,composedGfs0, composedGfs1);

    std::cout << "function space setup" << std::endl;

    typedef MultiGFS::ConstraintsContainer<RF>::Type ConstraintsContainer;
    ConstraintsContainer constraintsContainer;

    DirichletVelocity<MDGV, RF> dirichletVelocity(mdgv);
    DirichletPressure<MDGV, RF> dirichletPressure(mdgv);
    NeumannVelocity<MDGV, RF> neumannVelocity(mdgv);
    NeumannPressure<MDGV, RF> neumannPressure(mdgv);
    SourceMomentumBalance<MDGV, RF> sourceMomentumBalance(mdgv);
    SourceMassBalance<MDGV, RF> sourceMassBalance(mdgv);
    typedef Dune::PDELab::CompositeGridFunction
      <DirichletVelocity<MDGV, RF>, DirichletPressure<MDGV, RF> > DirichletComposed;
    DirichletComposed dirichletComposed(dirichletVelocity, dirichletPressure);

    // make grid function operator
    typedef Dune::PDELab::NavierStokesStaggeredGrid<BCType,
      SourceMomentumBalance<MDGV, RF>, SourceMassBalance<MDGV, RF>,
      DirichletVelocity<MDGV, RF>, DirichletPressure<MDGV, RF>,
      NeumannVelocity<MDGV, RF>, NeumannPressure<MDGV, RF>, MDGV> LOP;
    LOP lop(bc, sourceMomentumBalance, sourceMassBalance,
            dirichletVelocity, dirichletPressure, neumannVelocity, neumannPressure, mdgv);
    typedef Dune::PDELab::NavierStokesTransientStaggeredGrid<MDGV> TransientLocalOperator;
    TransientLocalOperator transientLocalOperator(mdgv);

    typedef Dune::PDELab::MultiDomain::SubDomainEqualityCondition<Grid> Condition;

    Condition c0(0);
    Condition c1(1);

    typedef Dune::PDELab::MultiDomain::SubProblem
      <MultiGFS, MultiGFS, LOP, Condition, 0> LeftSubProblem_dt0;
    typedef Dune::PDELab::MultiDomain::SubProblem
      <MultiGFS, MultiGFS, TransientLocalOperator, Condition, 0> LeftSubProblem_dt1;

    LeftSubProblem_dt0 left_sp_dt0(lop, c0);
    LeftSubProblem_dt1 left_sp_dt1(transientLocalOperator, c0);

    typedef Dune::PDELab::MultiDomain::SubProblem
      <MultiGFS, MultiGFS, LOP, Condition, 1> RightSubProblem_dt0;
    typedef Dune::PDELab::MultiDomain::SubProblem
      <MultiGFS, MultiGFS, TransientLocalOperator, Condition, 1> RightSubProblem_dt1;

    RightSubProblem_dt0 right_sp_dt0(lop, c1);
    RightSubProblem_dt1 right_sp_dt1(transientLocalOperator, c1);

    StokesStokesCoupling<RF> stokesStokesCoupling;

    typedef Dune::PDELab::MultiDomain::Coupling
      <LeftSubProblem_dt0, RightSubProblem_dt0, StokesStokesCoupling<RF> > Coupling;
    Coupling coupling(left_sp_dt0,right_sp_dt0,stokesStokesCoupling);

    std::cout << "subproblem / coupling setup" << std::endl;

    auto constraints = Dune::PDELab::MultiDomain::constraints<RF>(multigfs,
      Dune::PDELab::MultiDomain::constrainSubProblem(left_sp_dt0, bc),
      Dune::PDELab::MultiDomain::constrainSubProblem(right_sp_dt0, bc));

    constraints.assemble(constraintsContainer);

    std::cout << "constraints evaluation" << std::endl;

    typedef Dune::PDELab::ISTLMatrixBackend MatrixBackend;

    typedef Dune::PDELab::MultiDomain::GridOperator<
      MultiGFS, MultiGFS,
      MatrixBackend, RF, RF, RF, ConstraintsContainer, ConstraintsContainer,
      LeftSubProblem_dt0,
      RightSubProblem_dt0,
      Coupling
      > GridOperator_dt0;

    typedef Dune::PDELab::MultiDomain::GridOperator<
      MultiGFS, MultiGFS,
      MatrixBackend, RF, RF, RF, ConstraintsContainer, ConstraintsContainer,
      LeftSubProblem_dt1,
      RightSubProblem_dt1
      > GridOperator_dt1;

    typedef Dune::PDELab::OneStepGridOperator<GridOperator_dt0, GridOperator_dt1> GridOperator;

    // make coefficent Vector and initialize it from a function
    typedef GridOperator::Traits::Domain V;
    V xOld(multigfs);
    xOld = 0.0;
    Dune::PDELab::MultiDomain::interpolateOnTrialSpace(multigfs, xOld,
      dirichletComposed, left_sp_dt0, dirichletComposed, right_sp_dt0);

    // clear interior
    Dune::PDELab::set_nonconstrained_dofs(constraintsContainer, 0.0, xOld);

    std::cout << "interpolation: " << xOld.N() << " dof total, " << constraintsContainer.size() << " dof constrained" << std::endl;

    GridOperator_dt0 go_dt_0(multigfs, multigfs,
      constraintsContainer, constraintsContainer,
      left_sp_dt0,
      right_sp_dt0,
      coupling);

    GridOperator_dt1 go_dt_1(multigfs, multigfs,
      constraintsContainer, constraintsContainer,
      left_sp_dt1,
      right_sp_dt1);

    GridOperator gridoperator(go_dt_0, go_dt_1);

    std::cout << "operator space setup" << std::endl;

    // <<<5>>> Select a linear solver backend
    typedef Dune::PDELab::ISTLBackend_SEQ_SuperLU LinearSolver;
    LinearSolver ls(false);

    // <<<6>>> Solve (possibly) nonlinear problem
    typedef typename Dune::PDELab::Newton<GridOperator, LinearSolver, V> Newton;
    Newton newton(gridoperator, ls);
    newton.setReassembleThreshold(0.0);
    newton.setVerbosityLevel(20);
    newton.setMaxIterations(25);
    newton.setLineSearchMaxIterations(30);
    newton.setReduction(1e-3);
    newton.setLineSearchStrategy(newton.noLineSearch);

    // <<<7>>> time stepper / implicit Euler scheme
    Dune::PDELab::ImplicitEulerParameter<RF> method;               // defines coefficients
    Dune::PDELab::OneStepMethod<RF, GridOperator, Newton, V, V> osm(method, gridoperator, newton);
    osm.setVerbosityLevel(2);

    // <<<6>>> Solver for linear problem per stage
    typedef Dune::PDELab::StationaryLinearProblemSolver<GridOperator, LinearSolver, V> StationaryProblemSolver;
    StationaryProblemSolver pdesolver(gridoperator, ls, 1e-10);

    // <<<8>>> graphics for initial guess
    Dune::PDELab::FilenameHelper filenameLeft("test_stokesstokes_left");
    Dune::PDELab::FilenameHelper filenameRight("test_stokesstokes_right");

    {
      typedef Dune::PDELab::GridFunctionSubSpace
        <MultiGFS, Dune::TypeTree::TreePath<indexLeft, indexVelocity> > VSUB;
      VSUB vsub(multigfs);                   // velocity subspace

      typedef Dune::PDELab::GridFunctionSubSpace
        <MultiGFS, Dune::TypeTree::TreePath<indexLeft, indexPressure> > PSUB;
      PSUB psub(multigfs);                   // pressure subspace

      // make discrete function object
      typedef Dune::PDELab::DiscreteGridFunction<VSUB, V> StaggeredQ0DGF;
      StaggeredQ0DGF staggeredQ0DGF(vsub, xOld);
      typedef Dune::PDELab::DiscreteGridFunction<PSUB, V> P0DGF;
      P0DGF p0dgf(psub, xOld);

      // plot result as VTK
      Dune::SubsamplingVTKWriter<SDGV> vtkwriter(sdgv0, 1);
      vtkwriter.addCellData(new Dune::PDELab::VTKGridFunctionAdapter<StaggeredQ0DGF>(staggeredQ0DGF, "1velocity"));
      vtkwriter.addCellData(new Dune::PDELab::VTKGridFunctionAdapter<P0DGF>(p0dgf, "2pressure"));
      vtkwriter.write(filenameLeft.getName(),Dune::VTK::ascii);
      filenameLeft.increment();
    }

    {
      typedef Dune::PDELab::GridFunctionSubSpace
        <MultiGFS,Dune::TypeTree::TreePath<indexRight, indexVelocity> > VSUB;
      VSUB vsub(multigfs);                   // velocity subspace

      typedef Dune::PDELab::GridFunctionSubSpace
        <MultiGFS,Dune::TypeTree::TreePath<indexRight, indexPressure> > PSUB;
      PSUB psub(multigfs);                   // pressure subspace

      // make discrete function object
      typedef Dune::PDELab::DiscreteGridFunction<VSUB, V> StaggeredQ0DGF;
      StaggeredQ0DGF staggeredQ0DGF(vsub, xOld);
      typedef Dune::PDELab::DiscreteGridFunction<PSUB, V> P0DGF;
      P0DGF p0dgf(psub, xOld);

      // plot result as VTK
      Dune::SubsamplingVTKWriter<SDGV> vtkwriter(sdgv1, 1);
      vtkwriter.addCellData(new Dune::PDELab::VTKGridFunctionAdapter<StaggeredQ0DGF>(staggeredQ0DGF, "1velocity"));
      vtkwriter.addCellData(new Dune::PDELab::VTKGridFunctionAdapter<P0DGF>(p0dgf, "2pressure"));
      vtkwriter.write(filenameRight.getName(),Dune::VTK::ascii);
      filenameRight.increment();
    }


    // <<<9>>> time loop
    RF time = 0;
    const RF timeStep = 0.1;
    const RF timeEnd = 1.0;
    V xNew(xOld);                                              // solution to be computed

    while (time < timeEnd)
    {
      // do time step
      /*
        bctype.setTime(time+timeStep);                                       // compute constraints
        cc.clear();                                               // for this time step
        Dune::PDELab::constraints(bctype,gfs,cc);
      */

      auto f_ = Dune::PDELab::MultiDomain::interpolateOnSubProblems(
        dirichletComposed, left_sp_dt0, dirichletComposed, right_sp_dt0);

      osm.apply(time, timeStep, xOld, f_, xNew);                           // do one time step

      // graphics
      {
        typedef Dune::PDELab::GridFunctionSubSpace
          <MultiGFS, Dune::TypeTree::TreePath<indexLeft, indexVelocity> > VSUB;
        VSUB vsub(multigfs);                   // velocity subspace

        typedef Dune::PDELab::GridFunctionSubSpace
          <MultiGFS, Dune::TypeTree::TreePath<indexLeft, indexPressure> > PSUB;
        PSUB psub(multigfs);                   // pressure subspace

        // make discrete function object
        typedef Dune::PDELab::DiscreteGridFunction<VSUB, V> StaggeredQ0DGF;
        StaggeredQ0DGF staggeredQ0DGF(vsub, xOld);
        typedef Dune::PDELab::DiscreteGridFunction<PSUB, V> P0DGF;
        P0DGF p0dgf(psub, xOld);

        // plot result as VTK
        Dune::SubsamplingVTKWriter<SDGV> vtkwriter(sdgv0, 1);
        vtkwriter.addCellData(new Dune::PDELab::VTKGridFunctionAdapter<StaggeredQ0DGF>(staggeredQ0DGF, "1velocity"));
        vtkwriter.addCellData(new Dune::PDELab::VTKGridFunctionAdapter<P0DGF>(p0dgf, "2pressure"));
        vtkwriter.write(filenameLeft.getName(),Dune::VTK::ascii);
        filenameLeft.increment();
      }

      {
        typedef Dune::PDELab::GridFunctionSubSpace
          <MultiGFS,Dune::TypeTree::TreePath<indexRight, indexVelocity> > VSUB;
        VSUB vsub(multigfs);                   // velocity subspace

        typedef Dune::PDELab::GridFunctionSubSpace
          <MultiGFS,Dune::TypeTree::TreePath<indexRight, indexPressure> > PSUB;
        PSUB psub(multigfs);                   // pressure subspace

        // make discrete function object
        typedef Dune::PDELab::DiscreteGridFunction<VSUB, V> StaggeredQ0DGF;
        StaggeredQ0DGF staggeredQ0DGF(vsub, xOld);
        typedef Dune::PDELab::DiscreteGridFunction<PSUB, V> P0DGF;
        P0DGF p0dgf(psub, xOld);

        // plot result as VTK
        Dune::SubsamplingVTKWriter<SDGV> vtkwriter(sdgv1, 1);
        vtkwriter.addCellData(new Dune::PDELab::VTKGridFunctionAdapter<StaggeredQ0DGF>(staggeredQ0DGF, "1velocity"));
        vtkwriter.addCellData(new Dune::PDELab::VTKGridFunctionAdapter<P0DGF>(p0dgf, "2pressure"));
        vtkwriter.write(filenameRight.getName(),Dune::VTK::ascii);
        filenameRight.increment();
      }

      xOld = xNew;                                              // advance time step
      time += timeStep;
    }

    return EXIT_SUCCESS;
  }
  catch (Dune::Exception &e){
    std::cerr << "Dune reported error: " << e << std::endl;
    return 1;
  }
  catch (std::exception e) {
    std::cerr << "Dune reported std error: " << e.what() << std::endl;
  }
  catch (...){
    std::cerr << "Unknown exception thrown!" << std::endl;
    return 1;
  }

}
