#include "config.h"

#include <dune/common/parametertreeparser.hh>
#include <dune/grid/sgrid.hh>
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>

#include <dune/pdelab/backend/istl/bcrsmatrixbackend.hh>
#include <dune/pdelab/backend/istlsolverbackend.hh>
#include <dune/pdelab/backend/istlvectorbackend.hh>
#include <dune/pdelab/backend/seqistlsolverbackend.hh>
#include <dune/pdelab/common/vtkexport.hh>
#include <dune/pdelab/constraints/noconstraints.hh>
#include <dune/pdelab/finiteelementmap/p0fem.hh>
#include <dune/pdelab/gridfunctionspace/subspace.hh>
#include <dune/pdelab/newton/newton.hh>

#include <dune/pdelab/multidomain/constraints.hh>
#include <dune/pdelab/multidomain/coupling.hh>
#include <dune/pdelab/multidomain/gridoperator.hh>
#include <dune/pdelab/multidomain/interpolate.hh>
#include <dune/pdelab/multidomain/multidomaingridfunctionspace.hh>
#include <dune/pdelab/multidomain/subproblem.hh>
#include <dune/pdelab/multidomain/subproblemlocalfunctionspace.hh>
#include <dune/pdelab/multidomain/vtk.hh>

#include <dune/pdelab/stationary/linearproblem.hh>
#include <dune/pdelab/gridoperator/onestep.hh>
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>

#include <dune/pdelab/localoperator/diffusionccfv.hh>

#include "../../common_pdelab/l2interpolationerror.hh"
#include "../../../../../dune-multidomain/test/functionmacros.hh"
#include "couplingsina.hh"

// choose test case (original setup: x_length = 1.0, y_length = 2.0, x_refine = 1, y_refine = 2 in .ini-file)
#define TEST_CASE_SIMPLE // Ackermann2015
//#define TEST_CASE_COMPLEX // Muething2015 - use x_length = 1.0, y_length = 1.0, x_refine = 1, y_refine = 1 in .ini-file !!
//#define TEST_POLY // u = 3x^3 + 2y^2 - 5x^2y^2
//#define TEST_EXP // u = e^(-y) + x^3

// G - Dirichlet b.c.
SIMPLE_ANALYTIC_FUNCTION(G,x,y)
{
#if defined TEST_CASE_SIMPLE
    y = exp(x[0]*x[1]);
#elif defined TEST_CASE_COMPLEX
      typename Traits::DomainType center;
      for (int i=0; i<GV::dimension; i++) center[i] = 0.5;
      center -= x;
      y = exp(-center.two_norm2());
#elif defined TEST_POLY
      y = 3*x[0]*x[0]*x[0] + 2*x[1]*x[1] - 5*x[0]*x[0]*x[1]*x[1];
#elif defined TEST_EXP
      y = exp(-1.0*x[1]) +x[0]*x[0]*x[0];
#endif
}
END_SIMPLE_ANALYTIC_FUNCTION

// F0 - upper source term
SIMPLE_ANALYTIC_FUNCTION(F0,x,y)
{
#if defined TEST_SIMPLE
    y = -1.0*(x[0]*x[0] + x[1]*x[1] + 2) * exp(x[0] * x[1]);
#elif defined TEST_CASE_COMPLEX
    if (x[0]>0.25 && x[0]<0.375 && x[1]>0.25 && x[1]<0.375)
        y = 50.0;
    else {
        auto xm = x;
        xm = 0.5;
        xm -= x;
        if (xm.two_norm() < 0.2)
            y = 60 * std::exp(-10*x.two_norm2());
        else
            y = 0.0;
    }
#elif defined TEST_POLY
     y = -18*x[0] + 10*x[0]*x[0] + 10*x[1]*x[1] - 4;
#elif defined TEST_EXP
     y = -1.0 * exp(-1.0*x[1]) - 6.0 * x[0];
#endif
}
END_SIMPLE_ANALYTIC_FUNCTION

// F1 - lower source term
SIMPLE_ANALYTIC_FUNCTION(F1,x,y)
{
#if defined TEST_SIMPLE
    y = -1.0*(x[0]*x[0] + x[1]*x[1] + 2) * exp(x[0] * x[1]);
#elif defined TEST_CASE_COMPLEX
    if (x[0]>0.25 && x[0]<0.375 && x[1]>0.25 && x[1]<0.375)
        y = 50.0;
    else {
        auto xm = x;
        xm = 0.5;
        xm -= x;
        if (xm.two_norm() < 0.2)
            y = 60 * std::exp(-10*x.two_norm2());
        else
            y = 0.0;
    }
#elif defined TEST_POLY
     y = -18*x[0] + 10*x[0]*x[0] + 10*x[1]*x[1] - 4;
#elif defined TEST_EXP
     y = -1.0 * exp(-1.0*x[1]) - 6.0 * x[0];
#endif
}

END_SIMPLE_ANALYTIC_FUNCTION

// J - Neumann b.c.
SIMPLE_ANALYTIC_FUNCTION(J,x,y)
{
#if defined TEST_CASE_COMPLEX
    if (x[1]<1E-6 || x[1]>1.0-1E-6)
        {
        y = 0;
        return;
        }
    if (x[0]>1.0-1E-6 && x[1]>0.5+1E-6)
        {
        y = -5.0;
        return;
        }
    y = 0;
#else
    y = 0;
#endif
}
END_SIMPLE_ANALYTIC_FUNCTION

// K - permeability
SIMPLE_ANALYTIC_FUNCTION(K,x,y)
{
        y = 1.0;
}
END_SIMPLE_ANALYTIC_FUNCTION

// A0 - Helmholtz term
SIMPLE_ANALYTIC_FUNCTION(A0,x,y)
{
    y = 0;
}
END_SIMPLE_ANALYTIC_FUNCTION

// define some boundary grid functions to define boundary conditions
template<typename GV>
class B
        : public Dune::PDELab::BoundaryGridFunctionBase<Dune::PDELab::BoundaryGridFunctionTraits<GV,int,1,
          Dune::FieldVector<int,1> >,
          B<GV> >
{
    GV gv;

public:
    typedef Dune::PDELab::BoundaryGridFunctionTraits<GV,int,1,Dune::FieldVector<int,1> > Traits;
    typedef Dune::PDELab::BoundaryGridFunctionBase<Traits,B<GV> > BaseT;

    B (const GV& gv_) : gv(gv_) {}

    template<typename I>
    inline void evaluate (const Dune::PDELab::IntersectionGeometry<I>& ig,
            const typename Traits::DomainType& x,
            typename Traits::RangeType& y) const
    {
#if defined TEST_CASE_COMPLEX
        Dune::FieldVector<typename I::ctype,I::dimension>
        xg = ig.geometry().global(x);

        if (xg[1]<1E-6 || xg[1]>1.0-1E-6)
            {
            y = 0;
            }

        else if (xg[0]>1.0-1E-6 && xg[1]>0.5+1E-6)
            {
            y = 0;
            }
        else
                y = 1;
#else
        y = 1; // all is Dirichlet boundary
#endif
    }

    //! get a reference to the GridView
    inline const GV& getGridView () const
    {
        return gv;
    }
};

template<template<class,class,class,int> class Preconditioner,
template<class> class Solver>
class ISTL_SEQ_Subblock_Backend
        : public Dune::PDELab::SequentialNorm
          , public Dune::PDELab::LinearResultStorage
          {
          public:
    /*! \brief make a linear solver object

    \param[in] maxiter_ maximum number of iterations to do
    \param[in] verbose_ print messages if true
     */
    explicit ISTL_SEQ_Subblock_Backend(unsigned block, unsigned maxiter_=5000, int verbose_=1)
    : _block(block)
    , maxiter(maxiter_)
    , verbose(verbose_)
    {}

    unsigned block() const
    {
        return _block;
    }

    void setBlock(unsigned block)
    {
        _block = block;
    }

    /*! \brief solve the given linear system

    \param[in] A the given matrix
    \param[out] z the solution vector to be computed
    \param[in] r right hand side
    \param[in] reduction to be achieved
     */
    template<class M, class V, class W>
    void apply(M& A, V& z, W& r, typename W::ElementType reduction)
    {
        static_assert(std::is_same<V,W>::value,"V and W must be identical");

        typedef typename Dune::PDELab::istl::raw_type<M>::type ISTLMatrix;
        typedef typename Dune::PDELab::istl::raw_type<V>::type ISTLVector;

        typedef typename ISTLMatrix::block_type MatrixBlock;
        typedef typename ISTLVector::block_type VectorBlock;

        Dune::MatrixAdapter<
        MatrixBlock,
        VectorBlock,
        VectorBlock
        > opa(Dune::PDELab::istl::raw(A)[_block][_block]);
        Preconditioner<
        MatrixBlock,
        VectorBlock,
        VectorBlock,
        1
        > prec(Dune::PDELab::istl::raw(A)[_block][_block], 3, 1.0);
        Solver<
        VectorBlock
        > solver(opa, prec, reduction, maxiter, verbose);
        Dune::InverseOperatorResult stat;
        solver.apply(Dune::PDELab::istl::raw(z)[_block], Dune::PDELab::istl::raw(r)[_block], stat);
        res.converged  = stat.converged;
        res.iterations = stat.iterations;
        res.elapsed    = stat.elapsed;
        res.reduction  = stat.reduction;
        res.conv_rate  = stat.conv_rate;
    }

          private:
    unsigned _block;
    unsigned maxiter;
    int verbose;
          };


int main(int argc, char** argv)
{
    try
    {
        Dune::MPIHelper::instance(argc,argv);

        if (argc != 2) {
            std::cerr << "Usage: " << argv[0] << " <ini file>" << std::endl;
            return 1;
        }
        Dune::ParameterTree parameters;
        Dune::ParameterTreeParser::readINITree(argv[1],parameters);

        // set up grid
        const int dim = 2; //3; // 3d
        typedef Dune::SGrid<dim, dim, double> BaseGrid;
        Dune::FieldVector<double, dim> low(0.0);
        Dune::FieldVector<double, dim> high(0.0);
        high[0] = parameters.get<double>("mesh.x_length");
        high[1] = parameters.get<double>("mesh.y_length");
        //high[2] = parameters.get<double>("mesh.x_length"); // 3d
        Dune::FieldVector<int, dim> n(0.0);
        n[0] = parameters.get<int>("mesh.x_refine"); //1;
        n[1] = parameters.get<int>("mesh.y_refine"); //2;
        //n[2] = parameters.get<int>("mesh.x_refine"); //1; // 3d
        BaseGrid baseGrid(n, low, high);
        unsigned int refinementLevel = parameters.get<double>("mesh.refine");
        baseGrid.globalRefine(refinementLevel);
        typedef Dune::MultiDomainGrid<BaseGrid,Dune::mdgrid::FewSubDomainsTraits<BaseGrid::dimension,4> > Grid;
        Grid grid(baseGrid,false);
        typedef Grid::SubDomainGrid SubDomainGrid;
        SubDomainGrid& sdg0 = grid.subDomain(0);
        SubDomainGrid& sdg1 = grid.subDomain(1);
        typedef Grid::LeafGridView MDGV;
        typedef SubDomainGrid::LeafGridView SDGV;
        MDGV mdgv = grid.leafGridView();
        SDGV sdgv0 = sdg0.leafGridView(); // upper sub-domain
        SDGV sdgv1 = sdg1.leafGridView(); // lower sub-domain
        sdg0.hostEntityPointer(*sdgv0.begin<0>());
        grid.startSubDomainMarking();
        double interface = parameters.get<double>("mesh.interface");
        for (MDGV::Codim<0>::Iterator it = mdgv.begin<0>(); it != mdgv.end<0>(); ++it)
        {
#if defined TEST_CASE_COMPLEX
            if (it->geometry().center()[0] < interface * (high[1] + low[1])) // left
#else
            if (it->geometry().center()[1] > interface * (high[1] + low[1])) // upper
#endif
            {
                grid.addToSubDomain(0, *it);
            }
            else
            {
                grid.addToSubDomain(1, *it);
            }
        }
        grid.preUpdateSubDomains();
        grid.updateSubDomains();
        grid.postUpdateSubDomains();

        // types and constants
        typedef double DF;
        typedef double RF;
        const int indexUpper = 0;
        const int indexLower = 1;

        // instantiate finite element maps
        typedef Dune::PDELab::P0LocalFiniteElementMap<DF, RF, dim> P0Fem;
        P0Fem p0fem(Dune::GeometryType(Dune::GeometryType::cube, dim));

        typedef Dune::PDELab::NoConstraints NOCON;
        NOCON nocon;

        // construct grid function spaces
        typedef Dune::PDELab::ISTLVectorBackend<> VectorBackend;
        typedef Dune::PDELab::ISTLVectorBackend<
                Dune::PDELab::ISTLParameters::dynamic_blocking
                > MDVBE;

        // grid function space
        typedef Dune::PDELab::GridFunctionSpace<SDGV, P0Fem, NOCON, VectorBackend> GFS0;
        typedef Dune::PDELab::GridFunctionSpace<SDGV, P0Fem, NOCON, VectorBackend> GFS1;

        GFS0 gfs0(sdgv0, p0fem, nocon);
        GFS1 gfs1(sdgv1, p0fem, nocon);
        gfs0.name("u");
        gfs1.name("u");

        typedef Dune::PDELab::MultiDomain::MultiDomainGridFunctionSpace<
                Grid,
                MDVBE,
                Dune::PDELab::LexicographicOrderingTag,
                GFS0,
                GFS1
                > MultiGFS;
        MultiGFS multigfs(grid, gfs0, gfs1);

        typedef K<MDGV,RF> KType;
        KType k(mdgv);

        typedef A0<MDGV,RF> A0Type;
        A0Type a0(mdgv);

        typedef F0<MDGV,RF> F0Type;
        F0Type f0(mdgv);

        typedef F1<MDGV,RF> F1Type;
        F1Type f1(mdgv);

        typedef B<MDGV> BType;
        BType b(mdgv);

        typedef J<MDGV,RF> JType;
        JType j(mdgv);

        typedef G<MDGV,RF> GType;
        GType g(mdgv);

        // make grid function operator
        typedef Dune::PDELab::DiffusionCCFV<KType, A0Type, F0Type, BType, JType, GType> LOp0;
        LOp0 lop0(k,a0,f0,b,j,g);

        typedef Dune::PDELab::DiffusionCCFV<KType, A0Type, F1Type, BType, JType, GType> LOp1;
        LOp1 lop1(k,a0,f1,b,j,g);

        typedef Dune::PDELab::MultiDomain::SubDomainEqualityCondition<Grid> Condition;
        Condition c0(0);
        Condition c1(1);

        typedef Dune::PDELab::MultiDomain::SubProblem
                <MultiGFS, MultiGFS, LOp0, Condition, 0> UpperSubProblem;
        UpperSubProblem upper_sp(lop0, c0);

        typedef Dune::PDELab::MultiDomain::SubProblem
                <MultiGFS, MultiGFS, LOp1, Condition, 1> LowerSubProblem;
        LowerSubProblem lower_sp(lop1, c1);

        typedef typename Dune::PDELab::BackendVectorSelector<MultiGFS, DF>::Type V;
        V xOld(multigfs);
        xOld = 0.0;

        CouplingSina<KType,RF> couplingSina(k);
        typedef Dune::PDELab::MultiDomain::Coupling<UpperSubProblem,LowerSubProblem,CouplingSina<KType,RF> > Coupling_mono;
        Coupling_mono coupling_mono(upper_sp,lower_sp,couplingSina);

        auto constraints = Dune::PDELab::MultiDomain::constraints<RF>(multigfs,
                Dune::PDELab::MultiDomain::constrainSubProblem(upper_sp, b),
                Dune::PDELab::MultiDomain::constrainSubProblem(lower_sp, b));

        typedef MultiGFS::ConstraintsContainer<RF>::Type ConstraintsContainer;
        ConstraintsContainer constraintsContainer;

        typedef Dune::PDELab::ISTLMatrixBackend MatrixBackend;

        typedef Dune::PDELab::MultiDomain::GridOperator<
                MultiGFS, MultiGFS,
                MatrixBackend, RF, RF, RF, ConstraintsContainer, ConstraintsContainer,
                UpperSubProblem,
                LowerSubProblem,
                Coupling_mono
                > GridOperator;

        typedef Dune::PDELab::MultiDomain::Coupling<
                UpperSubProblem,
                LowerSubProblem,
                CouplingSina<KType,RF>
                > DirichletCoupling;
        DirichletCoupling dirichlet_coupling(
                upper_sp,
                lower_sp,
                couplingSina
        );

        typedef Dune::PDELab::MultiDomain::Coupling<
                LowerSubProblem,
                UpperSubProblem,
                CouplingSina<KType,RF>
                > NeumannCoupling;
        NeumannCoupling neumann_coupling(
                lower_sp,
                upper_sp,
                couplingSina
        );

        typedef Dune::PDELab::MultiDomain::GridOperator<
                MultiGFS,MultiGFS,
                MatrixBackend, RF, RF, RF, ConstraintsContainer, ConstraintsContainer,
                UpperSubProblem,
                DirichletCoupling,
                LowerSubProblem,
                NeumannCoupling
                > FullOperator;
        FullOperator full_operator(
                multigfs,multigfs,
                constraintsContainer, constraintsContainer,
                upper_sp,
                dirichlet_coupling,
                lower_sp,
                neumann_coupling
        );

        typedef Dune::PDELab::MultiDomain::GridOperator<
                MultiGFS,MultiGFS,
                MatrixBackend, RF, RF, RF, ConstraintsContainer, ConstraintsContainer,
                UpperSubProblem,
                DirichletCoupling
                > UpperOperator;
        UpperOperator upper_operator(
                multigfs,multigfs,
                constraintsContainer, constraintsContainer,
                upper_sp,
                dirichlet_coupling
        );

        typedef Dune::PDELab::MultiDomain::GridOperator<
                MultiGFS,MultiGFS,
                MatrixBackend, RF, RF, RF, ConstraintsContainer, ConstraintsContainer,
                LowerSubProblem,
                NeumannCoupling
                > LowerOperator;
        LowerOperator lower_operator(
                multigfs,multigfs,
                constraintsContainer, constraintsContainer,
                lower_sp,
                neumann_coupling
        );

        // make coefficient Vector and initialize it from a function
        Dune::PDELab::MultiDomain::interpolateOnTrialSpace(multigfs, xOld,
                g, upper_sp, g, lower_sp);

        // clear interior
        Dune::PDELab::set_nonconstrained_dofs(constraintsContainer, 0.0, xOld);

        std::cout << "interpolation: " << xOld.N() << " dof total, " << constraintsContainer.size() << " dof constrained" << std::endl;

        GridOperator gridoperator(multigfs, multigfs,
                constraintsContainer, constraintsContainer,
                upper_sp,
                lower_sp,
                coupling_mono);

        using SubDomainIndex = typename Grid::SubDomainIndex;

        if (parameters.get<bool>("monolithic.solve"))
        {
            auto start = std::chrono::high_resolution_clock::now();

            // <<<5>>> Select a linear solver backend
            typedef Dune::PDELab::ISTLBackend_SEQ_BCGS_SSOR LinearSolver;
            LinearSolver ls(
                    parameters.get<int>("monolithic.linearsolver.iterations"),
                    parameters.get<int>("monolithic.linearsolver.verbosity")
            );

            V xOld2(xOld);

            // <<<6>>> Solve nonlinear problem
            typename Dune::PDELab::Newton<GridOperator, LinearSolver, V> newton(gridoperator, xOld2, ls);
            newton.setReassembleThreshold(0.0);
            newton.setVerbosityLevel(2);
            newton.setReduction(1e-10);
            newton.setMinLinearReduction(1e-4);
            newton.setMaxIterations(25);
            newton.setLineSearchStrategy(newton.noLineSearch);
            newton.apply();

            // <<<8>>> graphical output
            {
                Dune::VTKWriter<SDGV> vtkwriter(sdgv0,Dune::VTK::nonconforming);
                Dune::PDELab::MultiDomain::addSolutionToVTKWriter(
                        vtkwriter,
                        multigfs,
                        xOld2,
                        Dune::PDELab::MultiDomain::subdomain_predicate<Grid::SubDomainIndex>(sdgv0.grid().domain())
                );

                vtkwriter.write("test_diffusionccfvcoupled-upper",Dune::VTK::ascii);
            }

            {
                Dune::SubsamplingVTKWriter<SDGV> vtkwriter(sdgv1,0);
                Dune::PDELab::MultiDomain::addSolutionToVTKWriter(
                        vtkwriter,
                        multigfs,
                        xOld2,
                        Dune::PDELab::MultiDomain::subdomain_predicate<Grid::SubDomainIndex>(sdgv1.grid().domain())
                );

                vtkwriter.write("test_diffusionccfvcoupled-lower",Dune::VTK::ascii);
            }

            typedef Dune::PDELab::GridFunctionSubSpace
              <MultiGFS, Dune::TypeTree::TreePath<indexUpper> > UPPERU;
            UPPERU upperU(multigfs);
            typedef Dune::PDELab::GridFunctionSubSpace
            <MultiGFS, Dune::TypeTree::TreePath<indexLower> > LOWERU;
          LOWERU lowerU(multigfs);
            // compute L2 error if analytical solution is available
            G<SDGV, RF> gUpper(sdgv0);
            G<SDGV, RF> gLower(sdgv1);
            std::cout.precision(8);
//            std::cout << "L2 error for "
//                      << std::setw(6) << sdgv0.size(0)
//                      << " elements: "
//                      << std::scientific
//                      << l2interpolationerror(gUpper, upperU, xOld2, 4)
//                      << std::endl;

            auto end = std::chrono::high_resolution_clock::now();

            auto elapsed = end - start;

            std::ofstream file("diffusion_results.txt", std::ios::out|std::ios::app);
            file << refinementLevel << "\t"
                    << std::chrono::duration_cast<std::chrono::duration<double> >(elapsed).count() << "\t"
                    << std::scientific << l2interpolationerror(gUpper, upperU, xOld2, 4) << "\t"
                    << std::scientific << l2interpolationerror(gLower, lowerU, xOld2, 4) << "\t"
                    << std::endl;

            std::cout << std::endl
                    << std::endl
                    << "Monolithic solver: " << std::chrono::duration_cast<std::chrono::duration<double> >(elapsed).count() << std::endl
                    << std::endl;

        } // if(parameters.get<bool>("monolithic.solve"))

        if (parameters.get<bool>("dn.solve"))
        {
            typedef ISTL_SEQ_Subblock_Backend<
                    Dune::SeqSSOR,
                    Dune::BiCGSTABSolver
                    > LinearSolver_DN_0;
            LinearSolver_DN_0 linear_solver_dn_0(
                    0,
                    parameters.get<int>("dn.upper.linearsolver.iterations"),
                    parameters.get<int>("dn.upper.linearsolver.verbosity")
            );

//            // direct solver (upper domain)
//#if HAVE_UMFPACK
//            typedef Dune::PDELab::ISTLBackend_SEQ_UMFPack LinearSolver_DN_0;
//#elif HAVE_SUPERLU
//            typedef Dune::PDELab::ISTLBackend_SEQ_SuperLU LinearSolver_DN_0;
//#else
//#error No direct linear solver, use UMFPack or SuperLU.
//#endif
//
//            LinearSolver_DN_0 linear_solver_dn_0(false);

            // iterative solver (lower domain)
            typedef ISTL_SEQ_Subblock_Backend<
                    Dune::SeqSSOR,
                    Dune::BiCGSTABSolver
                    > LinearSolver_DN_1;

            LinearSolver_DN_1 linear_solver_dn_1(
                    1,
                    parameters.get<int>("dn.lower.linearsolver.iterations"),
                    parameters.get<int>("dn.lower.linearsolver.verbosity")
            );

            // solve linear problem per stage
            typedef Dune::PDELab::StationaryLinearProblemSolver<
                    UpperOperator,
                    LinearSolver_DN_0,
                    V
                    > UpperPDESolver;
            UpperPDESolver upper_pde_solver(
                    upper_operator,
                    linear_solver_dn_0,
                    parameters.sub("dn.upper.solver")
            );

//            typename Dune::PDELab::Newton<UpperOperator, LinearSolver_DN_0, V> upper_newton(upper_operator, xOld, linear_solver_dn_0);
//            upper_newton.setReassembleThreshold(0.0);
//            upper_newton.setVerbosityLevel(2);
//            upper_newton.setReduction(1e-9);
//            upper_newton.setMinLinearReduction(1e-4);
//            upper_newton.setMaxIterations(25);
//            upper_newton.setLineSearchStrategy(upper_newton.noLineSearch);

            typedef Dune::PDELab::StationaryLinearProblemSolver<
                    LowerOperator,
                    LinearSolver_DN_1,
                    V
                    > LowerPDESolver;
            LowerPDESolver lower_pde_solver(
                    lower_operator,
                    linear_solver_dn_1,
                    parameters.sub("dn.lower.solver")
            );


            V residual(multigfs);
            full_operator.residual(xOld,residual);

            RF r, start_r;
            r = start_r = residual.two_norm();
            std::cout << "Start residual: " << r << std::endl;

            std::size_t i = 0;
            std::size_t max_iterations = parameters.get<int>("dn.coupling.iterations");

            auto x(xOld);

            double theta = parameters.get<double>("dn.coupling.damping");
            double rel_reduction = parameters.get<double>("dn.coupling.reduction");
            double max_error = parameters.get<double>("dn.coupling.maxerror");

            bool reassemble_jacobian_upper = parameters.get<bool>("dn.upper.reassemble");
            bool reassemble_jacobian_lower = parameters.get<bool>("dn.lower.reassemble");

            double upper_max_reduction = parameters.get<double>("dn.upper.solver.reduction");
            double upper_min_reduction = parameters.get<double>("dn.upper.solver.minreduction");
            double upper_decay = parameters.get<double>("dn.upper.solver.reductiondecay");
            double upper_offset =  1.0 / (1.0 - upper_max_reduction / upper_min_reduction);

            double lower_max_reduction = parameters.get<double>("dn.lower.solver.reduction");
            double lower_min_reduction = parameters.get<double>("dn.lower.solver.minreduction");
            double lower_decay = parameters.get<double>("dn.lower.solver.reductiondecay");
            double lower_offset = 1.0 / (1.0 - lower_max_reduction / lower_min_reduction);

            auto start = std::chrono::high_resolution_clock::now();

            const std::size_t vtk_frequency = parameters.get<int>("dn.coupling.vtk_frequency");
            const bool vtk_enabled = vtk_frequency > 0;

            while (r / start_r > rel_reduction && r > max_error && i < max_iterations)
            {
                if (vtk_enabled && i % vtk_frequency == 0)
                {
                    {
                        Dune::VTKWriter<SDGV> vtkwriter(sdgv0,Dune::VTK::nonconforming);
                        Dune::PDELab::MultiDomain::addSolutionToVTKWriter(
                                vtkwriter,
                                multigfs,
                                xOld,
                                Dune::PDELab::MultiDomain::subdomain_predicate<SubDomainIndex>(sdgv0.grid().domain())
                        );
                        Dune::PDELab::MultiDomain::addSolutionToVTKWriter(
                                vtkwriter,
                                multigfs,
                                residual,
                                Dune::PDELab::MultiDomain::subdomain_predicate<SubDomainIndex>(sdgv0.grid().domain()),
                                Dune::PDELab::vtk::DefaultFunctionNameGenerator("r")
                        );

                        std::stringstream fn;
                        fn << "upper-" << i;
                        vtkwriter.write(fn.str(),Dune::VTK::ascii);
                    }

                    {
                        Dune::SubsamplingVTKWriter<SDGV> vtkwriter(sdgv1,2);
                        Dune::PDELab::MultiDomain::addSolutionToVTKWriter(
                                vtkwriter,
                                multigfs,
                                xOld,
                                Dune::PDELab::MultiDomain::subdomain_predicate<SubDomainIndex>(sdgv1.grid().domain())
                        );
                        Dune::PDELab::MultiDomain::addSolutionToVTKWriter(
                                vtkwriter,
                                multigfs,
                                residual,
                                Dune::PDELab::MultiDomain::subdomain_predicate<SubDomainIndex>(sdgv1.grid().domain()),
                                Dune::PDELab::vtk::DefaultFunctionNameGenerator("r")
                        );

                        std::stringstream fn;
                        fn << "lower-" << i;
                        vtkwriter.write(fn.str(),Dune::VTK::ascii);
                    }
                } // if (vtk_enabled && i % vtk_frequency == 0)

                x = xOld;
//                upper_newton.apply();
                upper_pde_solver.setReduction(upper_min_reduction * (1.0 - 1.0 / (upper_offset + upper_decay * i)));
                upper_pde_solver.apply(xOld,!reassemble_jacobian_upper && (i>0));

                lower_pde_solver.setReduction(lower_min_reduction * (1.0 - 1.0 / (lower_offset + lower_decay * i)));
                lower_pde_solver.apply(xOld,!reassemble_jacobian_lower && (i>0));
                xOld *= theta;
                xOld.axpy(1.0-theta,x);

                residual = 0.0;
                full_operator.residual(xOld,residual);
                r = residual.two_norm();

                ++i;
                std::cout << std::setw(4) << i << " " << r << std::endl;

            } // while (r / start_r > rel_reduction && r > max_error && i < max_iterations)

            auto end = std::chrono::high_resolution_clock::now();

            auto elapsed = end - start;

            std::cout << std::endl
                    << std::endl
                    << "Dirichlet-Neumann solver: " << std::chrono::duration_cast<std::chrono::duration<double> >(elapsed).count() << std::endl
                    << std::endl;


            // <<<8>>> graphics
            {
                Dune::VTKWriter<SDGV> vtkwriter(sdgv0,Dune::VTK::nonconforming);
                Dune::PDELab::MultiDomain::addSolutionToVTKWriter(
                        vtkwriter,
                        multigfs,
                        xOld,
                        Dune::PDELab::MultiDomain::subdomain_predicate<SubDomainIndex>(sdgv0.grid().domain())
                );

                vtkwriter.write("test_diffusionccfv-DNcoupled-upper",Dune::VTK::ascii);
            }

            {
                Dune::SubsamplingVTKWriter<SDGV> vtkwriter(sdgv1,0);
                Dune::PDELab::MultiDomain::addSolutionToVTKWriter(
                        vtkwriter,
                        multigfs,
                        xOld,
                        Dune::PDELab::MultiDomain::subdomain_predicate<SubDomainIndex>(sdgv1.grid().domain())
                );

                vtkwriter.write("test_diffusionccfv-DNcoupled-lower",Dune::VTK::ascii);
            }


            typedef Dune::PDELab::GridFunctionSubSpace
              <MultiGFS, Dune::TypeTree::TreePath<indexUpper> > UPPERU;
            UPPERU upperU(multigfs);
            typedef Dune::PDELab::GridFunctionSubSpace
            <MultiGFS, Dune::TypeTree::TreePath<indexLower> > LOWERU;
          LOWERU lowerU(multigfs);
            // compute L2 error if analytical solution is available
            G<SDGV, RF> gUpper(sdgv0);
            G<SDGV, RF> gLower(sdgv1);
            std::cout.precision(8);
//            std::cout << "L2 error for "
//                      << std::setw(6) << sdgv0.size(0)
//                      << " elements: "
//                      << std::scientific
//                      << l2interpolationerror(gUpper, upperU, xOld, 4)
//                      << std::endl;

            std::ofstream file("diffusion_results_e-y.txt", std::ios::out|std::ios::app);
            file << refinementLevel << "\t"
                    << std::chrono::duration_cast<std::chrono::duration<double> >(elapsed).count() << "\t"
                    << std::scientific << l2interpolationerror(gUpper, upperU, xOld, 4) << "\t"
                    << std::scientific << l2interpolationerror(gLower, lowerU, xOld, 4) << "\t"
                    << i << std::endl;
        } // if (parameters.get<bool>("dn.solve"))

        return EXIT_SUCCESS;
    } // try
    catch (Dune::Exception &e){
        std::cerr << "Dune reported error: " << e << std::endl;
        return 1;
    }
    catch (std::exception e) {
        std::cerr << "Dune reported std error: " << e.what() << std::endl;
    }
    catch (...){
        std::cerr << "Unknown exception thrown!" << std::endl;
        return 1;
    }
} // main
