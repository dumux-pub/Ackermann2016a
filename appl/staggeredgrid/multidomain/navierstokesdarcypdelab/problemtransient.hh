/**
 * \file
 * \ingroup TestProblems
 *
 * \brief Boundary condition types for Stokes 1p / Darcy 1p coupled problem.
 *
 * The domain is \f$(0,0) \times (0,2)\f$ and is divided in the upper Stokes
 * domain \f$(0,1) \times (0,2)\f$ and the lower Darcy domain
 * \f$(0,0) \times (0,1)\f$.
 *
 * The Boundary conditions are for Stokes:
 * - Velocity:  Inflow at the left boundary,
 *              IsWall at the top boundary,
 *              Outflow at the right boundary.
 * - Pressure:  Dirichlet at the right boundary
 *
 *
 * The Boundary conditions are for Darcy:
 * - Neumann no-flow at the left, right and bottom boundary
 *
 *  (see test case C in problemchidyagwairiviereanalytic.hh)
 */

#ifndef DUMUX_MULTIDOMAIN_PDELAB_PROBLEM_KANSCHAT_RIVIERE_MASS_HH
#define DUMUX_MULTIDOMAIN_PDELAB_PROBLEM_KANSCHAT_RIVIERE_MASS_HH

#include<cmath>
#include<dune/pdelab/common/function.hh>
#include<dune/pdelab/constraints/common/constraintsparameters.hh>
#include <dune/pdelab/localoperator/diffusionparam.hh>

#include "../../common_pdelab/velocitywallfunctions.hh" // chidyagwairiviereinclusion.hh

#define KINEMATIC_VISCOSITY 1e-0
#define DENSITY 1
// #define ENABLE_NAVIER_STOKES 0

#define EPSILON 1e-9

// #define TEST_CASE_B // A
// #define TEST_CASE_C // B
// #define TEST_CASE_D // C
 #define TEST_CASE_E // D
//#define CR_INCLUSION //  = chidyagwairiviereinclusion.hh

// ////////////////////////////////////////////////////////////
// Boundary conditions for Navier-Stokes subdomain
// ////////////////////////////////////////////////////////////

/**
 * \brief Boundary condition function for the velocity components.
 */
class BCVelocity
: public Dune::PDELab::DirichletConstraintsParameters
{
public:
  //! \brief Return whether Intersection is Wall Boundary for velocity
  //! \tparam I Intersection type
  template<typename I>
  bool isWall(const I& intersection,
              const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
    return false;
  }

  //! \brief Return whether Intersection is Inflow Boundary for velocity
  //! \tparam I Intersection type
  template<typename I>
  bool isInflow(const I& intersection,
                const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
    // everywhere except bottom
    Dune::FieldVector<typename I::ctype, I::dimension> global =
      intersection.geometry().global(coord);
//     return (global[1] > 2.0 - EPSILON);
    return (global[1] > 1.0 + EPSILON);
  }

  //! \brief Return whether Intersection is Outflow Boundary for velocity
  //! \tparam I Intersection type
  template<typename I>
  bool isOutflow(const I& intersection,
                 const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
    return false;
  }

  //! \brief Return whether Intersection is Symmetry Boundary for velocity
  //! \tparam I Intersection type
  template<typename I>
  bool isSymmetry(const I& intersection,
                 const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
    return false;
  }
};

/**
 * \brief Boundary condition function for the pressure component.
 */
class BCPressure
: public Dune::PDELab::DirichletConstraintsParameters
{
public:
  //! \brief Return whether Intersection is Dirichlet Boundary for pressure
  //! \tparam I Intersection type
  template<typename I>
  bool isDirichlet(const I& intersection,
                   const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
#if defined CR_INCLUSION
      return false;
#elif defined TEST_CASE_B || defined TEST_CASE_C || defined TEST_CASE_D || defined TEST_CASE_E
    // only top
    Dune::FieldVector<typename I::ctype, I::dimension> global =
      intersection.geometry().global(coord);
    return (global[1] > 2.0 - EPSILON);
#endif
  }

  //! \brief Return whether Intersection is Outflow Boundary for pressure
  //! \tparam I Intersection type
  template<typename I>
  bool isOutflow(const I& intersection,
                 const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
    return !isDirichlet(intersection, coord);
  }
};

/**
 * \brief Function for velocity Dirichlet boundary conditions and initialization.
 *
 * \tparam GV GridView type
 * \tparam RF Range type
 */
template<typename GV, typename RF>
class DirichletVelocity
: public Dune::PDELab::AnalyticGridFunctionBase<
    Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, GV::dimension>, DirichletVelocity<GV, RF> >,
  public Dune::PDELab::InstationaryFunctionDefaults
{
public:
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, GV::dimension> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, DirichletVelocity<GV, RF> > BaseT;


  //! \brief Constructor
  DirichletVelocity(const GV& gv)
  : BaseT(gv), time(0)
  {}

  //! \brief Evaluate the function globally
  inline void evaluateGlobal(const typename Traits::DomainType& x,
                             typename Traits::RangeType& y) const
  {
     // test case B * sin²(t)
#if defined TEST_CASE_B
    y[0] = 0.0;
    y[1] = 1.0 * std::sin(time) * std::sin(time);
    // test case C * sin²(t)
#elif defined TEST_CASE_C
    y[0] = std::exp(-1.0 * x[1]) * std::sin(time) * std::sin(time);
    y[1] = 0.0;
    // test case D * sin²(t)
#elif defined TEST_CASE_D
    y[0] = 0.0;
    y[1] = x[1] * std::sin(time) * std::sin(time);
    // test case E * sin²(t)
#elif defined TEST_CASE_E
    y[0] = 100.0 * std::exp(-1.0 * x[1]) * std::sin(time) * std::sin(time);
    y[1] = 2.0 * x[1] * std::sin(time) * std::sin(time);
#elif defined CR_INCLUSION
    y = 0.0;
    // left
    if (x[0] < EPSILON)
    {
        y[0] = -1.0 * (x[1] - 2.0) * (x[1] - 1.0);
        y[1] = 0.0;
    }
    // right
    if (x[0] > 1.5 - EPSILON)
    {
        y[0] = -0.5 * (x[1] - 2.0) * (x[1] - 1.0);
        y[1] = 0.0;
    }
#endif
  }

  //! set time for subsequent evaluation
  /**
   * This method sets the time for subsequent calls to any of the
   * evaluation methods.
   */
  template<typename Time>
  inline void setTime(Time t)
  {
      time = t;
  }

private:
  RF time;
};

/**
 * \brief Function for pressure Dirichlet boundary conditions and initialization.
 *
 * \tparam GV GridView type
 * \tparam RF Range type
 */
template<typename GV, typename RF>
class DirichletPressure
: public Dune::PDELab::AnalyticGridFunctionBase<
    Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, 1>, DirichletPressure<GV, RF> >,
  public Dune::PDELab::InstationaryFunctionDefaults
{
public:
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, 1> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, DirichletPressure<GV, RF> > BaseT;

  //! \brief Constructor
  DirichletPressure(const GV& gv)
  : BaseT(gv) {}

  //! \brief Evaluate the function globally
  inline void evaluateGlobal(const typename Traits::DomainType& x,
                             typename Traits::RangeType& y) const
  {
#if defined TEST_CASE_B
    y = -1.0 * x[1];
#elif defined TEST_CASE_C
    y = 7.0;
#elif defined TEST_CASE_D
    y = -1.0 * x[1] + 7.0;
#elif defined TEST_CASE_E
    y = -2.0 * x[1] + 6.0;
#elif defined CR_INCLUSION
    y = 0.0;
#endif
  }
};

/**
 * \brief Function for velocity Neumann boundary conditions.
 *
 * \tparam GV GridView type
 * \tparam RF Range type
 */
template<typename GV, typename RF>
class NeumannVelocity
: public Dune::PDELab::AnalyticGridFunctionBase<
    Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, GV::dimension>, NeumannVelocity<GV, RF> >
{
public:
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, GV::dimension> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, NeumannVelocity<GV, RF> > BaseT;

  //! \brief Constructor
  NeumannVelocity(const GV& gv)
  : BaseT(gv) {}

  //! \brief Evaluate the function globally
  inline void evaluateGlobal(const typename Traits::DomainType& x,
                             typename Traits::RangeType& y) const
  {
    y = 0;
  }
};

/**
 * \brief Function for pressure Neumann boundary conditions.
 *
 * \tparam GV GridView type
 * \tparam RF Range type
 */
template<typename GV, typename RF>
class NeumannPressure
: public Dune::PDELab::AnalyticGridFunctionBase<
    Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, 1>, NeumannPressure<GV, RF> >
{
public:
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, 1> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, NeumannPressure<GV, RF> > BaseT;

  //! \brief Constructor
  NeumannPressure(const GV& gv)
  : BaseT(gv) {}

  //! \brief Evaluate the function globally
  inline void evaluateGlobal(const typename Traits::DomainType& x,
                             typename Traits::RangeType& y) const
  {
    y = 0;
  }
};

/**
 * \brief Source term function for the momentum balance.
 *
 * \tparam GV GridView type
 * \tparam RF Range type
 */
template<typename GV, typename RF>
class SourceMomentumBalance
  : public Dune::PDELab::AnalyticGridFunctionBase<
      Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, GV::dimension>, SourceMomentumBalance<GV, RF> >
{
public:
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, GV::dimension> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, SourceMomentumBalance<GV, RF> > BaseT;

  //! \brief Constructor
  SourceMomentumBalance(const GV& gv)
  : BaseT(gv) {}

  //! \brief Evaluate the function globally
  inline void evaluateGlobal(const typename Traits::DomainType& x,
                             typename Traits::RangeType& y) const
  {
#if defined TEST_CASE_B
    y[0] = 0.0;
    y[1] = -1.0;
#elif defined TEST_CASE_C
    y[0] = -1.0 * std::exp(-1.0 * x[1]);
    y[1] = 0.0;
#elif defined TEST_CASE_D
    y[0] = 0.0;
    y[1] = -1.0;
#elif defined TEST_CASE_E
    y[0] = -100.0 * std::exp(-1.0 * x[1]);
    y[1] = -2.0;
#elif defined CR_INCLUSION
    y = 0.0;
#endif
  }
};

/**
 * \brief Source term function for the mass balance.
 *
 * \tparam GV GridView type
 * \tparam RF Range type
 */
template<typename GV, typename RF>
class SourceMassBalance
  : public Dune::PDELab::AnalyticGridFunctionBase<
      Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, 1>, SourceMassBalance<GV, RF> >
{
public:
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, 1> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, SourceMassBalance<GV, RF> > BaseT;

  //! \brief Constructor
  SourceMassBalance(const GV& gv)
  : BaseT(gv) {}

  //! \brief Evaluate the function globally
  inline void evaluateGlobal(const typename Traits::DomainType& x,
                             typename Traits::RangeType& y) const
  {
#if defined TEST_CASE_B || defined TEST_CASE_C || defined CR_INCLUSION
    y = 0.0;
#elif defined TEST_CASE_D
    y = 1.0;
#elif defined TEST_CASE_E
    y = 2.0;
#endif
  }
};

// ////////////////////////////////////////////////////////////
// Boundary conditions for Darcy subdomain
// ////////////////////////////////////////////////////////////

/**
 * \brief Scalar describing the permeability. (homogenious case)
 *
 * \tparam GV GridView type
 * \tparam RF Range type
 */
template<typename GV, typename RF>
class DarcyK
  : public Dune::PDELab::AnalyticGridFunctionBase<
      Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, 1>,
      DarcyK<GV, RF> >
{
public:
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, 1> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, DarcyK<GV, RF> > BaseT;

  DarcyK(const GV& gv)
  : BaseT(gv)
  {}

  void evaluateGlobal(const typename Traits::DomainType& x,
                      typename Traits::RangeType& y) const
  {
#if defined CR_INCLUSION
      y = 1.0;

      if ((x[0] > 0.5) && (x[0] < 1.0)
          && (x[1] > 0.25) && (x[1] < 0.75))
      {
        y = 1.0e-5;
      }
#elif defined TEST_CASE_B || defined TEST_CASE_C || defined TEST_CASE_D || defined TEST_CASE_E
    y = 1e-1; //1.0;
#endif
  }

  template<typename Time>
  void setTime(const Time& t)
  {}
};

/**
 * \brief Scalar Helmholtz term.
 *
 * \tparam GV GridView type
 * \tparam RF Range type
 */
template<typename GV, typename RF>
class DarcyA0
  : public Dune::PDELab::AnalyticGridFunctionBase<
      Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, 1>,
      DarcyA0<GV, RF> >
{
public:
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, 1> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, DarcyA0<GV, RF> > BaseT;

  DarcyA0(const GV& gv)
  : BaseT(gv)
  {}

  void evaluateGlobal(const typename Traits::DomainType& x,
                      typename Traits::RangeType& y) const
  {
    y = 0.0;
  }

  template<typename Time>
  void setTime(const Time& t)
  {}
};

/**
 * \brief Source term.
 *
 * \tparam GV GridView type
 * \tparam RF Range type
 */
template<typename GV, typename RF>
class DarcySource
  : public Dune::PDELab::AnalyticGridFunctionBase<
      Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, 1>,
      DarcySource<GV,RF> >
{
public:
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, 1> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, DarcySource<GV, RF> > BaseT;

  DarcySource(const GV& gv)
  : BaseT(gv)
  {}

  void evaluateGlobal(const typename Traits::DomainType& x,
                      typename Traits::RangeType& y) const
  {
    y = 0.0;
  }

  template<typename Time>
  void setTime(const Time& t)
  {}
};

/**
 * \brief Select boundary conditions excluding for coupoling interface.
 *
 * \tparam GV GridView type
 * \tparam RF Range type
 */
template<typename GV>
class DarcyBC
: public Dune::PDELab::BoundaryGridFunctionBase<
    Dune::PDELab::BoundaryGridFunctionTraits<GV,
      Dune::PDELab::DiffusionBoundaryCondition::Type, 1,
      Dune::FieldVector<Dune::PDELab::DiffusionBoundaryCondition::Type, 1> >,
    DarcyBC<GV> >
{
  const GV& gv;

public:
  typedef Dune::PDELab::DiffusionBoundaryCondition BC;
  typedef Dune::PDELab::BoundaryGridFunctionTraits<GV,
    Dune::PDELab::DiffusionBoundaryCondition::Type, 1,
    Dune::FieldVector<Dune::PDELab::DiffusionBoundaryCondition::Type, 1> > Traits;
  typedef Dune::PDELab::BoundaryGridFunctionBase<Traits, DarcyBC<GV> > BaseT;

  DarcyBC(const GV& gv_)
  : gv(gv_)
  {}

  template<typename I>
  void evaluate(const Dune::PDELab::IntersectionGeometry<I>& ig,
                const typename Traits::DomainType& x,
                typename Traits::RangeType& y) const
  {
    const Dune::FieldVector<typename I::ctype, I::dimension> global =
      ig.geometry().global(x);

#if defined CR_INCLUSION
    y = BC::Neumann;

    if ((global[1] < EPSILON) || (global[1] > 1.0 - EPSILON))
    {
      y = BC::Dirichlet;
    }
#elif defined TEST_CASE_B || defined TEST_CASE_C || defined TEST_CASE_D || defined TEST_CASE_E
       y = global[1] < EPSILON ? BC::Dirichlet : BC::Neumann;
       // y = BC::Neumann;
#endif
  }

  //! get a reference to the GridView
  const GV& getGridView()
  {
    return gv;
  }

  template<typename Time>
  void setTime(const Time& t)
  {}
};

/**
 * \brief Dirichlet boundary conditions.
 *
 * \tparam GV GridView type
 * \tparam RF Range type
 */
template<typename GV, typename RF>
class DarcyG
: public Dune::PDELab::AnalyticGridFunctionBase<
    Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, 1>, DarcyG<GV, RF> >,
  public Dune::PDELab::InstationaryFunctionDefaults
{
public:
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, 1> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, DarcyG<GV, RF> > BaseT;

  DarcyG(const GV& gv)
  : BaseT(gv)
  {}

  void evaluateGlobal(const typename Traits::DomainType& x,
                      typename Traits::RangeType& y) const
  {
#if defined TEST_CASE_B
    y = -1.0 * x[1];
#elif defined TEST_CASE_C
    y = 7.0;
#elif defined TEST_CASE_D
    y = -1.0 * x[1] + 6.0;
#elif defined TEST_CASE_E
    y = -2.0 * x[1] + 4.0;
#elif defined CR_INCLUSION
    y = 0.0;
#endif
  }

  template<typename Time>
  void setTime(const Time& t)
  {}
};

/**
 * \brief Neumann boundary conditions.
 *
 * \tparam GV GridView type
 * \tparam RF Range type
 */
template<typename GV, typename RF>
class DarcyJ
: public Dune::PDELab::AnalyticGridFunctionBase<
  Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, 1>, DarcyJ<GV, RF> >
{
public:
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, 1> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, DarcyJ<GV, RF> > BaseT;

  DarcyJ(const GV& gv)
  : BaseT(gv)
  {}

  void evaluateGlobal(const typename Traits::DomainType& x,
                      typename Traits::RangeType& y) const
  {
    y = 0.0;
  }

  template<typename Time>
  void setTime(const Time& t)
  {}
};

#endif // DUMUX_MULTIDOMAIN_PDELAB_PROBLEM_KANSCHAT_RIVIERE_MASS_HH
