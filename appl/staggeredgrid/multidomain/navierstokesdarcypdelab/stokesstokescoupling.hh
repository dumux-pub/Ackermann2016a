#ifndef DUMUX_MULTIDOMAIN_STOKESSTOKES_COUPLING_HH
#define DUMUX_MULTIDOMAIN_STOKESSTOKES_COUPLING_HH

#include <dune/geometry/quadraturerules.hh>

#include <dune/pdelab/localoperator/idefault.hh>
#include <dune/pdelab/localoperator/pattern.hh>

#include <dune/pdelab/multidomain/couplingutilities.hh>

template<typename TReal>
class StokesStokesCoupling
  : public Dune::PDELab::MultiDomain::CouplingOperatorDefaultFlags
  , public Dune::PDELab::MultiDomain::NumericalJacobianCoupling<StokesStokesCoupling<TReal> >
  , public Dune::PDELab::MultiDomain::NumericalJacobianApplyCoupling<StokesStokesCoupling<TReal> >
  , public Dune::PDELab::MultiDomain::FullCouplingPattern
  , public Dune::PDELab::InstationaryLocalOperatorDefaultMethods<TReal>
{
public:
  static const bool doAlphaCoupling = true;
  static const bool doPatternCoupling = true;

  //! \brief Index of velocity
  static const unsigned int velocityIdx = 0;
  static const unsigned int pressureIdx = 1;

  template<typename IG, typename LFSU1, typename LFSU2,
           typename X, typename LFSV1, typename LFSV2,
           typename R>
  void alpha_coupling(const IG& ig,
                      const LFSU1& lfsu_s, const X& x_s, const LFSV1& lfsv_s,
                      const LFSU2& lfsu_n, const X& x_n, const LFSV2& lfsv_n,
                      R& r_s, R& r_n) const
  {
    // select the two components from the subspaces
    typedef typename LFSU1::template Child<velocityIdx>::Type LFSU_V;
    const LFSU_V& lfsu_v_s = lfsu_s.template child<velocityIdx>();
    const LFSU_V& lfsu_v_n = lfsu_n.template child<velocityIdx>();
    typedef typename LFSU1::template Child<pressureIdx>::Type LFSU_P;
    const LFSU_P& lfsu_p_s = lfsu_s.template child<pressureIdx>();
    const LFSU_P& lfsu_p_n = lfsu_n.template child<pressureIdx>();

    // domain and range field type
    typedef typename LFSU_V::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::DomainFieldType DF;
    typedef typename LFSU_V::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::RangeFieldType RF;
    typedef typename LFSU_V::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::RangeType RangeVelocity;

    // domain and range field type
    typedef typename LFSU_V::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::DomainFieldType DF;
    typedef typename LFSU_V::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::RangeFieldType RF;
    typedef typename LFSU_V::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::RangeType RangeVelocity;

    // /////////////////////
    // geometry information

    // dimension
    static const unsigned int dim = IG::Geometry::dimension;

    // local position of cell and face centers
    const Dune::FieldVector<DF, dim>& insideCellCenterLocal =
      Dune::ReferenceElements<DF, dim>::general(ig.inside()->type()).position(0, 0);
    const Dune::FieldVector<DF, dim>& outsideCellCenterLocal =
      Dune::ReferenceElements<DF, dim>::general(ig.outside()->type()).position(0, 0);
    const Dune::FieldVector<DF, dim-1>& faceCenterLocal =
      Dune::ReferenceElements<DF, dim-1>::general(ig.geometry().type()).position(0, 0);
//     const Dune::FieldVector<DF, dim>& insideFaceCenterLocal =
//         ig.geometryInInside().global(faceCenterLocal);

    // global position of cell and face centers
    Dune::FieldVector<DF, dim> insideCellCenterGlobal =
      ig.inside()->geometry().global(insideCellCenterLocal);
    Dune::FieldVector<DF, dim> outsideCellCenterGlobal =
      ig.outside()->geometry().global(outsideCellCenterLocal);
    Dune::FieldVector<DF, dim> faceCenterGlobal =
      ig.geometry().global(faceCenterLocal);

    // face normal
    const Dune::FieldVector<DF, dim>& faceUnitOuterNormal = ig.centerUnitOuterNormal();

    // evaluate orientation of intersection
    unsigned int normDim = 0;
    unsigned int tangDim = 1;
    for (unsigned int curDim = 0; curDim < dim; ++curDim)
    {
      if (std::abs(faceUnitOuterNormal[curDim]) > 1e-10 && dim > 1)
      {
        normDim = curDim;
        tangDim = 1 - curDim;
      }
    }

    // face midpoints of all faces
    const unsigned int numFaces =
      Dune::ReferenceElements<DF, dim>::general(ig.inside()->type()).size(1);
    std::vector<Dune::FieldVector<DF, dim> > faceCentersLocal_s(numFaces);
    std::vector<Dune::FieldVector<DF, dim> > faceCentersLocal_n(numFaces);
    std::vector<Dune::FieldVector<DF, dim> > faceCentersGlobal_s(numFaces);
    std::vector<Dune::FieldVector<DF, dim> > faceCentersGlobal_n(numFaces);
    for (unsigned int curFace = 0; curFace < numFaces; ++curFace)
    {
      faceCentersLocal_s[curFace] =
        Dune::ReferenceElements<DF, dim>::general(ig.inside()->geometry().type()).position(curFace, 1);
      faceCentersLocal_n[curFace] =
        Dune::ReferenceElements<DF, dim>::general(ig.outside()->geometry().type()).position(curFace, 1);
      faceCentersGlobal_s[curFace] = ig.inside()->geometry().global(faceCentersLocal_s[curFace]);
      faceCentersGlobal_n[curFace] = ig.outside()->geometry().global(faceCentersLocal_n[curFace]);
    }

    // face volume for integration
    RF faceVolume = ig.geometry().integrationElement(faceCenterLocal)
                    * Dune::ReferenceElements<DF, dim-1>::general(ig.geometry().type()).volume();

    if (faceVolume < 1e-9)
    {
      std::cout << "error, normDim: " << normDim << " tangDim: " << tangDim << " faceCenterGlobal: " << faceCenterGlobal << std::endl;
      for (unsigned int curFace = 0; curFace < numFaces; ++curFace)
      {
        std::cout << " faceCentersGlobal_s[" << curFace << "]: " << faceCentersGlobal_s[curFace] << std::endl;
        std::cout << " faceCentersGlobal_n[" << curFace << "]: " << faceCentersGlobal_n[curFace] << std::endl;
      }
    }

    // /////////////////////
    // velocities

    // evaluate shape functions at all face midpoints
    std::vector<std::vector<RangeVelocity> > velocityBasis_s(numFaces);
    std::vector<std::vector<RangeVelocity> > velocityBasis_n(numFaces);
    for (unsigned int curFace = 0; curFace < numFaces; ++curFace)
    {
      velocityBasis_s[curFace].resize(lfsu_v_s.size());
      velocityBasis_n[curFace].resize(lfsu_v_n.size());
      lfsu_v_s.finiteElement().localBasis().evaluateFunction(
        faceCentersLocal_s[curFace], velocityBasis_s[curFace]);
      lfsu_v_n.finiteElement().localBasis().evaluateFunction(
        faceCentersLocal_n[curFace], velocityBasis_n[curFace]);
    }

    // evaluate velocity on midpoints of all faces
    std::vector<RangeVelocity> velocities_s(numFaces);
    std::vector<RangeVelocity> velocities_n(numFaces);
    for (unsigned int curFace = 0; curFace < numFaces; ++curFace)
    {
      velocities_s[curFace] = RangeVelocity(0.0);
      velocities_n[curFace] = RangeVelocity(0.0);
      for (unsigned int i = 0; i < lfsu_v_s.size(); i++)
      {
        velocities_s[curFace].axpy(x_s(lfsu_v_s, i), velocityBasis_s[curFace][i]);
        velocities_n[curFace].axpy(x_n(lfsu_v_n, i), velocityBasis_n[curFace][i]);
      }
    }

    // distances between face center and cell centers for rectangular shapes
    DF distanceInsideToFace = std::abs(faceCenterGlobal[normDim] - insideCellCenterGlobal[normDim]);
    DF distanceOutsideToFace = std::abs(outsideCellCenterGlobal[normDim] - faceCenterGlobal[normDim]);

    // /////////////////////
    // evaluation of unknown, upwinding and averaging

    // evaluate cell values
    DF pressure_s = x_s(lfsu_p_s, 0);
    DF pressure_n = x_n(lfsu_p_n, 0);
#if !COMPRESSIBILITY
    DF density_s = pressure_s * 0.0 + DENSITY;
    DF density_n = pressure_n * 0.0 + DENSITY;
#else
    DF density_s = pressure_s / (SPECIFIC_GAS_CONSTANT * TEMPERATURE);
    DF density_n = pressure_n / (SPECIFIC_GAS_CONSTANT * TEMPERATURE);
#endif
    DF kinematicViscosity_s = pressure_s * 0.0 + KINEMATIC_VISCOSITY;
    DF kinematicViscosity_n = pressure_n * 0.0 + KINEMATIC_VISCOSITY;

    //! \todo TODO: remove hard coded gravitional acceleration
//         Dune::FieldVector<DF, dim> gravity(0.0);
//         gravity[dim-1] = -9.81;

    // normal velocity
    DF velocityNormal_s = (velocities_s[ig.indexInInside()] * faceUnitOuterNormal);
    DF velocityNormal_n = (velocities_n[ig.indexInOutside()] * faceUnitOuterNormal);

    // upwinding (from self to neighbor)
    DF density_up = density_s;
    if (velocityNormal_s < 0)
    {
      density_up = density_n;
    }

    // averaging: distance weighted average quantities on intersection
    DF density_avg = (distanceInsideToFace * density_n + distanceOutsideToFace * density_s)
        / (distanceInsideToFace + distanceOutsideToFace);
    DF kinematicViscosity_avg = (distanceInsideToFace * kinematicViscosity_n + distanceOutsideToFace * kinematicViscosity_s)
        / (distanceInsideToFace + distanceOutsideToFace);
    std::vector<RangeVelocity> velocities_avg(numFaces);
    for (unsigned int curFace = 0; curFace < numFaces; ++curFace)
    {
      for (unsigned int curDim = 0; curDim < dim; ++curDim)
      {
        velocities_avg[curFace][curDim] = (distanceInsideToFace * velocities_n[curFace][curDim] + distanceOutsideToFace * velocities_s[curFace][curDim])
            / (distanceInsideToFace + distanceOutsideToFace);
      }
    }

        // /////////////////////
        // contribution to residual on inside element, other residual is computed by symmetric call
        Dune::FieldVector<DF, dim> ones(1.0);

        /**
         * Penalize velocity jump over coupling interrface.
         */
        r_s.accumulate(lfsu_v_s, ig.indexInInside(),
                       (velocityNormal_s - velocityNormal_n)
                       * 100000000
                       / faceVolume);
        r_n.accumulate(lfsu_v_n, ig.indexInOutside(),
                       -1.0 * (velocityNormal_s - velocityNormal_n)
                       * 100000000
                       / faceVolume);
std::cout <<
                       lfsu_v_s.localIndex(ig.indexInInside())
                       << " / " << (
                       (velocityNormal_s - velocityNormal_n)
                       * 100000000
                       / faceVolume) << " // "<<
                       lfsu_v_n.localIndex(ig.indexInOutside())
                       << "  / " <<
                       (-1.0 * (velocityNormal_s - velocityNormal_n)
                       * 100000000
                       / faceVolume)
                       << std::endl;

        /**
         * Contribution to the different balance equations. All formulas are given for
         * the <tt>self</tt> element.<br>
         * <br>
         * (1) \b Flux term of \b mass balance equation
         *
         * \f[
         *    \varrho v
         *    \Rightarrow \int_\gamma \left( \varrho v \right) \cdot n
         * \f]
         * \f[
         *    \alpha_\textrm{self}
         *    = |\gamma| \varrho_\textrm{up} \left( v \cdot n \right)
         * \f]
         */
        r_s.accumulate(lfsu_p_s, 0,
                       1.0 * density_up
                       * velocityNormal_s
                       * faceVolume);
        r_n.accumulate(lfsu_p_n, 0,
                       -1.0 * density_up
                       * velocityNormal_n
                       * faceVolume);
#if PRINT_ACCUMULATION_TERM
std::cout << "FLUX MASS NORMAL @" <<
" insideCellCenterGlobal: " << insideCellCenterGlobal <<
" accumulate: " << (1.0 * density_up
                       * velocityNormal_s
                       * faceVolume) <<
std::endl;
std::cout << "FLUX MASS NORMAL @" <<
" outsideCellCenterGlobal: " << outsideCellCenterGlobal <<
" accumulate: " << (-1.0 * density_up
                       * velocityNormal_n
                       * faceVolume) <<
std::endl;
#endif // PRINT_ACCUMULATION_TERM

        for (unsigned int curDim = 0; curDim < dim; ++curDim)
        {
          /**
           * (2) \b Inertia term of \b momentum balance equation
           *
           * \bug think of dentsity averaging also, right now, velocity is averaged,
           *      whereas density is upwinded
           *
           * \f[
           *    \varrho v v^T
           *    \Rightarrow \int_\gamma \left( \varrho v v^T \right) \cdot n
           * \f]
           * Tangential cases for all coordinate axes
           * \f[
           *    \alpha_\textrm{self}
           *    = |\gamma| \left[ \left( \varrho v v^T \right) \cdot n \right] \cdot t
           *    = |\gamma| \varrho_\textrm{up} \left( v \cdot n \right)
           *      \left(v_\textrm{avg} \cdot t \right)
           *    = 0.5 |\gamma| \varrho_\textrm{up} \left( v \cdot n \right)
           *        v_\textrm{avg,left,t}
           *      + 0.5 |\gamma| \varrho_\textrm{up} \left( v \cdot n \right)
           *        v_\textrm{avg,right,t}
           * \f]
           * or shorter
           * \f[
           *    \alpha_\textrm{self}
           *    = 0.5 |\gamma| \varrho_\textrm{up} \left( v \cdot n \right)
           *       \left[ v_\textrm{avg,left,t} + v_\textrm{avg,right,t} \right]
           * \f]
           */
#if ENABLE_NAVIER_STOKES
          // only tangential case, exclude normal case
          if (curDim != normDim && dim > 1)
          {
            r_s.accumulate(lfsu_v_s, 2*tangDim,
                          0.5 * density_up
                          * velocityNormal_s
                          * velocities_avg[2*tangDim][tangDim]
                          * faceVolume);
            r_n.accumulate(lfsu_v_n, 2*tangDim,
                          -0.5 * density_up
                          * velocityNormal_n
                          * velocities_avg[2*tangDim][tangDim]
                          * faceVolume);
            r_s.accumulate(lfsu_v_s, 2*tangDim+1,
                          0.5 * density_up
                          * velocityNormal_s
                          * velocities_avg[2*tangDim+1][tangDim]
                          * faceVolume);
            r_n.accumulate(lfsu_v_n, 2*tangDim+1,
                          -0.5 * density_up
                          * velocityNormal_n
                          * velocities_avg[2*tangDim+1][tangDim]
                          * faceVolume);

#if PRINT_ACCUMULATION_TERM
std::cout << "INERTIA MOMENTUM TANGENTIAL @" <<
" faceCentersGlobal_s[2*tangDim]: " << faceCentersGlobal_s[2*tangDim] <<
" accu: " << (0.5 * density_up
                          * velocityNormal_s
                          * velocities_avg[2*tangDim][tangDim]
                          * faceVolume) <<
std::endl;
std::cout << "INERTIA MOMENTUM TANGENTIAL @" <<
" faceCentersGlobal_n[2*tangDim]: " << faceCentersGlobal_n[2*tangDim] <<
" accu: " << (-0.5 * density_up
                          * velocityNormal_n
                          * velocities_avg[2*tangDim][tangDim]
                          * faceVolume) <<
std::endl;
std::cout << "INERTIA MOMENTUM TANGENTIAL @" <<
" faceCentersGlobal_s[2*tangDim+1]: " << faceCentersGlobal_s[2*tangDim+1] <<
" accu: " << (0.5 * density_up
                          * velocityNormal_s
                          * velocities_avg[2*tangDim+1][tangDim]
                          * faceVolume) <<
std::endl;
std::cout << "INERTIA MOMENTUM TANGENTIAL @" <<
" faceCentersGlobal_n[2*tangDim+1]: " << faceCentersGlobal_n[2*tangDim+1] <<
" accu: " << (-0.5 * density_up
                          * velocityNormal_n
                          * velocities_avg[2*tangDim+1][tangDim]
                          * faceVolume) <<
std::endl;
#endif // PRINT_ACCUMULATION_TERM
          }
#endif // ENABLE_NAVIER_STOKES

          /**
           * (3) \b Viscous term of \b momentum balance equation
           *
           * \f[
           *    - \boldsymbol{\tau}
           *    = - \nu \rho \nabla v
           *    \Rightarrow - \int_\gamma \boldsymbol{\tau} \cdot n
           *    = - \int_\gamma \nu \rho \left( \nabla v \cdot n \right)
           * \f]
           * Tangential cases for all coordinate axes (given for \b 2-D,
           * \b rectangular grids and \f$n=(0,1)^T\f$ which means balancing
           * momentum for \f$v_\textrm{0}\f$)<br>
           * \f[
           *    \alpha_\textrm{self}
           *    = - \left( \boldsymbol{\tau} \cdot n \right) \cdot t
           *    = - |\gamma| \varrho_\textrm{avg} \nu_\textrm{avg}
           *      \frac{\partial v_{0}}{\partial x_{1}}
           * \f]
           * Tangential case, for: \f$\frac{\partial v_{0}}{\partial x_{1}}\f$,
           * right and left means along \f$x_1\f$ axis, \f$n\f$ means 1st entry,
           * \f$t\f$ means 0th entry
           * \f[
           *    A
           *    = - |\gamma| \varrho_\textrm{avg} \nu_\textrm{avg}
           *      \frac{\partial v_{0}}{\partial x_{1}}
           *    = - |\gamma| \varrho_\textrm{avg} \nu_\textrm{avg}
           *      \left( 0.5 \left[\frac{\left(v_\textrm{right} - v_\textrm{left} \right|_\textrm{t}}
           *                  {\left| x_\textrm{right} - x_\textrm{left} \right|_\textrm{n}}
           *                \right]_\textrm{curDim}
           *           + 0.5 \left[ \frac{\left(v_\textrm{right} - v_\textrm{left} \right|_\textrm{t}}
           *                  {\left| x_\textrm{right} - x_\textrm{left} \right|_\textrm{n}}
           *                \right]_\textrm{curDim+1}
           *      \right)
           * \f]
           * and \f$\varrho_\textrm{avg}\f$, \f$\nu_\textrm{avg}\f$ as distance-weighted average
           */
          // only tangential case, exclude normal case
          if (curDim != normDim && dim > 1)
          {
            r_s.accumulate(lfsu_v_s, 2*tangDim,
                           -0.5 * density_avg * kinematicViscosity_avg
                           * (velocities_n[2*tangDim][tangDim] - velocities_s[2*tangDim][tangDim])
                             / (outsideCellCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                           * faceUnitOuterNormal[normDim] * faceVolume);
            r_n.accumulate(lfsu_v_n, 2*tangDim,
                           0.5 * density_avg * kinematicViscosity_avg
                           * (velocities_n[2*tangDim][tangDim] - velocities_s[2*tangDim][tangDim])
                             / (outsideCellCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                           * faceUnitOuterNormal[normDim] * faceVolume);

            r_s.accumulate(lfsu_v_s, 2*tangDim+1,
                           -0.5 * density_avg * kinematicViscosity_avg
                           * (velocities_n[2*tangDim+1][tangDim] - velocities_s[2*tangDim+1][tangDim])
                             / (outsideCellCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                           * faceUnitOuterNormal[normDim] * faceVolume);
            r_n.accumulate(lfsu_v_n, 2*tangDim+1,
                           0.5 * density_avg * kinematicViscosity_avg
                           * (velocities_n[2*tangDim+1][tangDim] - velocities_s[2*tangDim+1][tangDim])
                             / (outsideCellCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                           * faceUnitOuterNormal[normDim] * faceVolume);

#if PRINT_ACCUMULATION_TERM
std::cout << "VISCOUS MOMENTUM TANGENTIAL @" <<
" faceCentersGlobal_s[2*tangDim]: " << faceCentersGlobal_s[2*tangDim] <<
" accu: " << (-0.5 * density_avg * kinematicViscosity_avg
                           * (velocities_n[2*tangDim][tangDim] - velocities_s[2*tangDim][tangDim])
                             / (outsideCellCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                           * faceUnitOuterNormal[normDim] * faceVolume) <<
std::endl;
std::cout << "VISCOUS MOMENTUM TANGENTIAL @" <<
" faceCentersGlobal_n[2*tangDim]: " << faceCentersGlobal_n[2*tangDim] <<
" accu: " << (0.5 * density_avg * kinematicViscosity_avg
                           *(velocities_n[2*tangDim][tangDim] - velocities_s[2*tangDim][tangDim])
                             / (outsideCellCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                           * faceUnitOuterNormal[normDim] * faceVolume) <<
std::endl;
std::cout << "VISCOUS MOMENTUM TANGENTIAL @" <<
" faceCentersGlobal_s[2*tangDim+1]: " << faceCentersGlobal_s[2*tangDim+1] <<
" accu: " << (-0.5 * density_avg * kinematicViscosity_avg
                           * (velocities_n[2*tangDim+1][tangDim] - velocities_s[2*tangDim+1][tangDim])
                             / (outsideCellCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                           * faceUnitOuterNormal[normDim] * faceVolume) <<
std::endl;
std::cout << "VISCOUS MOMENTUM TANGENTIAL @" <<
" faceCentersGlobal_n[2*tangDim+1]: " << faceCentersGlobal_n[2*tangDim+1] <<
" accu: " << (0.5 * density_avg * kinematicViscosity_avg
                           * (velocities_n[2*tangDim+1][tangDim] - velocities_s[2*tangDim+1][tangDim])
                             / (outsideCellCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                           * faceUnitOuterNormal[normDim] * faceVolume) <<
std::endl;
#endif // PRINT_ACCUMULATION_TERM
          }
        }
      }

//     typedef typename LFSU1::Traits::SizeType size_type;
//     typedef typename IG::Element Element;
//     typedef typename Element::Geometry::Jacobian GeometryJacobian;
//
//     const double h_F = (ig.geometry().corner(0) - ig.geometry().corner(1)).two_norm();
//
//     // dimensions
//     const int dim = IG::dimension;
//
//     // select quadrature rule
//     Dune::GeometryType gtface = ig.geometryInInside().type();
//     const Dune::QuadratureRule<DF,dim-1>& rule = Dune::QuadratureRules<DF,dim-1>::rule(gtface,_intorder);
//
//     // save entity pointers to adjacent elements
//     Element e1 = ig.insideElement();
//     Element e2 = ig.outsideElement();
//
//     // loop over quadrature points and integrate normal flux
//     for (typename Dune::QuadratureRule<DF,dim-1>::const_iterator it=rule.begin(); it!=rule.end(); ++it)
//       {
//         // position of quadrature point in local coordinates of element
//         Dune::FieldVector<DF,dim> local1 = ig.geometryInInside().global(it->position());
//         Dune::FieldVector<DF,dim> local2 = ig.geometryInOutside().global(it->position());
//
//         // evaluate ansatz shape functions (assume Galerkin for now)
//         std::vector<RangeType> phi1(lfsv1.size());
//         lfsv1.finiteElement().localBasis().evaluateFunction(local1,phi1);
//
//         std::vector<RangeType> phi2(lfsv2.size());
//         lfsv2.finiteElement().localBasis().evaluateFunction(local2,phi2);
//
//         // evaluate gradient of shape functions
//         std::vector<JacobianType> js1(lfsu1.size());
//         lfsu1.finiteElement().localBasis().evaluateJacobian(local1,js1);
//         std::vector<JacobianType> js2(lfsu2.size());
//         lfsu2.finiteElement().localBasis().evaluateJacobian(local2,js2);
//
//         // transform gradient to real element
//         const GeometryJacobian jac1 = e1.geometry().jacobianInverseTransposed(local1);
//         const GeometryJacobian jac2 = e2.geometry().jacobianInverseTransposed(local2);
//
//         std::vector<Dune::FieldVector<RF,dim> > gradphi1(lfsu1.size());
//         for (size_t i=0; i<lfsu1.size(); i++)
//           {
//             gradphi1[i] = 0.0;
//             jac1.umv(js1[i][0],gradphi1[i]);
//           }
//         std::vector<Dune::FieldVector<RF,dim> > gradphi2(lfsu2.size());
//         for (size_t i=0; i<lfsu2.size(); i++)
//           {
//             gradphi2[i] = 0.0;
//             jac2.umv(js2[i][0],gradphi2[i]);
//           }
//
//         // compute gradient of u1
//         Dune::FieldVector<RF,dim> gradu1(0.0);
//         for (size_t i=0; i<lfsu1.size(); i++)
//           gradu1.axpy(x1(lfsu1,i),gradphi1[i]);
//         // compute gradient of u2
//         Dune::FieldVector<RF,dim> gradu2(0.0);
//         for (size_t i=0; i<lfsu2.size(); i++)
//           gradu2.axpy(x2(lfsu2,i),gradphi2[i]);
//
//
//         RF u1(0.0);
//         for (size_t i=0; i<lfsu1.size(); i++)
//           u1 += x1(lfsu1,i) * phi1[i];
//
//         RF u2(0.0);
//         for (size_t i=0; i<lfsu2.size(); i++)
//           u2 += x2(lfsu2,i) * phi2[i];
//
//         Dune::FieldVector<DF,dim> normal1 = ig.unitOuterNormal(it->position());
//         Dune::FieldVector<DF,dim> normal2 = ig.unitOuterNormal(it->position());
//         normal2 *= -1;
//         //const RF mean_u = 0.5*(u1 + u2);
//         const RF jump_u1 = u1 - u2;
//         const RF jump_u2 = u2 - u1;
//         Dune::FieldVector<RF,dim> mean_gradu = gradu1 + gradu2;
//         mean_gradu *= 0.5;
//         const double theta = 1;
//         const double alpha = _intensity;
//
//         // integrate
//         const RF factor = it->weight()*ig.geometry().integrationElement(it->position());
//         for (size_type i=0; i<lfsv1.size(); i++)
//           {
//             RF value = 0.0;
//             value -= /* epsilon * */ (mean_gradu * normal1) * phi1[i];
//             value -= theta * /* epsilon * */ 0.5 * (gradphi1[i] * normal1) * jump_u1;
//             value += alpha / h_F * jump_u1 * phi1[i];
//             r1.accumulate(lfsv1,i,factor * value);
//           }
//         for (size_type i=0; i<lfsv2.size(); i++)
//           {
//             RF value = 0.0;
//             value -= /* epsilon * */ (mean_gradu * normal2) * phi2[i];
//             value -= theta * /* epsilon * */ 0.5 * (gradphi2[i] * normal2) * jump_u2;
//             value += alpha / h_F * jump_u2 * phi2[i];
//             r2.accumulate(lfsv2,i,factor * value);
//           }
//       }
//   }
};

#endif // DUMUX_MULTIDOMAIN_STOKESSTOKES_COUPLING_HH
