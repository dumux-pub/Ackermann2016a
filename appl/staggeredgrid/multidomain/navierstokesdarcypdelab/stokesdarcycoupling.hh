// -*- tab-width: 4; indent-tabs-mode: nil -*-
#ifndef MULTIDOMAIN_TEST_STOKESDARCY_COUPLING_HH
#define MULTIDOMAIN_TEST_STOKESDARCY_COUPLING_HH

namespace Dune {
namespace PDELab {

enum class CouplingMode
{
    Darcy,
    Stokes,
    Both
};

/** a local operator for solving
 *
 * ...
 *
 */

template<typename TReal>
class StokesDarcyCoupling :
        public Dune::PDELab::MultiDomain::CouplingOperatorDefaultFlags,
        public Dune::PDELab::InstationaryLocalOperatorDefaultMethods<TReal>
{
public:
    static const bool doAlphaCoupling = true;

    //! \brief Index of velocity
    static const unsigned int velocityIdx = 0;
    static const unsigned int pressureIdx = 1;

    //! constructor: pass parameter object
    StokesDarcyCoupling(
            CouplingMode coupling_mode
    )
    : _coupling_mode(coupling_mode),
      _epsilon(1e-8)
    {}

    template<typename IG, typename LFSUStokes, typename LFSUDarcy,
    typename X, typename LFSVStokes, typename LFSVDarcy,
    typename R>
    void alpha_coupling(const IG& ig,
            const LFSUStokes& lfsu_s, const X& x_s, const LFSVStokes& lfsv_s,
            const LFSUDarcy& lfsu_n, const X& x_n, const LFSVDarcy& lfsv_n,
            R& r_s, R& r_n) const
    {
        // select the velocity component from the subspaces for Navier-Stokes
        // other components are not needed
        typedef typename LFSUStokes::template Child<velocityIdx>::Type LFSU_V;
        const LFSU_V& lfsu_v_s = lfsu_s.template child<velocityIdx>();

        // domain and range field type
        typedef typename LFSU_V::Traits::FiniteElementType::
                Traits::LocalBasisType::Traits::DomainFieldType DF;
        typedef typename LFSU_V::Traits::FiniteElementType::
                Traits::LocalBasisType::Traits::RangeFieldType RF;
        typedef typename LFSU_V::Traits::FiniteElementType::
                Traits::LocalBasisType::Traits::RangeType RangeVelocity;
        typedef typename LFSU_V::Traits::FiniteElementType::
                Traits::LocalBasisType::Traits::JacobianType JacobianVelocity;

        // /////////////////////
        // geometry information

        // dimension
        static const unsigned int dim = IG::Geometry::dimension;

        // local position of face center and face normal
        const Dune::FieldVector<DF, dim-1>& faceCenterLocal =
                Dune::ReferenceElements<DF, dim-1>::general(ig.geometry().type()).position(0, 0);
        const Dune::FieldVector<DF, dim>& faceUnitOuterNormal = ig.centerUnitOuterNormal();

        // evaluate orientation of intersection
        unsigned int normDim = 0;
        unsigned int tangDim = 1;
        for (unsigned int curDim = 0; curDim < dim; ++curDim)
        {
            if (std::abs(faceUnitOuterNormal[curDim]) > 1e-10 && dim > 1)
            {
                normDim = curDim;
                tangDim = 1 - curDim;
            }
        }

        // face midpoints of all faces
        const unsigned int numFaces =
                Dune::ReferenceElements<DF, dim>::general(ig.inside()->type()).size(1);
        std::vector<Dune::FieldVector<DF, dim> > faceCentersLocal_s(numFaces);
        std::vector<Dune::FieldVector<DF, dim> > faceCentersLocal_n(numFaces);
        std::vector<Dune::FieldVector<DF, dim> > faceCentersGlobal_s(numFaces);
        std::vector<Dune::FieldVector<DF, dim> > faceCentersGlobal_n(numFaces);
        for (unsigned int curFace = 0; curFace < numFaces; ++curFace)
        {
            faceCentersLocal_s[curFace] =
                    Dune::ReferenceElements<DF, dim>::general(ig.inside()->geometry().type()).position(curFace, 1);
            faceCentersLocal_n[curFace] =
                    Dune::ReferenceElements<DF, dim>::general(ig.outside()->geometry().type()).position(curFace, 1);
            faceCentersGlobal_s[curFace] = ig.inside()->geometry().global(faceCentersLocal_s[curFace]);
            faceCentersGlobal_n[curFace] = ig.outside()->geometry().global(faceCentersLocal_n[curFace]);
        }

        // face volume for integration
        RF faceVolume = ig.geometry().integrationElement(faceCenterLocal)
                                               * Dune::ReferenceElements<DF, dim-1>::general(ig.geometry().type()).volume();

        // /////////////////////
        // velocities

        // evaluate shape functions at all face midpoints
        std::vector<std::vector<RangeVelocity> > velocityBasis_s(numFaces);
        for (unsigned int curFace = 0; curFace < numFaces; ++curFace)
        {
            velocityBasis_s[curFace].resize(lfsu_v_s.size());
            lfsu_v_s.finiteElement().localBasis().evaluateFunction(
                    faceCentersLocal_s[curFace], velocityBasis_s[curFace]);
        }

        // evaluate velocity on midpoints of all faces
        std::vector<RangeVelocity> velocities_s(numFaces);
        for (unsigned int curFace = 0; curFace < numFaces; ++curFace)
        {
            velocities_s[curFace] = RangeVelocity(0.0);
            for (unsigned int i = 0; i < lfsu_v_s.size(); i++)
            {
                velocities_s[curFace].axpy(x_s(lfsu_v_s, i), velocityBasis_s[curFace][i]);
            }
        }

        // /////////////////////
        // evaluation of unknown, upwinding and averaging

        // evaluate cell values
        DF pressure_n = x_n(lfsu_n, 0);

        // upwinding on staggered intersection
        std::vector<RF> velocity_s_up(dim);
        for (unsigned int curDim = 0; curDim < dim; ++curDim)
        {
            velocity_s_up[curDim] = velocities_s[curDim*2][curDim];
            if (velocity_s_up[curDim] < 0)
            {
                velocity_s_up[curDim] = velocities_s[curDim*2+1][curDim];
            }
        }

        //! \todo TODO: remove hard coded gravitional acceleration
        //         Dune::FieldVector<DF, dim> gravity(0.0);
        //         gravity[dim-1] = -9.81;

        // normal velocity
        RF normalAtInterfaceVelocity_s = velocities_s[ig.indexInInside()] * faceUnitOuterNormal;

        RF fromPermeability = std::pow(1.0, -0.5); // TODO: take value from DarcyK function
        RF alphaBeaversJoseph = 1.0;

        unsigned int indexTangentialVelocity0 = (1 - normDim) * 2; // TODO: extend to 3d
        unsigned int indexTangentialVelocity1 = indexTangentialVelocity0 + 1;

        if (_coupling_mode == CouplingMode::Darcy || _coupling_mode == CouplingMode::Both)
        {
            // pressure for Darcy (continuity of normal mass fluxes)
            r_n.accumulate(lfsu_n, 0,
                    -1.0 * normalAtInterfaceVelocity_s
                    * faceVolume);
        }
        if (_coupling_mode == CouplingMode::Stokes || _coupling_mode == CouplingMode::Both)
        {
            // Neumann boundary condition for normal momentum equation of Stokes
            // (continuity of stress in normal direction)
            r_s.accumulate(lfsu_v_s, ig.indexInInside(),
                    faceUnitOuterNormal[normDim]
                                        * pressure_n
                                        * faceVolume);
            // Robin boundary condition for tangential momentum equation of Stokes
            // (Beavers-Joseph-Saffman condition)
            r_s.accumulate(lfsu_v_s, indexTangentialVelocity0,
                    -0.5 * alphaBeaversJoseph
                    * fromPermeability
                    * velocities_s[2*tangDim][tangDim]
                                              * faceVolume);
            r_s.accumulate(lfsu_v_s, indexTangentialVelocity1,
                    -0.5 * alphaBeaversJoseph
                    * fromPermeability
                    * velocities_s[2*tangDim+1][tangDim]
                                                * faceVolume);

        }


    } // void alpha_coupling


    //! compute local jacobian of the skeleton term
      template<typename IG, typename LFSU_S, typename LFSU_N,
               typename X, typename LFSV_S, typename LFSV_N,
               typename Jacobian>
      void jacobian_coupling
      ( const IG& ig,
        const LFSU_S& lfsu_s, const X& x_s, const LFSV_S& lfsv_s,
        const LFSU_N& lfsu_n, const X& x_n, const LFSV_N& lfsv_n,
        Jacobian& mat_ss, Jacobian& mat_sn,
        Jacobian& mat_ns, Jacobian& mat_nn) const
      {
          if (_coupling_mode == CouplingMode::Darcy)
            return;

        typedef typename X::value_type D;
        typedef typename Jacobian::value_type R;
        typedef LocalVector<R,TestSpaceTag,typename Jacobian::weight_type> ResidualVector;
        typedef typename ResidualVector::WeightedAccumulationView ResidualView;

        const int m_s=lfsv_s.size();
        const int n_s=lfsu_s.size();
        const int n_n=lfsu_n.size();

        X u_s(x_s);
        X u_n(x_n);

        ResidualVector down_s(mat_ss.nrows()),up_s(mat_ss.nrows());
        ResidualView downview_s = down_s.weightedAccumulationView(1.0);
        ResidualView upview_s = up_s.weightedAccumulationView(1.0);

        ResidualVector down_n(mat_nn.nrows()),up_n(mat_nn.nrows());
        ResidualView downview_n = down_n.weightedAccumulationView(1.0);
        ResidualView upview_n = up_n.weightedAccumulationView(1.0);

        // base line
        alpha_coupling(ig,lfsu_s,u_s,lfsv_s,lfsu_n,u_n,lfsv_n,downview_s,downview_n);

        // jiggle in self
        for (int j=0; j<n_s; j++)
          {
            up_s = 0.0;
            up_n = 0.0;
            D delta = _epsilon*(1.0+std::abs(u_s(lfsu_s,j)));
            u_s(lfsu_s,j) += delta;
            alpha_coupling(ig,lfsu_s,u_s,lfsv_s,lfsu_n,u_n,lfsv_n,upview_s,upview_n);
            for (int i=0; i<m_s; i++)
              mat_ss.accumulate(lfsv_s,i,lfsu_s,j,(up_s(lfsv_s,i)-down_s(lfsv_s,i))/delta);
            u_s(lfsu_s,j) = x_s(lfsu_s,j);
          }

        // jiggle in neighbor
        for (int j=0; j<n_n; j++)
          {
            up_s = 0.0;
            up_n = 0.0;
            D delta = _epsilon*(1.0+std::abs(u_n(lfsu_n,j)));
            u_n(lfsu_n,j) += delta;
            alpha_coupling(ig,lfsu_s,u_s,lfsv_s,lfsu_n,u_n,lfsv_n,upview_s,upview_n);
            u_n(lfsu_n,j) = x_n(lfsu_n,j);
          }
      }

//private:
    CouplingMode _coupling_mode;
    double _epsilon;
};

}
}

#endif // MULTIDOMAIN_TEST_STOKESDARCY_COUPLING_HH
