/**
 * \file
 * \ingroup TestProblems
 *
 * \brief Boundary condition types for Stokes 1p / Darcy 1p coupled problem.
 *
 * All examples have analytic solutions; thus the error can be computed. The
 * domain is \f$(0,0) \times (0,2)\f$ and is divided in the upper Stokes
 * domain \f$(0,1) \times (0,2)\f$ and the lower Darcy domain
 * \f$(0,0) \times (0,1)\f$.
 *
 * The Boundary conditions are for Stokes:
 * - Inflow everywhere except interface for velocity
 * - Darcy everywhere expcept interface for pressure
 * - Outflow at interface for pressure
 * The Boundary conditions are for Darcy:
 * - Darcy at the bottom
 * - Neumann no-flow at the left and right
 *
 * # Test Case B #
 *
 * This example has flux only normal to the interface and the pressure is
 * continuous, thus in the continuity of the normal stresses the two
 * pressures are equal \f$p_\text{pm} = p_\text{ff}\f$.
 * \f[
 * v = \begin{pmatrix} 0 \\ 1 \end{pmatrix}
 * \f]
 * \f[
 * p_\text{Darcy} = p_\text{Stokes} = -y
 * \f]
 * \f[
 * q_\text{mass} = 0
 * \f]
 * \f[
 * q_\text{momentum} = \begin{pmatrix} 0 \\ -1 \end{pmatrix}
 * \f]
 * \f[
 * q_\text{Darcy} = 0
 * \f]
 * The example works for both Stokes and Naiver-Stokes.
 *
 * This was case Ba, case Bb is the same but the velocity is directed
 * toward the coupling interface and the pressure gradient is flipped,
 * too.
 * \f[
 * v = \begin{pmatrix} 0 \\ -1 \end{pmatrix}
 * \f]
 * \f[
 * p_\text{Darcy} = p_\text{Stokes} = y
 * \f]
 * \f[
 * q_\text{mass} = 0
 * \f]
 * \f[
 * q_\text{momentum} = \begin{pmatrix} 0 \\ 1 \end{pmatrix}
 * \f]
 * \f[
 * q_\text{Darcy} = 0
 * \f]
 *
 * # Test Case C #
 *
 * This example has flux only tangential to the interface and the pressure
 * is continuous, actual constant, thus in the continuity of the normal
 * stresses the two pressures are equal \f$p_\text{pm} = p_\text{ff}\f$.
 * \f[
 * v = \begin{pmatrix} e^{-y} \\ 0 \end{pmatrix}
 * \f]
 * \f[
 * p_\text{Darcy} = p_\text{Stokes} = 7
 * \f]
 * \f[
 * q_\text{mass} = 0
 * \f]
 * \f[
 * q_\text{momentum} = \begin{pmatrix} -e^{-y} \\ 0 \end{pmatrix}
 * \f]
 * \f[
 * q_\text{Darcy} = 0
 * \f]
 *
 * # Test Case D #
 *
 * This example has flux only tangential to the interface and the pressure
 * is continuous, actual constant, thus in the continuity of the normal
 * stresses the two pressures are equal \f$p_\text{pm} = p_\text{ff}\f$.
 * \f[
 * v = \begin{pmatrix} e^{-y} \\ 0 \end{pmatrix}
 * \f]
 * \f[
 * p_\text{Darcy} = p_\text{Stokes} = 7
 * \f]
 * \f[
 * q_\text{mass} = 0
 * \f]
 * \f[
 * q_\text{momentum} = \begin{pmatrix} -e^{-y} \\ 0 \end{pmatrix}
 * \f]
 * \f[
 * q_\text{Darcy} = 0
 * \f]
 *
 * # Test Case E #
 *
 * This example has flux only tangential to the interface and the pressure
 * is continuous, actual constant, thus in the continuity of the normal
 * stresses the two pressures are equal \f$p_\text{pm} = p_\text{ff}\f$.
 * \f[
 * v = \begin{pmatrix} e^{-y} \\ 0 \end{pmatrix}
 * \f]
 * \f[
 * p_\text{Darcy} = p_\text{Stokes} = 7
 * \f]
 * \f[
 * q_\text{mass} = 0
 * \f]
 * \f[
 * q_\text{momentum} = \begin{pmatrix} -e^{-y} \\ 0 \end{pmatrix}
 * \f]
 * \f[
 * q_\text{Darcy} = 0
 * \f]
 */

#ifndef DUMUX_MULTIDOMAIN_PDELAB_PROBLEM_KANSCHAT_RIVIERE_MASS_HH
#define DUMUX_MULTIDOMAIN_PDELAB_PROBLEM_KANSCHAT_RIVIERE_MASS_HH

#include<cmath>
#include<dune/pdelab/common/function.hh>
#include<dune/pdelab/constraints/common/constraintsparameters.hh>
#include <dune/pdelab/localoperator/diffusionparam.hh>

#define KINEMATIC_VISCOSITY 1e-0
#define DENSITY 1
#define ENABLE_NAVIER_STOKES 0

#define EPSILON 1e-9

#define TEST_CASE_Ba
// #define TEST_CASE_Bb
// #define TEST_CASE_C
// #define TEST_CASE_D
// #define TEST_CASE_E
// #define TEST_CASE_F

// check whether Stokes or Navier-Stokes


// ////////////////////////////////////////////////////////////
// Boundary conditions for Navier-Stokes subdomain
// ////////////////////////////////////////////////////////////

/**
 * \brief Boundary condition function for the velocity components.
 */
class BCVelocity
: public Dune::PDELab::DirichletConstraintsParameters
{
public:
  //! \brief Return whether Intersection is Wall Boundary for velocity
  //! \tparam I Intersection type
  template<typename I>
  bool isWall(const I& intersection,
              const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
    return false;
  }

  //! \brief Return whether Intersection is Inflow Boundary for velocity
  //! \tparam I Intersection type
  template<typename I>
  bool isInflow(const I& intersection,
                const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
    // everywhere except bottom
    Dune::FieldVector<typename I::ctype, I::dimension> global =
      intersection.geometry().global(coord);
//     return (global[1] > 2.0 - EPSILON);
    return (global[1] > 1.0 + EPSILON);
  }

  //! \brief Return whether Intersection is Outflow Boundary for velocity
  //! \tparam I Intersection type
  template<typename I>
  bool isOutflow(const I& intersection,
                 const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
    return false;
  }

  //! \brief Return whether Intersection is Symmetry Boundary for velocity
  //! \tparam I Intersection type
  template<typename I>
  bool isSymmetry(const I& intersection,
                 const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
    return false;
//     Dune::FieldVector<typename I::ctype, I::dimension> global =
//       intersection.geometry().global(coord);
//     return (global[1] < 2.0 - EPSILON && global[1] > 1.0 + EPSILON);
  }
};

/**
 * \brief Boundary condition function for the pressure component.
 */
class BCPressure
: public Dune::PDELab::DirichletConstraintsParameters
{
public:
  //! \brief Return whether Intersection is Dirichlet Boundary for pressure
  //! \tparam I Intersection type
  template<typename I>
  bool isDirichlet(const I& intersection,
                   const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
    // only top
    Dune::FieldVector<typename I::ctype, I::dimension> global =
      intersection.geometry().global(coord);
    return (global[1] > 2.0 - EPSILON);
  }

  //! \brief Return whether Intersection is Outflow Boundary for pressure
  //! \tparam I Intersection type
  template<typename I>
  bool isOutflow(const I& intersection,
                 const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
    return !isDirichlet(intersection, coord);
  }
};

/**
 * \brief Function for velocity Dirichlet boundary conditions and initialization.
 *
 * \tparam GV GridView type
 * \tparam RF Range type
 */
template<typename GV, typename RF>
class DirichletVelocity
: public Dune::PDELab::AnalyticGridFunctionBase<
    Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, GV::dimension>, DirichletVelocity<GV, RF> >
{
public:
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, GV::dimension> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, DirichletVelocity<GV, RF> > BaseT;

  //! \brief Constructor
  DirichletVelocity(const GV& gv)
  : BaseT(gv)
  {}

  //! \brief Evaluate the function globally
  inline void evaluateGlobal(const typename Traits::DomainType& x,
                             typename Traits::RangeType& y) const
  {
#if defined TEST_CASE_Ba
    y[0] = 0.0;
    y[1] = 1.0;
#elif defined TEST_CASE_Bb
    y[0] = 0.0;
    y[1] = -1.0;
#elif defined TEST_CASE_C
    y[0] = std::exp(-1.0 * x[1]);
    y[1] = 0.0;
#elif defined TEST_CASE_D
    y[0] = 0.0;
    y[1] = x[1];
#elif defined TEST_CASE_E
    y[0] = 100.0 * std::exp(-1.0 * x[1]);
    y[1] = 2.0 * x[1];
#elif defined TEST_CASE_F
    y[0] = 1000.0 * (x[1] - 1.0);
    y[1] = x[0] * x[0];
#endif
  }

  RF time = 0.0;
};

/**
 * \brief Function for pressure Dirichlet boundary conditions and initialization.
 *
 * \tparam GV GridView type
 * \tparam RF Range type
 */
template<typename GV, typename RF>
class DirichletPressure
: public Dune::PDELab::AnalyticGridFunctionBase<
    Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, 1>, DirichletPressure<GV, RF> >,
  public Dune::PDELab::InstationaryFunctionDefaults
{
public:
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, 1> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, DirichletPressure<GV, RF> > BaseT;

  //! \brief Constructor
  DirichletPressure(const GV& gv)
  : BaseT(gv) {}

  //! \brief Evaluate the function globally
  inline void evaluateGlobal(const typename Traits::DomainType& x,
                             typename Traits::RangeType& y) const
  {
#if defined TEST_CASE_Ba
    y = -1.0 * x[1];
#elif defined TEST_CASE_Bb
    y = x[1];
#elif defined TEST_CASE_C
    y = 7.0;
#elif defined TEST_CASE_D
    y = -1.0 * x[1] + 7.0;
#elif defined TEST_CASE_E
    y = -2.0 * x[1] + 6.0;
#elif defined TEST_CASE_F
    y = x[0] * x[0] - x[0] * x[0] * x[0] * x[0] + 5.0;
#endif
  }
};

/**
 * \brief Function for velocity Neumann boundary conditions.
 *
 * \tparam GV GridView type
 * \tparam RF Range type
 */
template<typename GV, typename RF>
class NeumannVelocity
: public Dune::PDELab::AnalyticGridFunctionBase<
    Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, GV::dimension>, NeumannVelocity<GV, RF> >
{
public:
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, GV::dimension> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, NeumannVelocity<GV, RF> > BaseT;

  //! \brief Constructor
  NeumannVelocity(const GV& gv)
  : BaseT(gv) {}

  //! \brief Evaluate the function globally
  inline void evaluateGlobal(const typename Traits::DomainType& x,
                             typename Traits::RangeType& y) const
  {
    y = 0;
  }
};

/**
 * \brief Function for pressure Neumann boundary conditions.
 *
 * \tparam GV GridView type
 * \tparam RF Range type
 */
template<typename GV, typename RF>
class NeumannPressure
: public Dune::PDELab::AnalyticGridFunctionBase<
    Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, 1>, NeumannPressure<GV, RF> >
{
public:
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, 1> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, NeumannPressure<GV, RF> > BaseT;

  //! \brief Constructor
  NeumannPressure(const GV& gv)
  : BaseT(gv) {}

  //! \brief Evaluate the function globally
  inline void evaluateGlobal(const typename Traits::DomainType& x,
                             typename Traits::RangeType& y) const
  {
    y = 0;
  }
};

/**
 * \brief Source term function for the momentum balance.
 *
 * \tparam GV GridView type
 * \tparam RF Range type
 */
template<typename GV, typename RF>
class SourceMomentumBalance
  : public Dune::PDELab::AnalyticGridFunctionBase<
      Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, GV::dimension>, SourceMomentumBalance<GV, RF> >
{
public:
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, GV::dimension> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, SourceMomentumBalance<GV, RF> > BaseT;

  //! \brief Constructor
  SourceMomentumBalance(const GV& gv)
  : BaseT(gv) {}

  //! \brief Evaluate the function globally
  inline void evaluateGlobal(const typename Traits::DomainType& x,
                             typename Traits::RangeType& y) const
  {
#if defined TEST_CASE_Ba
    y[0] = 0.0;
    y[1] = -1.0;
#elif defined TEST_CASE_Bb
    y[0] = 0.0;
    y[1] = 1.0;
#elif defined TEST_CASE_C
    y[0] = -1.0 * std::exp(-1.0 * x[1]);
    y[1] = 0.0;
#elif defined TEST_CASE_D
    y[0] = 0.0;
    y[1] = -1.0;
# if ENABLE_NAVIER_STOKES
    y[1] += 2.0 * x[1];
# endif
#elif defined TEST_CASE_E
    y[0] = -100.0 * std::exp(-1.0 * x[1]);
    y[1] = -2.0;
# if ENABLE_NAVIER_STOKES
    y[0] = (100.0 - 200.0 * x[1]) * std::exp(-1.0 * x[1]);
    y[1] = 8.0 * x[1] -2.0;
# endif
#elif defined TEST_CASE_F
    y[0] = 2 * x[0]
           - 4.0 * x[0] * x[0] * x[0]
           - 2000.0
           + 1000 * x[0] * x[0] * (2.0 * x[1] - 2.0);
    y[1] = 2000.0 * x[0] * (x[1] - 1.0) * (x[1] - 1.0) - 2.0;
#endif
  }
};

/**
 * \brief Source term function for the mass balance.
 *
 * \tparam GV GridView type
 * \tparam RF Range type
 */
template<typename GV, typename RF>
class SourceMassBalance
  : public Dune::PDELab::AnalyticGridFunctionBase<
      Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, 1>, SourceMassBalance<GV, RF> >
{
public:
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, 1> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, SourceMassBalance<GV, RF> > BaseT;

  //! \brief Constructor
  SourceMassBalance(const GV& gv)
  : BaseT(gv) {}

  //! \brief Evaluate the function globally
  inline void evaluateGlobal(const typename Traits::DomainType& x,
                             typename Traits::RangeType& y) const
  {
#if defined TEST_CASE_Ba || defined TEST_CASE_Bb || defined TEST_CASE_C || defined TEST_CASE_F
    y = 0.0;
#elif defined TEST_CASE_D
    y = 1.0;
#elif defined TEST_CASE_E
    y = 2.0;
#endif
  }
};

// ////////////////////////////////////////////////////////////
// Boundary conditions for Darcy subdomain
// ////////////////////////////////////////////////////////////

/**
 * \brief Scalar describing the permeability. (homogenious case)
 *
 * \tparam GV GridView type
 * \tparam RF Range type
 */
template<typename GV, typename RF>
class DarcyK
  : public Dune::PDELab::AnalyticGridFunctionBase<
      Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, 1>,
      DarcyK<GV, RF> >
{
public:
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, 1> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, DarcyK<GV, RF> > BaseT;

  DarcyK(const GV& gv)
  : BaseT(gv)
  {}

  void evaluateGlobal(const typename Traits::DomainType& x,
                      typename Traits::RangeType& y) const
  {
    y = 1.0;
  }
};

/**
 * \brief Scalar Helmholtz term.
 *
 * \tparam GV GridView type
 * \tparam RF Range type
 */
template<typename GV, typename RF>
class DarcyA0
  : public Dune::PDELab::AnalyticGridFunctionBase<
      Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, 1>,
      DarcyA0<GV, RF> >
{
public:
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, 1> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, DarcyA0<GV, RF> > BaseT;

  DarcyA0(const GV& gv)
  : BaseT(gv)
  {}

  void evaluateGlobal(const typename Traits::DomainType& x,
                      typename Traits::RangeType& y) const
  {
    y = 0.0;
  }
};

/**
 * \brief Source term.
 *
 * \tparam GV GridView type
 * \tparam RF Range type
 */
template<typename GV, typename RF>
class DarcySource
  : public Dune::PDELab::AnalyticGridFunctionBase<
      Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, 1>,
      DarcySource<GV,RF> >
{
public:
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, 1> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, DarcySource<GV, RF> > BaseT;

  DarcySource(const GV& gv)
  : BaseT(gv)
  {}

  void evaluateGlobal(const typename Traits::DomainType& x,
                      typename Traits::RangeType& y) const
  {
    y = 0.0;
#if defined TEST_CASE_F
    y = -2.0 * x[1];
#endif
  }
};

/**
 * \brief Select boundary conditions excluding for coupoling interface.
 *
 * \tparam GV GridView type
 * \tparam RF Range type
 */
template<typename GV>
class DarcyBC
: public Dune::PDELab::BoundaryGridFunctionBase<
    Dune::PDELab::BoundaryGridFunctionTraits<GV,
      Dune::PDELab::DiffusionBoundaryCondition::Type, 1,
      Dune::FieldVector<Dune::PDELab::DiffusionBoundaryCondition::Type, 1> >,
    DarcyBC<GV> >
{
  const GV& gv;

public:
  typedef Dune::PDELab::DiffusionBoundaryCondition BC;
  typedef Dune::PDELab::BoundaryGridFunctionTraits<GV,
    Dune::PDELab::DiffusionBoundaryCondition::Type, 1,
    Dune::FieldVector<Dune::PDELab::DiffusionBoundaryCondition::Type, 1> > Traits;
  typedef Dune::PDELab::BoundaryGridFunctionBase<Traits, DarcyBC<GV> > BaseT;

  DarcyBC(const GV& gv_)
  : gv(gv_)
  {}

  template<typename I>
  void evaluate(const Dune::PDELab::IntersectionGeometry<I>& ig,
                const typename Traits::DomainType& x,
                typename Traits::RangeType& y) const
  {
    const Dune::FieldVector<typename I::ctype, I::dimension> global =
      ig.geometry().global(x);

    y = global[1] < EPSILON ? BC::Dirichlet : BC::Neumann;
  }

  //! get a reference to the GridView
  const GV& getGridView()
  {
    return gv;
  }
};

/**
 * \brief Dirichlet boundary conditions.
 *
 * \tparam GV GridView type
 * \tparam RF Range type
 */
template<typename GV, typename RF>
class DarcyG
: public Dune::PDELab::AnalyticGridFunctionBase<
    Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, 1>, DarcyG<GV, RF> >,
  public Dune::PDELab::InstationaryFunctionDefaults
{
public:
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, 1> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, DarcyG<GV, RF> > BaseT;

  DarcyG(const GV& gv)
  : BaseT(gv)
  {}

  void evaluateGlobal(const typename Traits::DomainType& x,
                      typename Traits::RangeType& y) const
  {
#if defined TEST_CASE_Ba
    y = -1.0 * x[1];
# if ENABLE_NAVIER_STOKES
    y += 1.0;
# endif
#elif defined TEST_CASE_Bb
    y = x[1];
# if ENABLE_NAVIER_STOKES
    y += 1.0;
# endif
#elif defined TEST_CASE_C
    y = 7.0;
#elif defined TEST_CASE_D
    y = -1.0 * x[1] + 6.0;
# if ENABLE_NAVIER_STOKES
    y = -1.0 * x[1] + 7.0;
# endif
#elif defined TEST_CASE_E
    y = -2.0 * x[1] + 4.0;
# if ENABLE_NAVIER_STOKES
    y = -2.0 * x[1] + 8.0;
# endif
#elif defined TEST_CASE_F
    y = x[0] * x[0] * x[1] + 5.0;
#endif
  }
};

/**
 * \brief Neumann boundary conditions.
 *
 * \tparam GV GridView type
 * \tparam RF Range type
 */
template<typename GV, typename RF>
class DarcyJ
: public Dune::PDELab::AnalyticGridFunctionBase<
  Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, 1>, DarcyJ<GV, RF> >
{
public:
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, 1> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, DarcyJ<GV, RF> > BaseT;

  DarcyJ(const GV& gv)
  : BaseT(gv)
  {}

  void evaluateGlobal(const typename Traits::DomainType& x,
                      typename Traits::RangeType& y) const
  {
    y = 0.0;
  }
};

#endif // DUMUX_MULTIDOMAIN_PDELAB_PROBLEM_KANSCHAT_RIVIERE_MASS_HH
