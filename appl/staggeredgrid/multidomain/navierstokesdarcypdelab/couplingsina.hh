#ifndef DUMUX_MULTIDOMAIN_PDELAB_COUPLING_SINA_HH
#define DUMUX_MULTIDOMAIN_PDELAB_COUPLING_SINA_HH

#include <dune/geometry/quadraturerules.hh>

#include <dune/pdelab/localoperator/idefault.hh>
#include <dune/pdelab/localoperator/pattern.hh>

#include <dune/pdelab/multidomain/couplingutilities.hh>

/**
 * \tparam TReal Scalar value type
 */

template<typename K, typename TReal>
class CouplingSina :
        public Dune::PDELab::MultiDomain::CouplingOperatorDefaultFlags,
        public Dune::PDELab::MultiDomain::NumericalJacobianCoupling<CouplingSina<K, TReal> >,
        public Dune::PDELab::MultiDomain::NumericalJacobianApplyCoupling<CouplingSina<K, TReal> >,
        public Dune::PDELab::MultiDomain::FullCouplingPattern,
        public Dune::PDELab::InstationaryLocalOperatorDefaultMethods<TReal>
        {
        public:

            static const bool doAlphaCoupling = true;
            static const bool doPatternCoupling = true;

            template<typename IG, typename LFSU1, typename LFSU2, typename X,
            typename LFSV1, typename LFSV2, typename R>
            void alpha_coupling(const IG& ig, const LFSU1& lfsu_s, const X& x_s, const LFSV1& lfsv_s,
                    const LFSU2& lfsu_n, const X& x_n, const LFSV2 lfsv_n, R& r_s, R& r_n) const
            {
                // domain and range field type
                typedef typename LFSU1::Traits::FiniteElementType::
                        Traits::LocalBasisType::Traits::DomainFieldType DF;
                typedef typename LFSU1::Traits::FiniteElementType::
                        Traits::LocalBasisType::Traits::RangeFieldType RF;

                // cell centers in references elements
                const Dune::FieldVector<DF,IG::dimension>&
                  inside_local = Dune::ReferenceElements<DF,IG::dimension>::general(ig.inside()->type()).position(0,0);
                const Dune::FieldVector<DF,IG::dimension>&
                  outside_local = Dune::ReferenceElements<DF,IG::dimension>::general(ig.outside()->type()).position(0,0);

                // evaluate diffusion coefficient
                typename K::Traits::RangeType k_inside, k_outside;
                k.evaluate(*(ig.inside()),inside_local,k_inside);
                k.evaluate(*(ig.outside()),outside_local,k_outside);
                typename K::Traits::RangeType k_avg = 2.0/(1.0/(k_inside+1E-30) + 1.0/(k_outside+1E-30));

                // cell centers in global coordinates
                Dune::FieldVector<DF,IG::dimension>
                  inside_global = ig.inside()->geometry().center();
                Dune::FieldVector<DF,IG::dimension>
                  outside_global = ig.outside()->geometry().center();

                // distance between the two cell centers
                inside_global -= outside_global;
                RF deltaX = inside_global.two_norm();

                // face volume for integration
                RF faceVolume = ig.geometry().volume();

                r_s.accumulate(lfsu_s,0,k_avg*(x_s(lfsu_s,0)-x_n(lfsu_n,0))*faceVolume/deltaX);
                //r_n.accumulate(lfsu_n,0,-k_avg*(x_s(lfsu_s,0)-x_n(lfsu_n,0))*faceVolume/deltaX);
            }

            CouplingSina (const K& k_)
              : k(k_)
            {}

            private:
            const K& k;

        };
#endif // DUMUX_MULTIDOMAIN_PDELAB_COUPLING_SINA_HH
