#include "config.h"

#define ITERATIVE_COUPLING

#include <dune/common/parametertreeparser.hh>
#include <dune/grid/sgrid.hh>
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>

#include <dune/pdelab/backend/istlmatrixbackend.hh>
#include <dune/pdelab/backend/istlsolverbackend.hh>
#include <dune/pdelab/backend/istlvectorbackend.hh>
#include <dune/pdelab/backend/seqistlsolverbackend.hh>
#include <dune/pdelab/common/vtkexport.hh>
#include <dune/pdelab/constraints/noconstraints.hh>
#include <dune/pdelab/finiteelementmap/p0fem.hh>
#include <dune/pdelab/gridfunctionspace/subspace.hh>
#include <dune/pdelab/newton/newton.hh>

#include <dune/pdelab/multidomain/constraints.hh>
#include <dune/pdelab/multidomain/coupling.hh>
#include <dune/pdelab/multidomain/gridoperator.hh>
#include <dune/pdelab/multidomain/interpolate.hh>
#include <dune/pdelab/multidomain/multidomaingridfunctionspace.hh>
#include <dune/pdelab/multidomain/subproblem.hh>
#include <dune/pdelab/multidomain/subproblemlocalfunctionspace.hh>
#include <dune/pdelab/multidomain/vtk.hh>

#include <dune/pdelab/stationary/linearproblem.hh>
#include <dune/pdelab/gridoperator/onestep.hh>
#include <dune/pdelab/gridoperator/common/timesteppingparameterinterface.hh>
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>

#include "problemtransient.hh"                        // test case with transient boundary condition & analytic solution
#define ANALYTIC_SOLUTION // only for problemtransient! not for problemsina!
//#include "problemsina.hh"                             // 'realistic' test cases

#include "diffusionccfv.hh"
#include "l2.hh"
#include "../../common_pdelab/fixpressureconstraints.hh"
#include "../../common_pdelab/fixvelocityconstraints.hh"
#include "../../common_pdelab/l2interpolationerror.hh"
#include "../../localfunctions/staggeredq0fem.hh"
#include "../../freeflow/navierstokes/navierstokes_pdelab/navierstokesstaggeredgrid.hh"
#include "../../freeflow/navierstokes/navierstokes_pdelab/navierstokestransientstaggeredgrid.hh"

#include "couplingstokes1pdarcy1p.hh" // monolithic coupling
#include "stokesdarcycoupling.hh" // iterative coupling

template<template<class,class,class,int> class Preconditioner,
template<class> class Solver>
class ISTL_SEQ_Subblock_Backend
        : public Dune::PDELab::SequentialNorm
          , public Dune::PDELab::LinearResultStorage
          {
          public:
    /*! \brief make a linear solver object

    \param[in] maxiter_ maximum number of iterations to do
    \param[in] verbose_ print messages if true
     */
    explicit ISTL_SEQ_Subblock_Backend(unsigned block, unsigned maxiter_=5000, int verbose_=1)
    : _block(block)
    , maxiter(maxiter_)
    , verbose(verbose_)
    {}

    unsigned block() const
    {
        return _block;
    }

    void setBlock(unsigned block)
    {
        _block = block;
    }

    /*! \brief solve the given linear system

    \param[in] A the given matrix
    \param[out] z the solution vector to be computed
    \param[in] r right hand side
    \param[in] reduction to be achieved
     */
    template<class M, class V, class W>
    void apply(M& A, V& z, W& r, typename W::ElementType reduction)
    {
        static_assert(std::is_same<V,W>::value,"V and W must be identical");

        typedef typename Dune::PDELab::istl::raw_type<M>::type ISTLMatrix;
        typedef typename Dune::PDELab::istl::raw_type<V>::type ISTLVector;

        typedef typename ISTLMatrix::block_type MatrixBlock;
        typedef typename ISTLVector::block_type VectorBlock;

        Dune::MatrixAdapter<
        MatrixBlock,
        VectorBlock,
        VectorBlock
        > opa(Dune::PDELab::istl::raw(A)[_block][_block]);
        Preconditioner<
        MatrixBlock,
        VectorBlock,
        VectorBlock,
        1
        > prec(Dune::PDELab::istl::raw(A)[_block][_block], 3, 1.0);
        Solver<
        VectorBlock
        > solver(opa, prec, reduction, maxiter, verbose);
        Dune::InverseOperatorResult stat;
        solver.apply(Dune::PDELab::istl::raw(z)[_block], Dune::PDELab::istl::raw(r)[_block], stat);
        res.converged  = stat.converged;
        res.iterations = stat.iterations;
        res.elapsed    = stat.elapsed;
        res.reduction  = stat.reduction;
        res.conv_rate  = stat.conv_rate;
    }

          private:
    unsigned _block;
    unsigned maxiter;
    int verbose;
          };

int main(int argc, char** argv)
{
    try
    {
        Dune::MPIHelper::instance(argc,argv);

        if (argc != 2) {
            std::cerr << "Usage: " << argv[0] << " <ini file>" << std::endl;
            return 1;
        }

        Dune::ParameterTree parameters;
        Dune::ParameterTreeParser::readINITree(argv[1],parameters);

        // set up grid
        const int dim = 2;
        typedef Dune::SGrid<dim, dim, double> BaseGrid;
        Dune::FieldVector<double, dim> low(0.0);
        Dune::FieldVector<double, dim> high(0.0);
        high[0] = 1.0;
        high[1] = 2.0;
        Dune::FieldVector<int, dim> n(0.0);
        n[0] = 1;
        n[1] = 2;
        BaseGrid baseGrid(n, low, high);
        unsigned int refinementLevel = parameters.get<double>("mesh.refine");
        baseGrid.globalRefine(refinementLevel);
        typedef Dune::MultiDomainGrid<BaseGrid,Dune::mdgrid::FewSubDomainsTraits<BaseGrid::dimension,4> > Grid;
        Grid grid(baseGrid,false);
        typedef Grid::SubDomainGrid SubDomainGrid;
        SubDomainGrid& sdg0 = grid.subDomain(0);
        SubDomainGrid& sdg1 = grid.subDomain(1);
        typedef Grid::LeafGridView MDGV;
        typedef SubDomainGrid::LeafGridView SDGV;
        MDGV mdgv = grid.leafGridView();
        SDGV sdgv0 = sdg0.leafGridView();     // upper subdomain
        SDGV sdgv1 = sdg1.leafGridView();    // lower subdomain
        sdg0.hostEntityPointer(*sdgv0.begin<0>());
        grid.startSubDomainMarking();
        double interface = parameters.get<double>("mesh.interface");
        for (MDGV::Codim<0>::Iterator it = mdgv.begin<0>(); it != mdgv.end<0>(); ++it)
        {
            if (it->geometry().center()[1] > interface * (high[1] + low[1]))
            {
                grid.addToSubDomain(0, *it);
            }
            else
            {
                grid.addToSubDomain(1, *it);
            }
        }
        grid.preUpdateSubDomains();
        grid.updateSubDomains();
        grid.postUpdateSubDomains();

        // types and constants
        typedef double DF;
        typedef double RF;
        const int indexVelocity = 0;
        const int indexPressure = 1;
        const int indexUpper = 0;
        const int indexLower = 1;

        // instantiate finite element maps
        typedef Dune::PDELab::P0LocalFiniteElementMap<DF, RF, dim> P0FEM;
        P0FEM p0fem(Dune::GeometryType(Dune::GeometryType::cube, dim));
        typedef Dune::PDELab::StaggeredQ0LocalFiniteElementMap<DF, RF, dim> StaggeredQ0FEM;
        StaggeredQ0FEM staggeredq0fem;

        // set up functions defining the problem
        // functions for Navier-Stokes
        typedef Dune::PDELab::CompositeConstraintsParameters<BCVelocity, BCPressure> BCType;
        BCVelocity bcVelocity;
        BCPressure bcPressure;
        BCType bc(bcVelocity, bcPressure);
        DirichletVelocity<MDGV, RF> dirichletVelocity(mdgv);
        DirichletPressure<MDGV, RF> dirichletPressure(mdgv);
        NeumannVelocity<MDGV, RF> neumannVelocity(mdgv);
        NeumannPressure<MDGV, RF> neumannPressure(mdgv);
        SourceMomentumBalance<MDGV, RF> sourceMomentumBalance(mdgv);
        SourceMassBalance<MDGV, RF> sourceMassBalance(mdgv);
        typedef Dune::PDELab::CompositeGridFunction
                <DirichletVelocity<MDGV, RF>, DirichletPressure<MDGV, RF> > DirichletComposed;
        DirichletComposed dirichletComposed(dirichletVelocity, dirichletPressure);
        // functions for Darcy
        DarcyBC<MDGV> darcyBC(mdgv);
        DarcyG<MDGV, RF> darcyG(mdgv);
        DarcyK<MDGV, RF> darcyK(mdgv);
        DarcyA0<MDGV, RF> darcyA0(mdgv);
        DarcySource<MDGV,RF> darcySource(mdgv);
        DarcyJ<MDGV, RF> darcyJ(mdgv);

        // construct grid function spaces
        typedef Dune::PDELab::ISTLVectorBackend<> VectorBackend;
        typedef Dune::PDELab::ISTLVectorBackend<
                Dune::PDELab::ISTLParameters::dynamic_blocking
                > MDVBE;

        typedef Dune::PDELab::FixVelocityConstraints VelocityConstraints;
        VelocityConstraints velocityConstraints;
        typedef Dune::PDELab::GridFunctionSpace<SDGV, StaggeredQ0FEM, VelocityConstraints, VectorBackend> StaggeredQ0GFS;
        StaggeredQ0GFS staggeredQ0Gfs0(sdgv0, staggeredq0fem, velocityConstraints);
        staggeredQ0Gfs0.name("velocity");

        typedef Dune::PDELab::FixPressureConstraints<MDGV, BCType> PressureConstraints;
        PressureConstraints pressureConstraints(mdgv, bc);
        typedef Dune::PDELab::GridFunctionSpace<SDGV, P0FEM, PressureConstraints, VectorBackend> P0GFS;
        P0GFS p0gfs0(sdgv0, p0fem, pressureConstraints);
        p0gfs0.name("pressure");

        // GFS1
        typedef Dune::PDELab::GridFunctionSpace<SDGV, P0FEM, Dune::PDELab::NoConstraints, VectorBackend> P0DarcyGFS;
        P0DarcyGFS p0gfs1(sdgv1, p0fem);
        p0gfs1.name("pressure");

        // composed grid function space GFS0
        typedef Dune::PDELab::CompositeGridFunctionSpace<VectorBackend,
                Dune::PDELab::LexicographicOrderingTag, StaggeredQ0GFS, P0GFS> ComposedGFS;
        ComposedGFS composedGfs0(staggeredQ0Gfs0, p0gfs0);

        typedef Dune::PDELab::MultiDomain::MultiDomainGridFunctionSpace<
                Grid,
                MDVBE,
                Dune::PDELab::LexicographicOrderingTag,
                ComposedGFS,
                P0DarcyGFS
                > MultiGFS;
        MultiGFS multigfs(grid, composedGfs0, p0gfs1);

        // make grid function operator
        // for Stokes
        typedef Dune::PDELab::NavierStokesStaggeredGrid<BCType,
                SourceMomentumBalance<MDGV, RF>, SourceMassBalance<MDGV, RF>,
                DirichletVelocity<MDGV, RF>, DirichletPressure<MDGV, RF>,
                NeumannVelocity<MDGV, RF>, NeumannPressure<MDGV, RF>, MDGV> LOpStokes;
        LOpStokes lopStokes(bc, sourceMomentumBalance, sourceMassBalance,
                dirichletVelocity, dirichletPressure, neumannVelocity, neumannPressure, mdgv);

        typedef Dune::PDELab::NavierStokesTransientStaggeredGrid<MDGV> TLOpStokes;
        TLOpStokes tlopStokes(mdgv);

        // for Darcy
        typedef Dune::PDELab::DiffusionCCFV<DarcyK<MDGV, RF>, DarcyA0<MDGV, RF>,
                DarcySource<MDGV, RF>, DarcyBC<MDGV>, DarcyJ<MDGV, RF>, DarcyG<MDGV, RF> > LOpDarcy;
        LOpDarcy lopDarcy(darcyK, darcyA0, darcySource, darcyBC, darcyJ, darcyG);

        typedef Dune::PDELab::L2<MDGV> TLOpDarcy;
        TLOpDarcy tlopDarcy(mdgv);

        typedef Dune::PDELab::MultiDomain::SubDomainEqualityCondition<Grid> Condition;
        Condition c0(0);
        Condition c1(1);

        typedef Dune::PDELab::MultiDomain::SubProblem
                <MultiGFS, MultiGFS, LOpStokes, Condition, 0> UpperSubProblem_x;
        UpperSubProblem_x upper_sp_x(lopStokes, c0);

        typedef Dune::PDELab::MultiDomain::SubProblem
                <MultiGFS, MultiGFS, TLOpStokes, Condition, 0> UpperSubProblem_t;
        UpperSubProblem_t upper_sp_t(tlopStokes, c0);

        typedef Dune::PDELab::MultiDomain::SubProblem
                <MultiGFS, MultiGFS, LOpDarcy, Condition, 1> LowerSubProblem_x;
        LowerSubProblem_x lower_sp_x(lopDarcy, c1);

        typedef Dune::PDELab::MultiDomain::SubProblem
                <MultiGFS, MultiGFS, TLOpDarcy, Condition, 1> LowerSubProblem_t;
        LowerSubProblem_t lower_sp_t(tlopDarcy, c1);

        typedef typename Dune::PDELab::BackendVectorSelector<MultiGFS, DF>::Type V;
        V xOld(multigfs);
        xOld = 0.0;

        CouplingStokes1pDarcy1p<RF> couplingStokes1pDarcy1p;
        typedef Dune::PDELab::MultiDomain::Coupling<UpperSubProblem_x, LowerSubProblem_x, CouplingStokes1pDarcy1p<RF> > Coupling_mono;
        Coupling_mono coupling_mono(upper_sp_x, lower_sp_x, couplingStokes1pDarcy1p);

        auto constraints = Dune::PDELab::MultiDomain::constraints<RF>(multigfs,
                Dune::PDELab::MultiDomain::constrainSubProblem(upper_sp_x, bc),
                Dune::PDELab::MultiDomain::constrainSubProblem(lower_sp_x, darcyBC));

        typedef MultiGFS::ConstraintsContainer<RF>::Type ConstraintsContainer;
        ConstraintsContainer constraintsContainer;
        constraints.assemble(constraintsContainer);

        typedef Dune::PDELab::ISTLMatrixBackend MatrixBackend;

        // for monolithic coupling
        typedef Dune::PDELab::MultiDomain::GridOperator<
                MultiGFS, MultiGFS,
                MatrixBackend, RF, RF, RF, ConstraintsContainer, ConstraintsContainer,
                UpperSubProblem_x,
                LowerSubProblem_x,
                Coupling_mono
                > GridOperator_x;

        typedef Dune::PDELab::MultiDomain::GridOperator<
                MultiGFS, MultiGFS,
                MatrixBackend, RF, RF, RF, ConstraintsContainer, ConstraintsContainer,
                UpperSubProblem_t,
                LowerSubProblem_t
                > GridOperator_t;

        typedef Dune::PDELab::OneStepGridOperator<GridOperator_x,GridOperator_t> GridOperator;

        GridOperator_x gridoperator_x(multigfs, multigfs,
                constraintsContainer, constraintsContainer,
                upper_sp_x,
                lower_sp_x,
                coupling_mono);

        GridOperator_t gridoperator_t(multigfs, multigfs,
                constraintsContainer, constraintsContainer,
                upper_sp_t,
                lower_sp_t);

        GridOperator gridoperator(gridoperator_x,gridoperator_t);

        // for iterative coupling
        auto parse_coupling_type = [](std::string name) -> Dune::PDELab::CouplingMode
                {
            if (name == "Stokes")
                return Dune::PDELab::CouplingMode::Stokes;
            if (name == "Darcy")
                return Dune::PDELab::CouplingMode::Darcy;
            DUNE_THROW(Dune::Exception,"Unknown coupling type " << name);
                };

        typedef Dune::PDELab::StokesDarcyCoupling<RF> SDCoupling;

        SDCoupling sd_coupling_stokes(parse_coupling_type(parameters["dn.coupling.upper"]));
        SDCoupling sd_coupling_darcy(parse_coupling_type(parameters["dn.coupling.lower"]));

        typedef Dune::PDELab::MultiDomain::Coupling<
                UpperSubProblem_x,
                LowerSubProblem_x,
                SDCoupling
                > StokesCoupling;
        StokesCoupling stokes_coupling(
                upper_sp_x,
                lower_sp_x,
                sd_coupling_stokes
        );

        typedef Dune::PDELab::MultiDomain::Coupling<
                UpperSubProblem_x,
                LowerSubProblem_x,
                SDCoupling
                > DarcyCoupling;
        DarcyCoupling darcy_coupling(
                upper_sp_x,
                lower_sp_x,
                sd_coupling_darcy
        );

        typedef Dune::PDELab::MultiDomain::GridOperator<
                MultiGFS,MultiGFS,
                MatrixBackend, RF, RF, RF, ConstraintsContainer, ConstraintsContainer,
                UpperSubProblem_x,
                StokesCoupling,
                LowerSubProblem_x,
                DarcyCoupling
                > FullOperator_x;
        FullOperator_x full_operator(
                multigfs,multigfs,
                constraintsContainer, constraintsContainer,
                upper_sp_x,
                stokes_coupling,
                lower_sp_x,
                darcy_coupling
        );

        typedef Dune::PDELab::MultiDomain::GridOperator<
                MultiGFS,MultiGFS,
                MatrixBackend, RF, RF, RF, ConstraintsContainer, ConstraintsContainer,
                UpperSubProblem_x,
                StokesCoupling
                > UpperOperator_x;
        UpperOperator_x upper_operator_x(
                multigfs,multigfs,
                constraintsContainer, constraintsContainer,
                upper_sp_x,
                stokes_coupling
        );
        typedef Dune::PDELab::MultiDomain::GridOperator<
                MultiGFS,MultiGFS,
                MatrixBackend, RF, RF, RF, ConstraintsContainer, ConstraintsContainer,
                UpperSubProblem_t
                > UpperOperator_t;
        UpperOperator_t upper_operator_t(
                multigfs,multigfs,
                constraintsContainer, constraintsContainer,
                upper_sp_t
        );
        typedef Dune::PDELab::OneStepGridOperator<UpperOperator_x,UpperOperator_t> UpperOperator;
        UpperOperator upper_operator(upper_operator_x,upper_operator_t);

        typedef Dune::PDELab::MultiDomain::GridOperator<
                MultiGFS,MultiGFS,
                MatrixBackend, RF, RF, RF, ConstraintsContainer, ConstraintsContainer,
                LowerSubProblem_x,
                DarcyCoupling
                > LowerOperator_x;
        LowerOperator_x lower_operator_x(
                multigfs,multigfs,
                constraintsContainer, constraintsContainer,
                lower_sp_x,
                darcy_coupling
        );

        typedef Dune::PDELab::MultiDomain::GridOperator<
                MultiGFS,MultiGFS,
                MatrixBackend, RF, RF, RF, ConstraintsContainer, ConstraintsContainer,
                LowerSubProblem_t
                > LowerOperator_t;
        LowerOperator_t lower_operator_t(
                multigfs,multigfs,
                constraintsContainer, constraintsContainer,
                lower_sp_t
        );
        typedef Dune::PDELab::OneStepGridOperator<LowerOperator_x,LowerOperator_t> LowerOperator;
        LowerOperator lower_operator(lower_operator_x,lower_operator_t);

        // make coefficent Vector and initialize it from a function
        Dune::PDELab::MultiDomain::interpolateOnTrialSpace(multigfs, xOld,
                dirichletComposed, upper_sp_x, darcyG, lower_sp_x);

#ifdef ANALYTIC_SOLUTION
        // clear interior
        Dune::PDELab::set_nonconstrained_dofs(constraintsContainer, 0.0, xOld);
#endif

        int dofs = xOld.block(0).N() + xOld.block(1).N();
        std::cout << "interpolation: " << dofs << " dof total, " << constraintsContainer.size() << " dof constrained" << std::endl;

        using SubDomainIndex = typename Grid::SubDomainIndex;

        if (parameters.get<bool>("monolithic.solve"))
        {
            // <<<5>>> Select a linear solver backend
            //        #if HAVE_UMFPACK
            //            typedef Dune::PDELab::ISTLBackend_SEQ_UMFPack LinearSolver;
            //        #elif HAVE_SUPERLU
            //            typedef Dune::PDELab::ISTLBackend_SEQ_SuperLU LinearSolver;
            //        #else
            //        #error No direct linear solver, use UMFPack or SuperLU.
            //        #endif
            //            LinearSolver ls(false);

            typedef Dune::PDELab::ISTLBackend_SEQ_BCGS_SSOR LinearSolver;
            LinearSolver ls(
                    parameters.get<int>("monolithic.linearsolver.iterations"),
                    parameters.get<int>("monolithic.linearsolver.verbosity"));

            V xOld2(xOld);

            // <<<6>>> Solve nonlinear problem
            typedef Dune::PDELab::Newton<GridOperator, LinearSolver, V> NewtonSolver;
            NewtonSolver newton(gridoperator, xOld2, ls);
            newton.setVerbosityLevel(2);
            newton.setMaxIterations(25);
            newton.setReduction(1e-9);
            newton.setLineSearchStrategy(newton.noLineSearch);

            // <<7>> time stepper
            Dune::PDELab::ImplicitEulerParameter<RF> method; // defines coefficients
            Dune::PDELab::OneStepMethod<RF, GridOperator, NewtonSolver,V,V> osm(method,gridoperator,newton);
            osm.setVerbosityLevel(2);  // time stepping scheme

            // <<<8>>> graphics for initial guess
            typedef Dune::PDELab::GridFunctionSubSpace
                    <MultiGFS, Dune::TypeTree::TreePath<indexUpper, indexVelocity> > VSUB;
            VSUB vsub(multigfs);                   // velocity subspace

            typedef Dune::PDELab::GridFunctionSubSpace
                    <MultiGFS, Dune::TypeTree::TreePath<indexUpper, indexPressure> > PSUB;
            PSUB psub(multigfs);                   // pressure subspace

            // make discrete function object
            typedef Dune::PDELab::DiscreteGridFunction<VSUB, V> StaggeredQ0DGF;
            StaggeredQ0DGF staggeredQ0DGF(vsub, xOld2);
            typedef Dune::PDELab::DiscreteGridFunction<PSUB, V> P0DGF;
            P0DGF p0dgf(psub, xOld2);

            Dune::PDELab::FilenameHelper fn_upper("test_mono_transient_upper");
            Dune::PDELab::FilenameHelper fn_lower("test_mono_transient_lower");

            // plot result as VTK
            Dune::SubsamplingVTKWriter<SDGV> vtkwriterStokes(sdgv0, 1);
            vtkwriterStokes.addCellData(new Dune::PDELab::VTKGridFunctionAdapter<StaggeredQ0DGF>(staggeredQ0DGF, "velocity"));
            vtkwriterStokes.addCellData(new Dune::PDELab::VTKGridFunctionAdapter<P0DGF>(p0dgf, "pressure"));

            vtkwriterStokes.write(fn_upper.getName(), Dune::VTK::ascii);
            fn_upper.increment();

            typedef Dune::PDELab::GridFunctionSubSpace
                    <MultiGFS, Dune::TypeTree::TreePath<indexLower> > LowerGFS;
            LowerGFS lowerGFS(multigfs);                   // velocity subspace
            // make discrete function object
            typedef Dune::PDELab::DiscreteGridFunction<LowerGFS, V> P0DarcyDGF;
            P0DarcyDGF p0dgfDarcy(lowerGFS, xOld2);

            // plot result as VTK
            Dune::VTKWriter<SDGV> vtkwriterDarcy(sdgv1, Dune::VTK::nonconforming);
            vtkwriterDarcy.addCellData(new Dune::PDELab::VTKGridFunctionAdapter<P0DarcyDGF>(p0dgfDarcy, "pressure"));

            vtkwriterDarcy.write(fn_lower.getName(), Dune::VTK::ascii);
            fn_lower.increment();

            // compute L2 error if analytical solution is available
            DirichletVelocity<SDGV, RF> dirichletVelocitySubdomain(sdgv0);
            DirichletPressure<SDGV, RF> dirichletPressureSubdomain(sdgv0);
            std::cout.precision(8);
            std::cout << "L2 error (Stokes) for "
                    << std::setw(6) << sdgv0.size(0)
                    << " elements. pressure: "
                    << std::scientific
                    << l2interpolationerror(dirichletPressureSubdomain, psub, xOld2, 8)
                    << " velocity: "
                    << std::scientific
                    << l2interpolationerror(dirichletVelocitySubdomain, vsub, xOld2, 8)
                    << std::endl;

            // <<9>> time loop
            RF time = 0;
//            const RF dt_ini = M_PI/2.0;//parameters.get<double>("discretization.dt");
            const RF tend = 0.5*M_PI;//parameters.get<double>("discretization.tEnd");
//            int factor = parameters.get<int>("discretization.factor");
            const RF dt = 0.05*M_PI; //dt_ini/factor;
            V xNew2(xOld2);
            while (time < tend - 1e-8) {
                // do time step

                auto f_ = Dune::PDELab::MultiDomain::interpolateOnSubProblems(dirichletComposed,upper_sp_x,darcyG,lower_sp_x);

                osm.apply(time,dt,xOld2,f_,xNew2);    // do one time step

                // graphics

                {   // Stokes
                    Dune::SubsamplingVTKWriter<SDGV> vtkwriterStokes(sdgv0, 1);
                    vtkwriterStokes.addCellData(new Dune::PDELab::VTKGridFunctionAdapter<StaggeredQ0DGF>(staggeredQ0DGF, "velocity"));
                    vtkwriterStokes.addCellData(new Dune::PDELab::VTKGridFunctionAdapter<P0DGF>(p0dgf, "pressure"));

                    vtkwriterStokes.write(fn_upper.getName(), Dune::VTK::ascii);
                    fn_upper.increment();
                }

                {   // Darcy
                    Dune::VTKWriter<SDGV> vtkwriterDarcy(sdgv1, Dune::VTK::nonconforming);
                    vtkwriterDarcy.addCellData(new Dune::PDELab::VTKGridFunctionAdapter<P0DarcyDGF>(p0dgfDarcy, "pressure"));

                    vtkwriterDarcy.write(fn_lower.getName(), Dune::VTK::ascii);
                    fn_lower.increment();
                }

                xOld2 = xNew2;                    // advance in time step
                time += dt;
            }
        } //  if (parameters.get<bool>("monolithic.solve"))

        if (parameters.get<bool>("dn.solve"))
        {
            // <<<5>>> Select a linear solver backend
            // direct solver for Stokes
#if HAVE_SUPERLU
            typedef Dune::PDELab::ISTLBackend_SEQ_SuperLU StokesSolver;
#else
#error No direct linear solver, use SuperLU.
#endif

            StokesSolver stokes_solver(false);

            // iterative solver for Darcy
            typedef ISTL_SEQ_Subblock_Backend<
                    Dune::SeqSSOR,
                    Dune::BiCGSTABSolver
                    > DarcySolver;

            DarcySolver darcy_solver(
                    1,
                    parameters.get<int>("dn.lower.linearsolver.iterations"),
                    parameters.get<int>("dn.lower.linearsolver.verbosity")
            );

            // <<<6>>> Solver for nonlinear problem per stage (Stokes)
            double newtonreduction = parameters.get<double>("dn.upper.newtonreduction");
            typedef Dune::PDELab::Newton<UpperOperator, StokesSolver, V> NewtonSolver;
            NewtonSolver upper_newton(upper_operator, xOld, stokes_solver);
            upper_newton.setVerbosityLevel(2);
            upper_newton.setMaxIterations(25);
            upper_newton.setReduction(newtonreduction);
            upper_newton.setLineSearchStrategy(upper_newton.noLineSearch);

            // Solver for linear problem per stage (Darcy)
            typedef Dune::PDELab::StationaryLinearProblemSolver<
                    LowerOperator,
                    DarcySolver,
                    V
                    > LowerPDESolver;
            LowerPDESolver lower_pde_solver(
                    lower_operator,
                    darcy_solver,
                    parameters.sub("dn.lower.solver")
            );

            // <<<7>>> time stepper
            Dune::PDELab::ImplicitEulerParameter<RF> method;               // defines coefficients
            Dune::PDELab::OneStepMethod<RF,UpperOperator,NewtonSolver,V,V> osm_upper(method,upper_operator,upper_newton);
            Dune::PDELab::OneStepMethod<RF,LowerOperator,LowerPDESolver,V,V> osm_lower(method,lower_operator,lower_pde_solver);
            osm_upper.setVerbosityLevel(2);
            osm_lower.setVerbosityLevel(2);

            V residual(multigfs);
            full_operator.residual(xOld,residual);

            RF r, start_r;
            r = start_r = residual.two_norm();
            std::cout << "Start residual: " << r << std::endl;

            std::size_t i = 0;
            std::size_t max_iterations = parameters.get<int>("dn.coupling.iterations");

            auto x(xOld);

            double theta = parameters.get<double>("dn.coupling.damping");
            double rel_reduction = parameters.get<double>("dn.coupling.reduction");
            double max_error = parameters.get<double>("dn.coupling.maxerror");

            const std::size_t vtk_frequency = parameters.get<int>("dn.coupling.vtk_frequency");
            const bool vtk_enabled = vtk_frequency > 0;

            // <<<8>>> graphics for initial guess
            typedef Dune::PDELab::GridFunctionSubSpace
                    <MultiGFS, Dune::TypeTree::TreePath<indexUpper, indexVelocity> > VSUB;
            VSUB vsub(multigfs);                   // velocity subspace

            typedef Dune::PDELab::GridFunctionSubSpace
                    <MultiGFS, Dune::TypeTree::TreePath<indexUpper, indexPressure> > PSUB;
            PSUB psub(multigfs);                   // pressure subspace

            // make discrete function object
            typedef Dune::PDELab::DiscreteGridFunction<VSUB, V> StaggeredQ0DGF;
            StaggeredQ0DGF staggeredQ0DGF(vsub, xOld);
            typedef Dune::PDELab::DiscreteGridFunction<PSUB, V> P0DGF;
            P0DGF p0dgf(psub, xOld);

            Dune::PDELab::FilenameHelper fn_upper("test_pdelabDNcoupledtransient_upper");
            Dune::PDELab::FilenameHelper fn_lower("test_pdelabDNcoupledtransient_lower");

            // plot result as VTK
            Dune::SubsamplingVTKWriter<SDGV> vtkwriterStokes(sdgv0, 1);
            vtkwriterStokes.addCellData(new Dune::PDELab::VTKGridFunctionAdapter<StaggeredQ0DGF>(staggeredQ0DGF, "velocity"));
            vtkwriterStokes.addCellData(new Dune::PDELab::VTKGridFunctionAdapter<P0DGF>(p0dgf, "pressure"));

            vtkwriterStokes.write(fn_upper.getName(), Dune::VTK::ascii);
            fn_upper.increment();

            typedef Dune::PDELab::GridFunctionSubSpace
                    <MultiGFS, Dune::TypeTree::TreePath<indexLower> > LowerGFS;
            LowerGFS lowerGFS(multigfs);                   // velocity subspace
            // make discrete function object
            typedef Dune::PDELab::DiscreteGridFunction<LowerGFS, V> P0DarcyDGF;
            P0DarcyDGF p0dgfDarcy(lowerGFS, xOld);

            // plot result as VTK
            Dune::VTKWriter<SDGV> vtkwriterDarcy(sdgv1, Dune::VTK::nonconforming);
            vtkwriterDarcy.addCellData(new Dune::PDELab::VTKGridFunctionAdapter<P0DarcyDGF>(p0dgfDarcy, "pressure"));

            vtkwriterDarcy.write(fn_lower.getName(), Dune::VTK::ascii);
            fn_lower.increment();

            // <<<9>>> time loop
            RF time = 0;
#ifdef ANALYTIC_SOLUTION
            const RF dt_ini = M_PI*0.05;//parameters.get<double>("discretization.dt");
            const RF tEnd = M_PI*0.5; //parameters.get<double>("discretization.tEnd");
#else
            const RF dt_ini = 16*0.05*M_PI;//parameters.get<double>("discretization.dt");
            const RF tEnd = 16*0.5*M_PI; //parameters.get<double>("discretization.tEnd");
#endif
            const RF factor = parameters.get<double>("discretization.factor");
            const RF dt = dt_ini * factor;
            V xNewStokes(xOld);
            V xNewDarcy(xOld);
            int k = 0;

            auto start = std::chrono::high_resolution_clock::now();

            while (time < tEnd - 1e-8)
            {
                k += 1;
                std::cout << "*** time step " << k << std::endl;
                auto f_ = Dune::PDELab::MultiDomain::interpolateOnSubProblems(dirichletComposed,upper_sp_x,darcyG,lower_sp_x);

                while ((r / start_r > rel_reduction && r > max_error && i < max_iterations) || i == 0)
                {
                    if (vtk_enabled && i % vtk_frequency == 0)
                    {
                        // Stokes
                        vtkwriterStokes.write(fn_upper.getName(), Dune::VTK::ascii);
                        fn_upper.increment();

                        // Darcy
                        vtkwriterDarcy.write(fn_lower.getName(), Dune::VTK::ascii);
                        fn_lower.increment();
                    } // if (vtk_enabled && i % vtk_frequency == 0)

                    std::cout << "\n*** Stokes:" << std::endl;
                    osm_upper.apply(time,dt,xOld,f_,xNewStokes);
                    xNewDarcy = xNewStokes;

                    std::cout << "\n*** Darcy:" << std::endl;
                    osm_lower.apply(time,dt,xNewStokes,f_,xNewDarcy);

                    xNewDarcy *= theta;
                    xNewDarcy.axpy(1.0-theta,x);

                    residual = 0.0;
                    full_operator.residual(xNewDarcy,residual);
                    r = residual.two_norm();
                    ++i;
                    std::cout << std::setw(4) << "\n*** Iteration " << i << " done, residual = " << r << std::endl;

                    x = xNewDarcy;
                    xOld = xNewDarcy;
                    xNewStokes = xNewDarcy;
                } // while (r / start_r > rel_reduction && r > max_error && i < max_iterations)

                // graphics for each time step
                // Stokes
                vtkwriterStokes.write(fn_upper.getName(), Dune::VTK::ascii);
                fn_upper.increment();

                // Darcy
                vtkwriterDarcy.write(fn_lower.getName(), Dune::VTK::ascii);
                fn_lower.increment();

                i = 0;
                time += dt;
                start_r = r;

            }// while (time < tEnd - 1e-8)

            auto end = std::chrono::high_resolution_clock::now();

            auto elapsed = end - start;

            std::cout << std::endl
                    << std::endl
                    << "iterative Stokes-Darcy solver: " << std::chrono::duration_cast<std::chrono::duration<double> >(elapsed).count() << "s."<< std::endl
                    << std::endl;


            // compute L2 error if analytical solution is available
            DirichletVelocity<SDGV, RF> dirichletVelocitySubdomain(sdgv0);
            dirichletVelocitySubdomain.setTime(time);
            DirichletPressure<SDGV, RF> dirichletPressureSubdomain(sdgv0);
            DarcyG<SDGV, RF> darcyPressureSubdomain(sdgv1);
            std::cout.precision(8);
            //            std::cout << "L2 error (Stokes) for "
            //                    << std::setw(6) << sdgv0.size(0)
            //                    << " elements. pressure: "
            //                    << std::scientific
            //                    << l2interpolationerror(dirichletPressureSubdomain, psub, xOld, 8)
            //                    << ", velocity: "
            //                    << std::scientific
            //                    << l2interpolationerror(dirichletVelocitySubdomain, vsub, xOld, 8)
            //                    << std::endl
            //                    << "L2 error (Darcy)  for "
            //                    << std::setw(6) << sdgv1.size(0)
            //                    << " elements. pressure: "
            //                    << std::scientific
            //                    << l2interpolationerror(darcyPressureSubdomain, lowerGFS, xOld, 8)
            //                    << std::endl;


//            double elements = std::pow(4,refinementLevel);
            std::ofstream file("results_pdelabDNcoupledtransient.txt", std::ios::out|std::ios::app);
            file << refinementLevel << "\t"
                    << std::chrono::duration_cast<std::chrono::duration<double> >(elapsed).count() << "\t"
                    << std::scientific << l2interpolationerror(dirichletPressureSubdomain, psub, xOld, 8) << "\t"
                    << std::scientific << l2interpolationerror(dirichletVelocitySubdomain, vsub, xOld, 8) << "\t"
                    << std::scientific << l2interpolationerror(darcyPressureSubdomain, lowerGFS, xOld, 8) << "\t"
                    << std::endl;

        } // if (parameters.get<bool>("dn.solve"))

        return EXIT_SUCCESS;
    } // try
    catch (Dune::Exception &e){
        std::cerr << "Dune reported error: " << e << std::endl;
        return 1;
    }
    catch (std::exception e) {
        std::cerr << "Dune reported std error: " << e.what() << std::endl;
    }
    catch (...){
        std::cerr << "Unknown exception thrown!" << std::endl;
        return 1;
    }
}
